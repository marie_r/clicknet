function check_xwavs(root, deployments)
% check_xwavs(root)
% Run the XwavAnalyzer class to check for gaps in data stored in the
% extended wav file format (.x.wav) used with high-frequency acoustic
% recording packages developed by Sean Wiggins in John Hildebrand's 
% Scripps Whale Acoustics Lab.
%
% Output is a list of contiguous data regions

narginchk(0, 2);
if nargin < 2
    deployments = {'DT03', 'DT08', 'MC03'};
    if nargin < 1
        root = 'F:\corpora\GofMX\bandpass10-90kHz';  % default
    else
        assert(isdir(root), 'root must be a directory');
    end
else
    assert(iscell(deployments), ...
        'deployments must be a cell array of deployment strings');
end

files = findfiles(root, '*.x.wav');

xwavinfo = XwavAnalyzer(files);

for didx = 1:length(deployments)
    dep = deployments{didx};
    % indices associated with this deployment
    indices = find(~cellfun(@isempty, strfind(files, dep)));
    fprintf('%s\n', dep);
    fprintf('Start %s\n', ...
        datestr(xwavinfo(indices(1)).startUTC, 'YYYY-mm-DD HH:MM:SS.FFF'))
    for idx = 1:length(indices)
        if idx > 1
            xwavinfo(indices(idx)).contiguous_with_previous(xwavinfo(indices(idx-1)));
        end
        xwavinfo(indices(idx)).report();
    end
    fprintf('End %s\n', datestr(...
        xwavinfo(indices(end)).startUTC, 'YYYY-mm-DD HH:MM:SS.FFF'));
end