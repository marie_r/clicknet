function encounters = identify_encounters(pattern, audio_dir, detedit_dir)
% encounters = identify_encounters(audio_dir, detedit_mat_dir)
% Given a directory containing detection edited Matlab files,
% load and identify encounters to test
%
% Example:
% identify_encounters('GofMX', 'c:/hdd/corpora/GofMX', ...
%   'c:/hdd/corpora/GofMX/siodeteditclicks')
%
% Requires:
%   Tethys/MatlabClient database module to be on path
%   http://tethys.sdsu.edu

audext = '.wav';

% Get a list of the audio files and click labels.
audio_files = dbFindFiles('Path', audio_dir, 'Pattern', [pattern, '*', audext]);
labels = dbFindFiles('Path', detedit_dir, 'Pattern', pattern);

%--------------------------------------------------------------------
% Find the deployments associated with the audio files by stripping
% off the filenames and disk directories and then finding unique ones.
% This is HARP specific.

% Find the deployment directories
audio_dirs = cellfun(@fileparts, audio_files, 'UniformOutput', 0);
[deployments, deployment_disks] = cellfun(@fileparts, audio_dirs, 'UniformOutput', 0);
deployments = unique(deployments);
deployment_disks = unique(deployment_disks);

% Strip out the audio_dir prefix to have a relative path to the deployment.
if ~ (audio_dir(end) == filesep || audio_dir(end) == '/')
    prefix = [audio_dir, filesep];
else
    prefix = audio_dir;
end
deployments = strrep(deployments, prefix, '');
% Find the deployment name by itself so that we can match in the label list
[~, deployment_names] = cellfun(@fileparts, deployments, ...
    'UniformOutput', 0);
%-------------------------------------------------------------------


% for data exploration
p = [.95 .98 .99 .995 .999];
quantiles = zeros(length(labels), length(p));

% Time between encounters
encounter_sep_m = 10;
encounter_sep_d = encounter_sep_m / 60 / 24;  % to days

total_clicks = 0;
encounter_threshold = 10;  % at least this many clicks to be considered
encounters = struct(...
    'encountersN', 0, ...
    'clicksN', 0, ...
    'name', '', ...
    'starts', [], 'stops', [], 'encounter_clicksN', []);
   
compute_quantiles = false;

% ASSUMPTIONS:  
% - No gaps in data associated with a deployment
% - The last click detection has at least pad_time_d days before 
%   the end of data
pad_time_d = datenum(0,0,0,0,0,.0015);  % 1.5 ms in days

% Write out the XML
fileH = 1;

fprintf(fileH, '\t<!--\n')

for didx = 1:length(deployments)
    
    % Find disks associated with this deployment
    diskP = cellfun(@(x) ~isempty(x), ...
        strfind(deployment_disks,  deployment_names{didx}));
    group_disks = deployment_disks(diskP);
    % Find labels associated with this deployment
    labelP = false(size(labels));
    for diskidx = 1:length(group_disks)
        matches = find(cellfun(@(x) ~isempty(x), ...
            strfind(labels, group_disks(diskidx))));
        if isempty(matches)
            fprintf('Unable to find labels for %s\n', group_disks{diskidx});
        else
            labelP(matches) = true;
        end
    end
    label_group = labels(labelP);

    clear d;  % clear previous iteration
    for idx=1:length(label_group)
        d(idx) = load(label_group{idx});
    end    
    click_times = vertcat(d.MTTsave);
    assert(issorted(click_times), ...
        sprintf('Clicks for labels are not sorted:\n%s', ...
            strjoin(label_group)));
    
    metrics_paper = true;
    if metrics_paper == true && strcmp(deployments{didx}, 'GofMX_DT08')
        % Restrict detections to DCLDE subset of data
        click_times = resctrict_dclde2018(click_times);
    end
    
    encounters(didx).name = deployments{didx};
    encounters(didx).encountersN = 0;
    encounters(didx).clicksN = 0;
    
    % Write out click times in a format that deepclick can process
    species_names = [];  % don't know
     save(fullfile(detedit_dir, '..', ...
         'LabeledClicksPerDeployment', encounters(didx).name), ...
         'species_names', 'click_times');
    
    total_dur = 0;
    if length(click_times) > 10
        time_to_next_click = diff(click_times);
        
        breaks = find(time_to_next_click > encounter_sep_d);
        breaks(end+1) = length(click_times)+1;
        
        first = 1;
        for bidx = 1:length(breaks)-1
            last = breaks(bidx);
            if last <= first + encounter_threshold
                continue  % too few clicks, ignore
            end
            encounters(didx).encountersN = encounters(didx).encountersN + 1;
            clicks = last - first + 1;
            encounters(didx).clicksN = encounters(didx).clicksN + clicks;
            encounters(didx).starts(end+1) = click_times(first);
            encounters(didx).stops(end+1) = click_times(last) + pad_time_d;
            encounters(didx).encounter_clicksN(end+1) = clicks;
    
            dur = click_times(last) - click_times(first);
            total_dur = total_dur + dur;
            fprintf(fileH, '\te%3d clicks %5d\t%s - %s dur %s\n', ...
                encounters(didx).encountersN, clicks, ...
                dbSerialDateToISO8601(click_times(first)), ...
                dbSerialDateToISO8601(click_times(last)), ...
                datestr(dur, 'HH:MM:SS'));
            first = breaks(bidx)+1;
        end
        
        if compute_quantiles
            sorted_s = sort(diff(d.MTTsave*24*3600));
            indices = round(p * length(sorted_s));
            quantiles(didx,:) = sorted_s(indices)';
        end
    else
        quantiles(didx,:) = p * NaN;
    end
end
total_clicks = sum(vertcat(encounters.clicksN));
% Find time of encounters in hours
time_h = arrayfun(@(e) e.stops - e.starts, encounters, 'UniformOutput', 0);
time_h = horzcat(time_h{:});
time_m = time_h * 60;
encountersN = length(time_h);
clicks_per_encounter = horzcat(encounters.encounter_clicksN);

fprintf(fileH, '\tTotal duration %s\n', datestr(total_dur, 'HH:MM:SS'));
fprintf(fileH, '\tTotal clicks %d, clicks/encounter = %.1f\n', ...
    total_clicks, total_clicks / encountersN);
fprintf(fileH, '\tEncounter temporal statistics (m):  ');
fprintf(fileH, '\tmu = %.2f, var = %.4f, median = %.2f\n', ...
    mean(time_m), var(time_m), median(time_m));
fprintf(fileH, '\t-->\n');
bins = 100;
figure('Name', 'Encounter duration')
histogram(time_m*60, bins)
xlabel('duration (m)')
ylabel('counts');

figure('Name', 'Encounter clicks')
histogram(clicks_per_encounter, bins)
xlabel('clicks')
ylabel('counts')

fprintf(fileH, '  <effort>\n');
for didx = 1:length(encounters)
    fprintf(fileH, '    <deployment>\n');
    fprintf(fileH, '      <!-- name is the audio dir and .mat with labels -->\n');
    fprintf(fileH, '      <name>%s</name>\n', deployments{didx});
    fprintf(fileH, '      <type> time </type>\n');
    isostarts = dbSerialDateToISO8601(encounters(didx).starts);
    isostops = dbSerialDateToISO8601(encounters(didx).stops);
    fprintf(fileH, '      <start type="list(datetime)">\n')
    fprintf(fileH, serial_dates_to_xml_list(encounters(didx).starts, ...
        '      '));
    fprintf(fileH, '\n      </start>\n')
    fprintf(fileH, '      <end type="list(datetime)">\n')
    fprintf(fileH, serial_dates_to_xml_list(encounters(didx).stops, ...
        '      '));
    fprintf(fileH, '\n      </end>\n')
    
    fprintf(fileH, '    </deployment>\n');
end
fprintf(fileH, '  </effort>\n')
%histogram(quantiles, 200);

end 
    
function s = serial_dates_to_xml_list(dates, indent)
    iso = dbSerialDateToISO8601(dates);
    newline_everyN = 4;  % # date before newline
    lines = {};
    sep = ', ';
    for idx = 1:length(iso)
        if idx == length(iso)
            sep = '';   % last entry, no separator
        end
        if rem(idx-1, newline_everyN) == 0 
            lines{end+1} = indent;
        end
        lines{end} = sprintf('%s%s%s', lines{end}, iso{idx}, sep);
    end
    s = strjoin(lines, '\n');
    1;
end

function click_times = resctrict_dclde2018(click_times)
% click_times = resctrict_dclde2018(click_times)
% Clicks were computed for the entire deployment, but we will
% restrict these to a subset of DCLDE 2018's Gulf of Mexico deployment 8

% Ugly kludgy code for Metrics paper, need to extract small sections
% of standard format...

% Timestamps of start of first file and end of last
% We used to add the HARP file duration, but this proved to be
% unreliable.  Make PARAMS global, then open up the XWAV and 
% check the end time:  datestr(PARAMS.end.dnum)
disk_first_last = [
    % disk 1
    datetime(2014,10,07,00,00,00), datetime(2014,10,13,00,00,00)
    % disk 8 
    datetime(2015,02,17,00,00,00), datetime(2015,02,18,00,00,00)
    %disk 8 and 9
    datetime(2015,02,19,00,00,00), datetime(2015,02,24,00,00,00)
    %datetime(2015,02,19,00,00,00), datetime(2015,02,20,16,53,30)
    % disk 9
    %datetime(2015,02,20,16,52,30), datetime(2015,02,24,00,00,00)
    ];

% Convert times to datenums.  endtimes have the standard
% xwav duration at 200 kHz added to them as these
% are starts of the file
% dur_m = 37.5;  % xwav file duration 
disk_start_end = zeros(size(disk_first_last));
% Create small pad around ends so that we are sure that we don't
% go off the ends with data
pad = milliseconds(10);
disk_start_end(:,1) = datenum(disk_first_last(:,1) + pad);
disk_start_end(:,2) = datenum(disk_first_last(:,2) - pad);
retain = false(size(click_times));
for ridx = 1:size(disk_start_end, 1)
    se = disk_start_end(ridx,:);  % start/end
    retain = retain | (click_times >= se(1) & click_times < se(2));
end

% Remove things not associated
click_times = click_times(retain);

end