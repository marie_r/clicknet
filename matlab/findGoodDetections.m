function [det_good,gt_good] = findGoodDetections(detections,groundTruth,max_delta)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
det_good = false(length(detections),1);
gt_good = false(length(groundTruth),1);
found_idx = 1;
gt_idx = 1;
while found_idx < length(detections)
    shifted_found = addtodate(detections(found_idx),2000,'year');
    while(gt_idx < length(groundTruth) && groundTruth(gt_idx) < shifted_found)
        gt_idx = gt_idx + 1;
    end
    prev = gt_idx -1;
    if prev > 0 && prev < length(groundTruth)
        delta = (shifted_found - groundTruth(prev))*24*3600;
        if abs(delta) <= max_delta
           det_good(found_idx) = true;
           gt_good(prev) = true;
        end
    end
    if gt_idx < length(groundTruth)
        delta = (groundTruth(gt_idx) - shifted_found)*24*3600;
        if abs(delta) <= max_delta
            det_good(found_idx) = true;
            gt_good(gt_idx) = true;
        end
    end
    found_idx = found_idx + 1;
end
end

