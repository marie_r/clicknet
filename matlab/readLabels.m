

tlab_fldr = '/mar/home/slindeneau/Desktop/deepclick/tlabs/';
mat_fldr = '/cache/slindeneau/6-2018/ASA-5-GT/';

% tlab_file = 'CINMS17B_1foundDetections_';
% mat_file = 'CINMS_B_111226_000500_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat';
% tlab_file = 'CINMS18B_2foundDetections_';
% mat_file = 'CINMS_B_120415_000731_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat';
% tlab_file = 'DCPP01B_1foundDetections_';
% mat_file = 'DCPP_B_121211_001428_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat';
tlab_file = 'DCPP01B_2foundDetections_';
mat_file = 'DCPP_B_130306_001722_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat';


thresholds = [0.2,0.5,0.7,0.8,0.9];
max_deltas = [0.001,0.01,0.1,0.3,0.5,1];


load([mat_fldr mat_file]);
precision = zeros(length(thresholds),length(max_deltas));
recall = zeros(length(thresholds),length(max_deltas));
for ti = 1:length(thresholds)
    
    t = thresholds(ti);
    tfile = [tlab_fldr tlab_file num2str(t) '_.tlab'];
    found = ioReadLabelFile(tfile,'Binary',true);
    for mdi = 1:length(max_deltas)
        md = max_deltas(mdi);
        fprintf('Working on t: %f delta: %f ',t,md);
        [good_det, good_gt] = findGoodDetections(found,allLabels,md);
        precision(ti,mdi) = sum(good_det)/length(found);
        recall(ti,mdi) = sum(good_gt)/length(allLabels);
        fprintf('p: %f, r: %f\n',precision(ti,mdi),recall(ti,mdi));
    end
end
