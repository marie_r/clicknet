function tpws2tlab(infile, outfile, duration_s)
% tpws2tlab(infile, outfile, duration_s)
% Given a Triton Remora clustertool id file, write out a Triton binary
% label file.
%
% Clustertool ids do not have an end time, provide
% default duration_s of .0005 if not specified.
%
% Clustertool creates a Matlab file with:
% zID - Nx2 - N detections, col 1 is start time, col 2 is 

narginchk(2,3);

default_s = 0.0005;
if nargin < 3
  duration_s = default_s;
end
duration_day = duration_s / (24*3600);


% Triton epoch
% Triton dates relative to this timestamp
epoch = datenum(2000, 0, 0);  

tpws = load(infile);

% Adjust for Triton epoch
tpws.MTT = tpws.MTT - epoch;

if isfield(tpws, 'Pscore')
    % score was provided
    labels = sprintf('%.2f-click ', tpws.Pscore);
    labels = split(labels);
else
    labels = 'click';
end
    
% Write out file
% Add duration to start

ioWriteLabel(outfile, [tpws.MTT, tpws.MTT+duration_day], ...
             labels, 'Binary', true);



