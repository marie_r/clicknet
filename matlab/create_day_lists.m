function create_day_lists(starteff, stopeff, everyN)
% create_day_lists(start, stop)
% Given start and stop as serial dates or yyyy-mm-dd HH:MM:SS.FFF,
% create XML tags for start and end days between the two dates
% If everyN is specified, samples one day everyN days starting with
% the start time


narginchk(2,3)
if nargin < 3
    everyN = 1;  % Every day
end

ms = datenum(0,0,0,0,0,.001);  % one ms

fmt = 'yyyy-mm-dd HH:MM:SS.FFF';
if isstr(starteff)
    starteff = datenum(start, fmt);
end
if isstr(stopeff)
    stopeff = datenum(stop, fmt);
end

% Add a ms to the start if the record begins at midnight
if starteff == ceil(starteff)
    start = starteff + ms;
else
    start = ceil(starteff);  % Start at midnight of next day
end

% end at midnight of day before, 1 ms earlier if already at midnight
if stopeff == floor(stopeff)
    stop = stopeff - ms;
else
    stop = floor(stopeff);    
end

% Determine number of days and preallocate
days = ceil(stop - start);
starts = zeros(1, days);
stops = zeros(1, days);

effidx = 0;
for didx = 1:days
    if rem(didx - 1, everyN) == 0
        effidx = effidx + 1;
        starts(effidx) = start;
        % next start and current end
        stops(effidx) = floor(start) + 1;
    end
    start = floor(start)+1;
end

% Convert to ISO8601
isostarts = dbSerialDateToISO8601(starts);
isostops = dbSerialDateToISO8601(stops);
if ~iscell(isostarts)
    isostarts = {isostarts};
end
if ~iscell(isostops)
    isostops = {isostops};
end

fprintf('       <!-- %d days on interval (%s,%s)-->\n', effidx, ...
    dbSerialDateToISO8601(starteff), dbSerialDateToISO8601(stopeff));
fprintf('  		<start type="list(datetime)">\n');
N=3;  % days per row
for idx = 1:N:effidx
    last = min(idx+N-1, effidx);
    fprintf('        %s', ...
        strjoin(isostarts(idx:last), ','));
    if last < effidx
        fprintf(',\n');
    else
        fprintf('\n');
    end
end
fprintf('       </start>\n');
fprintf('  		<end type="list(datetime)">\n');
for idx = 1:N:effidx
    last = min(idx+N-1, effidx);
    fprintf('        %s', ...
        strjoin(isostops(idx:last), ','));
    if last < effidx
        fprintf(',\n');
    else
        fprintf('\n');
    end
end
fprintf('  		</end>\n');
