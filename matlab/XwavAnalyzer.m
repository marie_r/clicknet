classdef XwavAnalyzer < handle
    % XwavAnalyzer(filenames)
    % Class for analyzing XWAV files.
    % Reads XWAV (proprietary Scripps Institution of Oceanography data
    % format that extends Microsoft RIFF WAV files with a custom chunk)
    % files and can be used to determine properties of the file.
    %
    % Example:
    % xinfo = XWavAnalyzer('SOCAL34N_sitN_090822_110345.df20.x.wav')
    % or 
    % xinfo = XWavAnalyzer({'f1.x.wav', 'f2.x.wav', ... |)
    %
    % xinfo(idx) will contain analysis of the idx'th file
    %
    % The following properties are available as Matlab serial dates
    %   startUTC - starting time of file (universal coordinated time)
    %   statDataUTC - start time of each contiguous block of data
    %
    % The following properties are in s of data relative to the start
    % of file.
    %   startdata_s - The start time of each contiguous chuck of data.
    %   durationData_s - Duration of each contiguous block.
    %   durationGap_s - Amount of time between this contiguous block and
    %        the next one.
    %
    % Methods
    %
    % contiguous() - Returns true if data is contiguous without any
    %       recording gaps.
    % decktest_occurred() - It is common to turn an instrument on to ensure
    %       that it is working before deployment.  This can result in data
    %       that should not be analyzed. decktest_P is set to true when it
    %       is believed that the first contiguous data segment was recorded
    %       on deck and should not be analyzed.  Returns true|false
    % indecktest(time_s) - Given an offset in s from the start of file (not
    %       accounting for gaps), does this time fall within a decktest
    %       period?
    % s_toUTC(s) - Given an offset in s from the start of file (not
    %       accounting for gaps), convert to a Matlab serial date in UTC
    % fitUTC(utc) - Given a serial date in UTC that was computed as if
    %       there were no gaps in the file, repair the timestmap to account
    %       for any gaps.
    % report() - Provide a report about the file.
    
    properties (Access=public)
        startUTC        % start of file in UTC
        
        % The data may consist of multiple chunks of contiguous data
        % Breaks in the data stream can occur due to running tests
        % on deck before deployment, duty cycled data, or simply errors
        % in the recording system.
        %
        % The following properties are vectors showing the offset in s
        % into the data stream for each chunk and the corresponding
        % timestamp.
        startData_s     % Offset in s from the first sample of data
        durationData_s  % Duration of each contiguous block in s
        durationGap_s   % Offset between contiguous blocks in s
        startDataUTC    % start of usable data blocks in UTC
        endDataUTC      % end of usable data blocks in UTC
        
        % Information about contiguity of file
        decktest_P   % First contig of data was a deck test? 
        contiguous_P % Is the entire file contiguous?

        hdr         % X wav header (primarily for debugging)
        filename    % Name of file
        
        s_per_day = 3600*24;  % Number of seconds in a day
        
        datefmt = 'YYYY-mm-DD HH:MM:SS.FFF';  % datestr format string
        
        status = {};     % Information about anomalies
        
        % Anything with a gap this long is considered to be a deck test as
        % the instrument is usually programmed to turn off for a while to
        % provide time for instrument to deploy.
        % This is just a heuristic and a duty cycle with a value longer
        % than this would incorrectly predict a deck test.  Decktests with
        % a shorter duration will fail.
        decktest_gap_thresh_s = 3600;
        dutycycle_s = [];  % empty for non duty cycled, otherwise on off 
        dutycycle_min = 1/20;  
    end
    
    methods
        
        function obj = XwavAnalyzer(filenames)
            % XwavAnalzyer(filenames)            
            % Used to detect gaps in the recording and identify sections
            % of the recording that were in decktest
            
            % Replace this with call to Tethys to find out if it is
            % supposed to be dutycycled...
            dutycycle_min = 10;  % minimum number of cycles for duty cycled data
            
            if nargin < 1
                % When calling this constructor wi.th a list of filenames,
                % the loop that processes each file will allocate a new obj
                % resulting in the constructor being called recursively
                % without arguments.  Let the caller handle initialization
                % of properties.
                return
            else
                if ischar(filenames)
                    filenames = {filenames};  % ensure cell array
                elseif length(filenames) > 1
                    obj(length(filenames)).filename = 'temp'; % preallocate
                end
            end
            
            % Anything with an N s difference in timestamps is
            % considered a data gap.  
            gapThresh = datenum(0, 0, 0, 0, 0, 1);  % Yr Mn Dy HH MM SS
            
            for idx = 1:length(filenames)
                obj(idx).filename = filenames{idx};
                obj(idx).status = {};  % information about file
                hdr = ioReadXWAVHeader(filenames{idx}); %#ok<*PROP>
                
                % Get UTC starting time (xwav relative to its own epoch)
                obj(idx).startUTC = dateoffset() + hdr.raw.dnumStart(1);
                
                % See if there is more than a 1 second gap anywhere
                gapDelta = hdr.raw.dnumStart(2:end) - hdr.raw.dnumEnd(1:end-1);
                negativeDelta = find(gapDelta < -gapThresh);

                % Compute duration of raw files from number of bytes
                durations_s = hdr.xhd.byte_length/hdr.xhd.ByteRate;
                % Any raw files with a different number of samples?
                Eduration_s = mode(durations_s);
                anomalies = find(durations_s ~= Eduration_s);
                if ~ isempty(anomalies)
                    for aidx = 1:length(anomalies)
                        obj(idx).status{end+1} = sprintf( ...
                            'raw file %4d duration: %.2f s expected %2.f s', ...
                            durations_s(aidx), Eduration_s);
                    end
                end
                
                if ~ isempty(negativeDelta)
                    % the raw file after the gap has a bad header as it
                    % appears to have been recorded before its predecessor.
                    
                    % Check to see if the data appears to be contiguous
                    % by looking to see if headers return to normal soon
                    
                    for nidx = negativeDelta
                        if nidx > 1 && nidx < length(hdr.raw.dnumEnd) && ...
                            hdr.raw.dnumStart(nidx+1) - ...
                                    (hdr.raw.dnumEnd(nidx-1) + ...
                                     Eduration_s/obj(idx).s_per_day) < gapThresh
                                 % Raw file of nidx-1 is bad

                                 % Assume current raw file header bad
                                 % Ignore any gap occurring at nidx-1 and
                                 % set gap at nidx to zero.
                                 gapDelta(nidx-1) = 0;
                                 gapDelta(nidx) = 0;
                        
                                 obj(idx).status{end+1} = sprintf(...                                     
                                     'raw file %d end after next start: %s > %s', ...
                                     nidx, ...
                                     datestr(hdr.raw.dnumEnd(nidx), obj.datefmt), ...
                                     datestr(hdr.raw.dnumStart(nidx+1), obj.datefmt));
                        end
                    end
                end
                gapP =  gapDelta > gapThresh;
                gapN = sum(gapP);
                switch gapN
                    case 0
                        % no gaps, excellent
                        obj(idx).contiguous_P = true;
                        obj(idx).startData_s = 0;
                        obj(idx).startDataUTC = obj(idx).startUTC;
                        obj(idx).durationData_s = sum(hdr.xhd.byte_length) / ...
                            hdr.xhd.ByteRate;
                        obj(idx).endDataUTC = obj(idx).startUTC + ...
                            obj(idx).durationData_s / obj(idx).s_per_day;
                        obj(idx).decktest_P = false;
                    otherwise                        
                        gaps = find(gapP > 0);   % Find raw files with gaps
                        obj(idx).contiguous_P = false;
                        
                        % Calculate number of bytes to each raw file
                        % We don't use header timestamps to due limitations
                        % of precision (1 ms)
                        cumbytes = cumsum(hdr.xhd.byte_length)';
                        cum_s =  cumbytes / hdr.xhd.ByteRate;  % to s
                        
                        % Note the physical offset into the data stream
                        % for each block
                        obj(idx).startData_s = [0; cum_s(gaps)];
                        % Note starting UTC for each block
                        starttm = hdr.raw.dnumStart(1) + dateoffset();
                        obj(idx).startDataUTC = [starttm;
                            hdr.raw.dnumStart(gaps+1)' + dateoffset()];
                        % Note duration of each spang of contiguous data 
                        obj(idx).durationData_s = [cum_s(gaps); cum_s(end) - cum_s(gaps(end))];
                        % Note duration of gap between data spans
                        deltatm = diff(obj(idx).startDataUTC);
                        obj(idx).durationGap_s = ...
                            deltatm*obj(idx).s_per_day - obj(idx).durationData_s(1:end-1);
                        
                        % Note end time based on duration.
                        obj(idx).endDataUTC = obj(idx).startDataUTC + ...
                            obj(idx).durationData_s / obj(idx).s_per_day;
                        
                        % Deck test heuristic:
                        % decktests occur when there is more than
                        % an hour recording gap between the first set of
                        % contiguous data and the second one.
                        obj(idx).decktest_P = obj(idx).durationGap_s(1) > 3600;
                        if obj(idx).decktest_P
                             days = obj(idx).durationGap_s(1) / obj(idx).s_per_day;
                             hhmmss = datestr(days, 'HH:MM:SS.FFF');
                             obj(idx).status{end+1} = sprintf( ...
                                 'data block %s - %s followed by possible decktest gap of %.3f s (%d days %s)', ...
                                 datestr(obj(idx).startDataUTC(1), obj(idx).datefmt), ...
                                 datestr(obj(idx).startDataUTC(1) + ...
                                   obj(idx).durationData_s(1)/obj(1).s_per_day, ...
                                   obj(idx).datefmt), ...
                                 obj(idx).durationGap_s(1), ...
                                 floor(days), hhmmss);
                             obj(idx).status{end+1} = sprintf('next start %s', ...
                                 datestr(obj(idx).startDataUTC(2), obj(idx).datefmt));
                        end
                        
                        % Attempt to detect duty cycle.
                        % If there are more than a couple gaps, see if they
                        % are consistent as this might be
                        % See if gaps are consistent.
                        GapsN = length(obj(idx).durationGap_s);
                        if obj(idx).decktest_P
                            GapsN = GapsN - 1;  % decktest, don't care
                        end
                        % We will assume that duty cycled data records
                        % for at least one raw file with a duty cycle
                        % greater than some minimum duty cycle
                        if GapsN >= dutycycle_min 
                            oncycle = mode(obj(idx).durationData_s);
                            offcycle = mode(obj(idx).durationGap_s);
                            obj(idx).dutycycle_s = [oncycle, offcycle];
                            obj(idx).status{end+1} = sprintf( ...
                                'Duty cycled %s s on, %s s off', ...
                                oncycle, offcycle);
                            
                            % Find any gaps that do not match duty cycle.
                            baddiscont = false(length(obj(idx).durationData_s)-1, 1);
                            for cidx = 1:length(obj(idx).durationData_s)-1
                                if cidx == 1 && obj(idx).decktest_P
                                    continue
                                else
                                    % TODO - check duty cycle
                                    if true
                                        baddiscont(cidx) = true;
                                    end
                                end
                            end
                        else
                            % Not duty cycled.
                            % All discontinuties are bad except perhaps
                            % the decktest
                            baddiscont = true(length(obj(idx).durationData_s)-1, 1);
                           if obj(idx).decktest_P
                                baddiscont(1) = false;
                            else
                                baddiscont(1) = true;
                           end
                        end
                        for cidx = 1:length(baddiscont)
                            if baddiscont(cidx)
                                days = obj(idx).durationGap_s(cidx) / obj(idx).s_per_day;
                                hhmmss = datestr(days, 'HH:MM:SS.FFF');
                                obj(idx).status{end+1} = sprintf( ...
                                    'Unexpected gap of %.3f s (%d days %s) after data block %s - %s Next block starts %s', ...
                                    obj(idx).durationGap_s(cidx), ...
                                    floor(days), hhmmss, ...
                                    datestr(obj(idx).startDataUTC(cidx), obj(idx).datefmt), ...
                                    datestr(obj(idx).startDataUTC(cidx) + ...
                                        obj(idx).durationData_s(cidx)/obj(idx).s_per_day, obj(idx).datefmt), ...
                                    datestr(obj(idx).startDataUTC(cidx+1), obj(idx).datefmt));
                            end
                        end
                        
                end
                obj(idx).hdr = hdr;
            end         
        end
        
        function utc = s_toUTC(obj, s)
            % utc = s_toUTC(obj, s)
            % Convert s offset into data into a timestamp
            % s may be a scalar, vector, or matrix
            
            % Find out which span each offset is in
            [~, spanidx] = histc(s, [obj.startData_s; Inf]);
            
            if ~isscalar(s) && size(s, 1) == 1
                % Special case for row vectors as indexing will not
                % produce a row
                utc = zeros(length(s), 1);
                utc = obj.startDataUTC(spanidx) + ...
                    (s - obj.startData_s(spanidx)) ./ obj.s_per_day;
            else
                utc = zeros(size(s));  % preallocate timestamp space
            
                utc = obj.startDataUTC(spanidx) + ...
                    (s - obj.startData_s(spanidx)) ./ obj.s_per_day;
            end
        end
        
        function p = decktest_occurred(obj)
            % Did a deck test occur
            p = obj.decktest_P;
        end
        
        function p = contiguous(obj)
            % True if data is entirely contiguous
            p = obj.contiguous_P;
        end
        
        function p = indecktest(obj, time_s)
            % p = indecktest(time_s)
            % Return true if offset into file occurred during the decktest
            p = obj.decktest_P == true && time_s <= obj.startData_s(2);
        end
        
        function utc = fixUTC(obj, utc)
            % utc = fixUTC(timestamps)
            % Timestamps computed on files that are not contiguous
            % with the assumption of continuity will be erroneous.
            % Assuming that all timestamps are relative to the file
            % start time, repair the timestamps.
           
            if ~ obj.contiguous_P
                % Convert times into s offsets
                offset_s = (utc - obj.startUTC) .* obj.s_per_day;
                utc = obj.s_toUTC(offset_s);
            end
        end
        
        function report(obj)
            % report() - print report on file
            if ~ isempty(obj.status)
                fprintf('%s header report\n  ', obj.filename);
                fprintf('%s\n', strjoin(obj.status, '\n  '));
            end
        end
            
        function contiguous_with_previous(obj, prev)
            % contiguous_with_previous(prev)
            % Given an XwavAnalyzer object that preceded this one,
            % are the data contiguous or is there a gap
            sample_gap = 1 / obj.hdr.fs; 
            if obj.startDataUTC(1) - prev.endDataUTC(end) > sample_gap
                fprintf('gap between files: stop %s start %s\n', ...
                    datestr(prev.endDataUTC(end), obj.datefmt), ...
                    datestr(obj.startDataUTC(1), obj.datefmt));
            end
        end
    end
end
