'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from SampleExtractor import SampleExtractor
from models import FeedForward

def driver():
    numSecondsToPredict = 5*60
    predictAbsTimeStart = datetime(2011,7,18,7,13,0)

    train_labels_path = '/mar/lab/corpora/siodeteditclicks/GofMX_DT03_disk01_DolphinLabels.mat'
    train_xwav_path = '/media/internal/GofMX_DT03_disk01'
    Fs = 200000
    N = 200
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    
    '''Creating separate training and testing datasets for model'''
    
    #Training data
    data = []
    labels = []
    for i in range(0,50000):    #click count range
        samps = sampEx.getSamplesForClickIdx(i)
        if(len(samps) != 200):
            print(i)
            continue
        data.append(samps[0:100])
        labels.append(True)
        data.append(samps[100:200])
        labels.append(False)
        
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    labels = np_utils.to_categorical(labels, 2)   
    
    x_train = np.array(data)
    y_train = np.array(labels)
    
    #Testing data
    data = []
    labels = []
    for i in range(1500000,1550000):    #click count range
        samps = sampEx.getSamplesForClickIdx(i)
        if(len(samps) != 200):
            print(i)
            continue
        data.append(samps[0:100])
        labels.append(True)
        data.append(samps[100:200])
        labels.append(False)
    
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    labels = np_utils.to_categorical(labels, 2)
    
    x_test = np.array(data)
    y_test = np.array(labels)
    #FeedForward(x_train, x_test, y_train, y_test)    

    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    nn = FeedForward(x_train=x_train, x_test=x_test, y_train=y_train, y_test=y_test,
                      epochs=5, pred_data=None)
    predictions = nn.NNtrain()
    
    precision, recall, thresholds = precision_recall_curve(y_test[:,1], predictions[:,1])
    plt.plot(recall,precision)
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim([0,1.1])
    plt.xlim([0,1.1])
    for i in range(0,len(thresholds),math.floor(len(thresholds)*.1)):
        plt.annotate("{:.3f}".format(thresholds[i]),xy=(recall[i],precision[i]))
    plt.title('PR by threshold')
    #print(classification_report(y_test,predictions))
    
if __name__ == '__main__':
    driver()