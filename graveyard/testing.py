'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from SampleExtractor import SampleExtractor
from models import FeedForward

def driver():
    numSecondsToPredict = 5*60
    predictAbsTimeStart = datetime(2011,7,18,7,13,0)

    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/CINMS_B_111226_000500_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
    Fs = 200000
    N = 200
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    
'''
    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    #print(classification_report(y_test,predictions))
'''   
if __name__ == '__main__':
    driver()