from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib import animation as pltanimation

from SampleExtractor import SampleExtractor
from models import FeedForward

def pltgood(se,at,good,idx):
    ''' '''
    time = se.cvtIdxToAbsTimeRelativeToAbsTime(at, good[idx]*100)
    t = se.getSamplesForAbsoluteTime(time,.001)
    plt.clf()
    plt.plot(t)
    plt.title(str(time))
    

def driver():
    path = '/mar/lab/corpora/siodeteditclicks/GofMX_DT03_disk01_DolphinLabels.mat'
    Fs = 200000
    N = 200
    epochs = 20
    numclicksintraining = 20000
    numSecondsToPredict = 5*60
    numShipSeconds = 30
    knownShipTime = []#[datetime(2011,7,18,0,53,37),datetime(2011,7,18,0,59,37),datetime(2011,7,18,1,7,50),datetime(2011,7,18,1,18,50),datetime(2011,7,18,1,21,47),datetime(2011,7,18,1,29,30),datetime(2011,7,18,1,40,0)]
    predictAbsTimeStart = datetime(2011,7,18,7,13,0)

    sampEx = SampleExtractor.build('/media/GofMXDT03/GofMX_DT03_disk01',path,Fs,.001)
    
    
    #t = sampEx.getSamplesForAbsoluteTime(sampEx.cvtIdxToAbsTimeRelativeToAbsTime(predictAbsTimeStart, 22*100), .01)
    #plt.plot(t)
    #temp=sampEx.getSamplesForClickIdx(0)
    #print(temp)
    #plt.plot(temp)
    #plt.ylabel("random")
    
    #Neural Network prep
    data = []
    labels = []
    for i in range(0,numclicksintraining):
        samps = sampEx.getSamplesForClickIdx(i)
        if(len(samps) != 200):
            print(i)
            continue
        data.append(samps[0:100])
        labels.append(True)
        data.append(samps[100:200])
        labels.append(False)
    
    
    
    le = LabelEncoder()
    cat_labels = le.fit_transform(labels)
    cat_labels = np_utils.to_categorical(cat_labels, 2)
    
    
    x_train = np.array(data)
    y_train = np.array(cat_labels)
    
    print('Getting known ship data')
    for kst in knownShipTime:
        known_ship_data = sampEx.getSamplesForAbsoluteTime(kst,numShipSeconds)
        known_ship_data = known_ship_data.reshape((int(len(known_ship_data)/100),100))
        x_train = np.concatenate((x_train,known_ship_data))
        y_train = np.concatenate((y_train,np_utils.to_categorical(np.zeros(numShipSeconds*2000),2)))
    '''
    data = []
    labels = []
    for i in range(1000000,1600000):
        samps = sampEx.getSamplesForClickIdx(i)
        if(len(samps) != 200):
            print(i)
            continue
        data.append(samps[0:100])
        labels.append(True)
        data.append(samps[100:200])
        labels.append(False)
    
    
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    labels = np_utils.to_categorical(labels, 2)
    
    
    x_test = np.array(data)
    y_test = np.array(labels)
    '''
    
    '''
    (x_train, x_test, y_train, y_test) = train_test_split(
    data,labels, test_size=0.20)
    
    x_train = np.array(x_train)
    x_test = np.array(x_test)
    y_train = np.array(y_train)
    y_test = np.array(y_test)
    '''
    
    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    predictions = FeedForward(x_train=x_train, x_test=None, y_train=y_train, y_test=None, epochs=5, pred_data=pred_data).NNtrain()
    
    good = []
    for i in range(0,len(predictions)):
        if(predictions[i][1] > .8):
            good.append(i)
    
    
    goodAST = [sampEx.cvtIdxToAbsTimeRelativeToAbsTime(predictAbsTimeStart, x*100) for x in good]
    
    goodClickIdx = [sampEx.getClickIdxForAbsTime(x) for x in goodAST]
    badClicks = [goodAST[i] for i in range(0,len(goodClickIdx)) if goodClickIdx[i] == None]
            
    FFMpegWriter = pltanimation.writers['ffmpeg']
    metadata = dict(title='Dense Layer Predicted Clicks', artist='Dense Layer',
                    comment='2x Dense Layer Click Predictions.')
    writer = FFMpegWriter(fps=3, metadata=metadata)
    fig = plt.figure()
    with writer.saving(fig, "dense_click_prediction.mp4", 1000):
        for i in range(0,1000):
            pltgood(sampEx,predictAbsTimeStart,good,i)
            writer.grab_frame()
    
    print(predictions)
    pltgood(sampEx,predictAbsTimeStart,good,0)
    '''
    (loss, accuracy) = model.evaluate(x_test, y_test,
    batch_size=128, verbose=1)
    print("[INFO] loss={:.4f}, accuracy: {:.4f}%".format(loss,
    accuracy * 100))
    '''
    '''
    tdeltas = []
    mat_data = fetchDolphinLabels(path)
    ctimes = convertTimeStamp(mat_data)
    #getMatPaths()
    xtimes,filenames = getWavPaths()
    #idx = getWavFileIdx(mtimes[0],xtimes)
    tdeltas = calcDelta(ctimes, xtimes)
    startsample = getSampleIdx(tdeltas[0])
    
    
    print(ctimes[0])
    print(startsample)
    wSamples(filenames[0], startsample)
    '''
    

if __name__ == '__main__':
    driver() 
    