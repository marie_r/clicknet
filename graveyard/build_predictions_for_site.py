'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os.path


from SampleExtractor import SampleExtractor
from TritonLabeler import TritonLabeler
from keras.models import load_model

def build_predictions_for_site(model_name,
                               train_labels_path,
                               train_xwav_path,
                               Fs=200000,
                               window_length_seconds=.001,
                               seconds_to_predict_per_batch = 60,
                               threshold=0.8,
                               band_stop_low=5000,
                               band_stop_high=92000):
    
    __model_fs = 200000
    
    N = int((__model_fs * window_length_seconds)/2)
    
    model = load_model(model_name)  
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    
    #5second standoff from edge of files in stream so that we don't run afoul of the resample/bandpass extra read needed for the fir filters
    predictAbsTimeStart = sampEx.wavDates[0]+timedelta(seconds=5)
    predictAbsTimeStop = sampEx.wavDates[-1]-timedelta(seconds=5)
    
    
    ground_truth_idx = sampEx.getAllClickIdxBetweenTimes(predictAbsTimeStart,predictAbsTimeStop)
    
    
    total_seconds_to_process = (predictAbsTimeStop - predictAbsTimeStart).total_seconds()
    seconds_processed_so_far = 0
    step_count = 0
    total_seconds_taken = 0
    
    all_found_ast = {}    
    thresholds = [0.2,0.5,0.7,0.8,0.9]
    while(predictAbsTimeStart+timedelta(seconds=seconds_to_predict_per_batch) < predictAbsTimeStop):
        targetFs = None
        if Fs != __model_fs:
            targetFs = __model_fs
        start_time = datetime.now()
        #define the data to predict on
        pred_data = sampEx.getBandPassedSamplesForAbsoluteTime(predictAbsTimeStart,band_stop_low,band_stop_high,seconds_to_predict_per_batch,resample_targetFs=targetFs)
        pred_data = pred_data.reshape((int(len(pred_data)/N),N))
        
        predictions = model.predict(pred_data,batch_size=1000,verbose=0)
        for t in thresholds:
            found = [i for i in range(0,len(predictions)) if predictions[i][1] > t]
            foundAST = [sampEx.cvtIdxToAbsTimeRelativeToAbsTime(predictAbsTimeStart, x*N) for x in found]
            if not (t in all_found_ast):
                all_found_ast[t] = []
            all_found_ast[t] += foundAST
            if t == thresholds[-2]:
                last_found_ast = foundAST
        step_count += 1
        total_seconds_taken += (datetime.now()-start_time).total_seconds()
        avg_time_taken_per_step = total_seconds_taken/step_count
        seconds_processed_so_far += seconds_to_predict_per_batch
        avg_num_seconds_per_step = seconds_processed_so_far/step_count
        seconds_left_to_process = total_seconds_to_process - seconds_processed_so_far
        est_remaining_time = timedelta(seconds=(seconds_left_to_process/avg_num_seconds_per_step)*avg_time_taken_per_step)
        print("\rTotal: "+str(len(all_found_ast[thresholds[-2]]))+", Current: "+str(len(last_found_ast)) + ", "+str(predictAbsTimeStart) + ", Estimated  processing time remaining: "+str(est_remaining_time),end="")
        predictAbsTimeStart += timedelta(seconds=seconds_to_predict_per_batch)
    
    
    
    fname = os.path.basename(os.path.normpath(train_xwav_path))
    
    precision = []
    recall = []
    for t in thresholds:
        # map the predicted clicks back to an index into all candidate clicks
        foundClickIdx = [sampEx.getClickIdxForAbsTime(x) for x in all_found_ast[t]]
        
        # identify the good and bad detections based on ground truth
        badClicks = [all_found_ast[t][i] for i in range(0,len(foundClickIdx)) if foundClickIdx[i] == None]
        goodClicks = [all_found_ast[t][i] for i in range(0,len(foundClickIdx)) if foundClickIdx[i] != None]
        
        # Write out a Triton label file for the correctly detected clicks
        tl = TritonLabeler(fname+"truePositive_"+str(t)+"_.tlab")
        tl.write(goodClicks,goodClicks)
        
        #truePositiveIdx = [x for x in foundClickIdx if x != None]
        if(len(found) > 0):
            precision.append(len(goodClicks)/len(foundClickIdx))
        else:
            precision.append(1)
        if(len(ground_truth_idx) > 0):
            recall.append(len(goodClicks)/len(ground_truth_idx))
        else:
            recall.append(1)
    
        tl = TritonLabeler(fname+"foundDetections_"+str(t)+"_.tlab")
        tl.write(all_found_ast[t],all_found_ast[t])
    plt.figure()
    plt.plot(recall,precision)
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim([0,1.01])
    plt.xlim([0,1.01])
    plt.title('PR by threshold for '+fname)
    for i in range(0,len(thresholds)):
        plt.annotate("{:.1f}".format(thresholds[i]),xy=(recall[i],precision[i]))
    plt.savefig(fname+"PR.png")
'''
    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    #print(classification_report(y_test,predictions))
'''   
if __name__ == '__main__':
    model_name = 'stupid_2layer_dense2.h5'
    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/CINMS_B_111226_000500_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
    
    build_predictions_for_site(model_name,train_labels_path,train_xwav_path)