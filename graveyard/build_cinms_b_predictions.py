'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from SampleExtractor import SampleExtractor
from TritonLabeler import TritonLabeler
from keras.models import load_model

def driver():
    model_name = 'stupid_2layer_dense.h5'
    #12/26/2011 02:38:02
    numSecondsToPredict = 60*10
    predictAbsTimeStart = datetime(2011, 12, 26, 3, 20 )

    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/CINMS_B_111226_000500_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
    Fs = 200000
    N = 200
    
    model = load_model(model_name)
    
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    
    #define the data to predictDCPP01B on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    ground_truth_idx = sampEx.getAllClickIdxBetweenTimes(predictAbsTimeStart,predictAbsTimeStart+timedelta(seconds=numSecondsToPredict))
    
    
    predictions = model.predict(pred_data,batch_size=1000,verbose=1)
    
    
    thresholds = np.linspace(0.1,.9,9)
    #thresholds = [.7]
    precision = []
    recall = []
    for threshold in thresholds:
        # select detection above current threshold and determine their start times
        found = [i for i in range(0,len(predictions)) if predictions[i][1] > threshold]
        foundAST = [sampEx.cvtIdxToAbsTimeRelativeToAbsTime(predictAbsTimeStart, x*100) for x in found]
        
        # Write out a Triton label file for the current threshold.  Labels show detected clicks
        tl = TritonLabeler("foundDetections_"+str(threshold)+"_.tlab")
        tl.write(foundAST,foundAST)
        
        # map the predicted clicks back to an index into all candidate clicks
        foundClickIdx = [sampEx.getClickIdxForAbsTime(x) for x in foundAST]
        
        # identify the good and bad detections based on ground truth
        badClicks = [foundAST[i] for i in range(0,len(foundClickIdx)) if foundClickIdx[i] == None]
        goodClicks = [foundAST[i] for i in range(0,len(foundClickIdx)) if foundClickIdx[i] != None]
        
        # Write out a Triton label file for the correctly detected clicks
        tl = TritonLabeler("truePositive_"+str(threshold)+"_.tlab")
        tl.write(goodClicks,goodClicks)
        
        truePositiveIdx = [x for x in foundClickIdx if x != None]
        if(len(found) > 0):
            precision.append(len(truePositiveIdx)/len(found))
        else:
            precision.append(1)
        if(len(ground_truth_idx) > 0):
            recall.append(len(truePositiveIdx)/len(ground_truth_idx))
        else:
            recall.append(1)
        
    plt.figure()
    plt.plot(recall,precision)
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim([0,1.01])
    plt.xlim([0,1.01])
    plt.title('PR by threshold')
    for i in range(0,len(thresholds)):
        plt.annotate("{:.1f}".format(thresholds[i]),xy=(recall[i],precision[i]))
    plt.show()
    print("done!")
'''
    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    #print(classification_report(y_test,predictions))
'''   
if __name__ == '__main__':
    driver()