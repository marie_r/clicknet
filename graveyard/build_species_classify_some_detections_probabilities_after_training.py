'''
Created on Aug 23, 2018

@author: jmerindol
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
from sklearn.pipeline import Pipeline
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from keras import regularizers
from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense
from keras import backend 
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import MinMaxScaler

from SampleExtractor import SampleExtractor
import csv
import dateutil.parser
from builtins import input
from docutils.nodes import row
from collections import defaultdict
from os import listdir

from encounter_groups import SpeciesGroups

''' Model Probabilities '''
    
model_name = 'probabilities_species_classify.h5'    
x_prob = []
y_prob = model.predict(x_prob)  
x_prob, y_prob = make_blobs(n_samples=100, centers=2, n_features=2, random_state=1)
scalar = MinMaxScaler()
scalar.fit(x_prob)
x_prob = scalar.transform(x_prob)
sp_guesses = np.empty()

data_training = []
labels_training = []
data_testing = []
labels_testing = []
prob_data = defaultdict(list) 

for label in sp_data.keys():
    all_data = sp_data[label]
    good_data = []
    for an_encounter in all_data:
        if len(an_encounter) > 0:
            #This is a good encounter
            good_data.append(an_encounter)
    # Split list of encounters into # of training and test encounters
    train_amt = int(round(len(good_data)*test_train_split))
    test_amt = len(good_data) - train_amt
    this_train_data = good_data[:train_amt]
    if(len(this_train_data) > 0):
        this_train_data = list(np.concatenate(np.asarray(this_train_data)))
        labels_training += [label] * len(this_train_data)
        data_training += this_train_data
     
    this_test_data = good_data[train_amt:]
    if(len(this_test_data)>0):     
        for prob_label in all_groups["CINMS17B_1.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["CINMS17B_1.mat"].get_encounters_for_species(prob_label)
        for prob_label in all_groups["CINMS18B_2.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["CINMS18B_2.mat"].get_encounters_for_species(prob_label)
        for prob_label in all_groups["CINMS18C_1.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["CINMS18C_1.mat"].get_encounters_for_species(prob_label)
        for prob_label in all_groups["CINMS19C_2.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["CINMS19C_2.mat"].get_encounters_for_species(prob_label)
    #     for sp_label in all_groups["DCPP01A_1.mat"].get_species_labels():
    #         sp_data[prob_label] += all_groups["DCPP01A_1.mat"].get_encounters_for_species(prob_label)
    #     for sp_label in all_groups["DCPP01A_2.mat"].get_species_labels():
    #         sp_data[prob_label] += all_groups["DCPP01A_2.mat"].get_encounters_for_species(prob_label)
    #     for sp_label in all_groups["DCPP01B_1.mat"].get_species_labels():
    #         sp_data[prob_label] += all_groups["DCPP01B_1.mat"].get_encounters_for_species(prob_label)
    #     for sp_label in all_groups["DCPP01B_2.mat"].get_species_labels():
    #         sp_data[prob_label] += all_groups["DCPP01B_2.mat"].get_encounters_for_species(prob_label)
    #     for sp_label in all_groups["DCPP01C_1.mat"].get_species_labels():
    #         sp_data[prob_label] += all_groups["DCPP01C_1.mat"].get_encounters_for_species(prob_label)
    #     for sp_label in all_groups["DCPP01C_2.mat"].get_species_labels():
    #         sp_data[prob_label] += all_groups["DCPP01C_2.mat"].get_encounters_for_species(prob_label)
        for prob_label in all_groups["SOCAL32E_1.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["SOCAL32E_1.mat"].get_encounters_for_species(prob_label)
        for prob_label in all_groups["SOCAL33E_2.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["SOCAL33E_2.mat"].get_encounters_for_species(prob_label)
        for prob_label in all_groups["SOCAL35R_1.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["SOCAL35R_1.mat"].get_encounters_for_species(prob_label)
        for prob_label in all_groups["SOCAL38R_2.mat"].get_species_labels():
            prob_data[prob_label] += all_groups["SOCAL38R_2.mat"].get_encounters_for_species(prob_label)
        this_test_data = list(np.concatenate(np.asarray(this_test_data)))
        data_testing += this_test_data
        labels_testing += [label] * len(this_test_data)
                
 
    le = LabelEncoder()
    le.fit(labels_training + labels_testing)
    labels_training_fit = le.transform(labels_training)
    labels_testing_fit = le.transform(labels_testing)
    num_cat = max(labels_training_fit)+1
    labels_training_cat = np_utils.to_categorical(labels_training_fit, num_cat) 
    labels_testing_cat = np_utils.to_categorical(labels_testing_fit, num_cat)  
     
    x_train = np.array(data_training)
    x_test = np.array(data_testing)
    y_train = np.array(labels_training_cat)
    y_test = np.array(labels_testing_cat)    
    model = Sequential()
    model.add(Dense(200, input_dim=200, activation="relu",kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(200, activation="relu",kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(num_cat))
    model.add(Activation("softmax"))
    
    model.compile(optimizer = "Adam",  
                  loss = "binary_crossentropy",
                  metrics = ['categorical_accuracy'])
    
    model.fit(x=x_train, y=y_train, validation_data=(x_test,y_test), epochs=epochs, batch_size=batch_size)
    #     predictions = model.predict(x_test)
    model.save(model_name)
    print("Done Training!")
    
    
    # This is currently broken
    # 
    for label in prob_data.keys():
    #for each species in the validation data
        all_data = prob_data[label]
        good_prob_data = []
        for an_encounter in all_data:
        #for each acoustic encounter in that species:
            if len(an_encounter) > 0:
                #This is a good encounter
                good_data.append(an_encounter)
            prob = model.predict(x_prob, batch_size=batch_size, verbose=0, steps=steps)
            #model predict for the detections in that acoustic encounter
            log_prob = np.log(prob)
            #calculate the loglikelihood values for each species across all of the results
            sp_index = np.argmax(log_prob)
            #choose a single species label for this acoustic encounter
            sp_gues = sp_list[sp_index]
            #keep track of the guess, and the actual species
            sp_guesses.append(sp_gues)
            encounter_prob = np.sum(log_prob)
            # Make a decision based on the joint log likelihood of the 
            # class-conditional probability.  We assume a uniform prior
            # and ignore it.
            encounter_label = np.argmax(encounter_prob)

            #calculate the accuracy of the guessses 
            good_prob_data = list(np.concatenate(np.asarray(good_prob_data)))
#             return sum(prob_data[:,"species we want"]) 
           
    model = Sequential()
    model.add(Dense(4, input_dim=2, activation='relu'))
    model.add(Dense(4, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam')
    model.fit(x_prob, y_prob, epochs=500, verbose=0)  
    x_prob, _ = make_blobs(n_samples=3, centers=2, n_features=2, random_state=1)
    x_prob = scalar.transform(x_prob)
    y_prob = model.predict_classes(x_prob)
    for i in range(len(x_prob)):
        print("x_prob=%s, Predicted=%s" % (x_prob[i], y_prob[i]))      
    #driver()
    
    

     
     
     
     
     
     
     
     
     