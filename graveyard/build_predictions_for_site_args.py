'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from SampleExtractor import SampleExtractor
from TritonLabeler import TritonLabeler
from keras.models import load_model

def build_predictions_for_site(model_name,
                               train_labels_path,
                               train_xwav_path,
                               site_name,
                               Fs=200000,
                               window_length_seconds=.001,
                               seconds_to_predict_per_batch = 60,
                               threshold=0.8):
    
    N = int((Fs * window_length_seconds)/2)
    
    model = load_model(model_name)  
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    
    predictAbsTimeStart = sampEx.wavDates[0]+timedelta(seconds=5)
    predictAbsTimeStop = sampEx.wavDates[len(sampEx.wavDates)-1]
    
    
    all_found_ast = []    
    while(predictAbsTimeStart < predictAbsTimeStop):
        #define the data to predict on
        pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,seconds_to_predict_per_batch)
        pred_data = pred_data.reshape((int(len(pred_data)/N),N))
        
        predictions = model.predict(pred_data,batch_size=1000,verbose=0)
        found = [i for i in range(0,len(predictions)) if predictions[i][1] > threshold]
        foundAST = [sampEx.cvtIdxToAbsTimeRelativeToAbsTime(predictAbsTimeStart, x*N) for x in found]
        all_found_ast += foundAST
        print("Total: "+str(len(all_found_ast))+", Current: "+str(len(foundAST)) + ", "+str(predictAbsTimeStart))
        
        predictAbsTimeStart += timedelta(seconds=seconds_to_predict_per_batch)
    
    
    tl = TritonLabeler(site_name+"foundDetections_"+str(threshold)+"_.tlab")
    tl.write(all_found_ast,all_found_ast)
    
'''
    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    #print(classification_report(y_test,predictions))
'''   
if __name__ == '__main__':
    model_name = 'stupid_2layer_dense.h5'
    #site_name = 'SOCAL_E_090605_002615_5_clicks.bcTg_Delphin_TPWS1.mat_TD'
    site_name = 'SOCAL_R_091105_001345_5_clicks.bcTg_Delphin_TPWS1.mat_TD'
    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/'+site_name+'.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/SOCAL_R/SOCAL35R_1/'
    
    build_predictions_for_site(model_name,train_labels_path,train_xwav_path,site_name)