'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense

from SampleExtractor import SampleExtractor

def driver():
    
    model_name = 'stupid_2layer_dense2.h5'
    num_nodes_per_layer = 100
    epochs = 10
    batch_size = 200
    
    num_samples_for_training = 50000
    
    if True:
        train_labels_path = '/mar/lab/corpora/siodeteditclicks/GofMX_DT03_disk01_DolphinLabels.mat'
        train_xwav_path = '/media/internal/GofMX_DT03_disk01'
    else:
        train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/CINMS_C_120407_001556_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
        train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_C/CINMS18_C1/'
    
    Fs = 200000
    N = 200
    print("Setup")
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    #Training data
    data = []
    labels = []
    print("Getting data")
    for i in range(0,num_samples_for_training):    #click count range
        print("\r"+str(i)+"/"+str(num_samples_for_training),end="")
        click_time = sampEx.absoluteStartOfClicks[i]
        samps = sampEx.getBandPassedSamplesForAbsoluteTime(click_time,5000,92000,.001)
        if(len(samps) != 200):
            print("Error, incorrect sample length for detection: "+str(i))
            continue
        data.append(samps[0:100])
        labels.append(True)
        data.append(samps[100:200])
        labels.append(False)
        
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    labels = np_utils.to_categorical(labels, 2)   
    
    x_train = np.array(data)
    y_train = np.array(labels)
    print("Training")
    #
    ''' Model Training '''
    #
    model = Sequential()
    model.add(Dense(num_nodes_per_layer, input_dim=100, activation="relu"))
    model.add(Dense(num_nodes_per_layer, activation="relu"))
    model.add(Dense(2))
    model.add(Activation("softmax"))
    
    model.compile(optimizer = "Adam",  
                  loss = "binary_crossentropy",
                  metrics = ['categorical_accuracy'])
    
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size)
    model.save(model_name)
    
if __name__ == '__main__':
    driver()