'''
Created on Aug 3, 2018

@author: jmerindol
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from keras import regularizers
from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense
 
from SampleExtractor import SampleExtractor
import csv
import dateutil.parser
from builtins import input
from docutils.nodes import row
from collections import defaultdict
from os import listdir

from encounter_groups import SpeciesGroups

# def driver():
#     
#     model_name = 'test_species_classify.h5'
#     num_nodes_per_layer = 100
#     epochs = 10
#     batch_size = 200
#     test_train_split = .8
#     num_samples_for_training = 10000
#     num_samples_for_testing = 10000
#     num_labels_for_training = 300
#     num_labels_for_testing = 300
#     train_labels_path = 'CINMS17B_1.mat'
#     test_labels_path = 'CINMS17B_1.mat'
#     train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
#     test_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
#     csv_files = "/mar/home/jmerindol/Desktop/dclde_csv/CINMS-B.csv"
#     Fs = 200000
#     N = 200
#     sampEx_train = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
#     #Training data_training
#     sampEx_test = SampleExtractor.build(test_xwav_path,test_labels_path,Fs,.001)
#     #Testing data_testing

  
  

#     with open(csv_files) as csvfile: 
#         reader = csv.reader(csvfile,delimiter=",") #lecture des donnees sous forme de tableau. 
#         csv_rows = [] #colonnes sous forme de liste.
#         
#         for row in reader:
#             csv_rows.append(row)
#         csv_row_index = 0
#         row = csv_rows[csv_row_index]
#         row_start = dateutil.parser.parse(row[3]) #quatrieme colonne = depart du signal.
#         row_stop = dateutil.parser.parse(row[4]) #cinquieme colonne = fin du signal.
#         data_training = [] #donnees pour le training. 
#         data_testing = [] #donnees pour le testing. 
#         labels_training = [] #output pour le training.
#         labels_testing = [] #output pour le testing.
#         cur_detection_index = 0
#         data = defaultdict(list)
#         
#         for row in csv_rows:
#             print(row)
#             row_start = dateutil.parser.parse(row[3]) #quatrieme colonne = depart du signal.
#             row_stop = dateutil.parser.parse(row[4]) #cinquieme colonne = fin du signal.
#             num_of_collected_detections_training_so_far = 0 #nombre de detections training collectees au debut du run.
#             num_of_collected_detections_testing_so_far = 0 #nombre de detections testing collectees au debut du run.
#             data[row[2]].append([])
#             while (cur_detection_index != None 
#                    and num_of_collected_detections_training_so_far < num_samples_for_training
#                    and sampEx_train.absoluteStartOfClicks[cur_detection_index] < row_stop): #pour limiter le nombre de samples qu'on analyse pour eviter un surplus de memoire.
# #                 while (cur_detection_index < len(sampEx_train.absoluteStartOfClicks) 
# #                        and sampEx_train.absoluteStartOfClicks[cur_detection_index] < row_start): #on passe toutes les detections avant le debut du labels_training 
# #                     cur_detection_index += 1
#                 if(sampEx_train.absoluteStartOfClicks[cur_detection_index] < row_start):
#                     cur_detection_index = sampEx_train.findFirstClickIdxAfterAbsoluteTime(row_start)
#                 if cur_detection_index == None:
#                     break;  #boucle while : skip toutes les detections en dehors du start et stop d'un label.  
#                 
#                 data[row[2]][-1].append(sampEx_train.getSamplesForClickIdx(cur_detection_index))
#                 num_of_collected_detections_training_so_far += 1


#     for label in data.keys():
#         all_data = data[label]
#         good_data = []
#         for an_encounter in all_data:
#             if len(an_encounter) > 0:
#                 #This is a good encounter
#                 good_data.append(an_encounter)
#         train_amt = int(round(len(good_data)*test_train_split))
#         test_amt = len(good_data) - train_amt
#         this_train_data = good_data[:train_amt]
#         if(len(this_train_data) > 0):
#             this_train_data = list(np.concatenate(np.asarray(this_train_data)))
#             labels_training += [label] * len(this_train_data)
#             data_training += this_train_data
#         
#         this_test_data = good_data[train_amt:]
#         if(len(this_test_data)>0):
#             this_test_data = list(np.concatenate(np.asarray(this_test_data)))
#             data_testing += this_test_data
#             labels_testing += [label] * len(this_test_data)
#         
# 
#     le = LabelEncoder()
#     le.fit(labels_training + labels_testing)
#     labels_training_fit = le.transform(labels_training)
#     labels_testing_fit = le.transform(labels_testing)
#     num_cat = max(labels_training_fit)+1
#     labels_training_cat = np_utils.to_categorical(labels_training_fit, num_cat) 
#     labels_testing_cat = np_utils.to_categorical(labels_testing_fit, num_cat)  
#     
#     x_train = np.array(data_training)
#     x_test = np.array(data_testing)
#     y_train = np.array(labels_training_cat)
#     y_test = np.array(labels_testing_cat)
    
#
''' Model Training '''

if __name__ == '__main__':
    x_train = []
    x_test = []
    y_train = []
    y_test = []
    test_train_split = 0.8
    model_name = 'test_species_classify.h5'
    num_nodes_per_layer = 200
    epochs = 10
    batch_size = 200
    encounter_label_dir = "/mar/home/jmerindol/Desktop/dclde_csv/"
    clickstart_dir = "/mar/home/jmerindol/Desktop/deepclick/model_labels_Matlab/"
    labels2encounter = {"CINMS17B_1.mat":["CINMS-B.csv","CINMS_B/CINMS17B_1/",200000],
                        "CINMS18B_2.mat":["CINMS-B.csv","CINMS_B/CINMS18B_2/",200000], 
                        "CINMS18C_1.mat":["CINMS-C.csv","CINMS_C/CINMS18C_1/",200000], 
                        "CINMS19C_2.mat":["CINMS-C.csv","CINMS_C/CINMS19C_2/",200000],
#                         "DCPP01A_1.mat":["DCPP-A.csv","DCPP-A/DCPP01A_1/",200000],
#                         "DCPP01A_2.mat":["DCPP-A.csv","DCPP-A/DCPP01A_2/",200000],
#                         "DCPP01B_1.mat":["DCPP-B.csv","DCPP-B/DCPP01B_1/",200000],
#                         "DCPP01B_2.mat":["DCPP-B.csv","DCPP-B/DCPP01B_2/",200000],
#                         "DCPP01C_1.mat":["DCPP-C.csv","DCPP-C/DCPP01C_1/",200000],
#                         "DCPP01C_2.mat":["DCPP-C.csv","DCPP-C/DCPP01C_2/",200000]}#,
                        "SOCAL32E_1.mat":["SOCAL-E.csv","SOCAL_E/SOCAL32E_1/",200000],
                        "SOCAL33E_2.mat":["SOCAL-E.csv","SOCAL_E/SOCAL33E_2/",200000], 
                        "SOCAL35R_1.mat":["SOCAL-R.csv","SOCAL_R/SOCAL35R_1/",200000],
                        "SOCAL38R_2.mat":["SOCAL-R.csv","SOCAL_R/SOCAL38R_2/",200000]}
    all_groups = {}
    for start in labels2encounter:
#         encounterfile = labels2encounter[start][0]#                         "DCPP01A_1.mat":["DCPP-A.csv","DCPP-A/DCPP01A_1/",200000],
#                         "DCPP01A_2.mat":["DCPP-A.csv","DCPP-A/DCPP01A_2/",200000],
#                         "DCPP01B_1.mat":["DCPP-B.csv","DCPP-B/DCPP01B_1/",200000],
#                         "DCPP01B_2.mat":["DCPP-B.csv","DCPP-B/DCPP01B_2/",200000],
#                         "DCPP01C_1.mat":["DCPP-C.csv","DCPP-C/DCPP01C_1/",200000],
#                         "DCPP01C_2.mat":["DCPP-C.csv","DCPP-C/DCPP01C_2/",200000],
#         print("encounter file: key:"+start+"\tList0 value:"+labels2encounter[start][0]+"\tList1 value:"+labels2encounter[start][1])
        
        #if start in labels2encounter :
        xwav_dir = '/media/dclde/DCLDE2015_HF_training/'+labels2encounter[start][1]
        groundtruth_file = encounter_label_dir + labels2encounter[start][0]
        detection_file = '/mar/home/jmerindol/Desktop/deepclick/model_labels_Matlab/'+start 
        print("whole_mat: "+str(detection_file)+"\t"+str(groundtruth_file)+"\t"+str(xwav_dir)+"\t"+str(labels2encounter[start][2]))
        cur_group = SpeciesGroups(detection_file,groundtruth_file,xwav_dir,labels2encounter[start][2])
        all_groups[start] = cur_group
#         for xwav_dir in listdir('/media/dclde/DCLDE2015_HF_training/'+labels2encounter[start][1]):
#             print("xwav_dir:"+xwav_dir)
#                 
#     
#             SpeciesGroups(start,encounter_label_dir+labels2encounter[start][0],xwav_dir)
#             x_train = np.array(SpeciesGroups().get_data_training())
#             x_test = np.array(SpeciesGroups().get_data_testing())
#             y_train = np.array(SpeciesGroups().get_labels_training_cat())
#             y_test = np.array(SpeciesGroups().get_labels_testing_cat())
    sp_data = defaultdict(list)
    for sp_label in all_groups["CINMS17B_1.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["CINMS17B_1.mat"].get_encounters_for_species(sp_label)
    for sp_label in all_groups["CINMS18B_2.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["CINMS18B_2.mat"].get_encounters_for_species(sp_label)
    for sp_label in all_groups["CINMS18C_1.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["CINMS18C_1.mat"].get_encounters_for_species(sp_label)
    for sp_label in all_groups["CINMS19C_2.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["CINMS19C_2.mat"].get_encounters_for_species(sp_label)
#     for sp_label in all_groups["DCPP01A_1.mat"].get_species_labels():
#         sp_data[sp_label] += all_groups["DCPP01A_1.mat"].get_encounters_for_species(sp_label)
#     for sp_label in all_groups["DCPP01A_2.mat"].get_species_labels():
#         sp_data[sp_label] += all_groups["DCPP01A_2.mat"].get_encounters_for_species(sp_label)
#     for sp_label in all_groups["DCPP01B_1.mat"].get_species_labels():
#         sp_data[sp_label] += all_groups["DCPP01B_1.mat"].get_encounters_for_species(sp_label)
#     for sp_label in all_groups["DCPP01B_2.mat"].get_species_labels():
#         sp_data[sp_label] += all_groups["DCPP01B_2.mat"].get_encounters_for_species(sp_label)
#     for sp_label in all_groups["DCPP01C_1.mat"].get_species_labels():
#         sp_data[sp_label] += all_groups["DCPP01C_1.mat"].get_encounters_for_species(sp_label)
#     for sp_label in all_groups["DCPP01C_2.mat"].get_species_labels():
#         sp_data[sp_label] += all_groups["DCPP01C_2.mat"].get_encounters_for_species(sp_label)
    for sp_label in all_groups["SOCAL32E_1.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["SOCAL32E_1.mat"].get_encounters_for_species(sp_label)
    for sp_label in all_groups["SOCAL33E_2.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["SOCAL33E_2.mat"].get_encounters_for_species(sp_label)
    for sp_label in all_groups["SOCAL35R_1.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["SOCAL35R_1.mat"].get_encounters_for_species(sp_label)
    for sp_label in all_groups["SOCAL38R_2.mat"].get_species_labels():
        sp_data[sp_label] += all_groups["SOCAL38R_2.mat"].get_encounters_for_species(sp_label)
    data_training = []
    labels_training = []
    data_testing = []
    labels_testing = []
    for label in sp_data.keys():
        all_data = sp_data[label]
        good_data = []I have runned
        for an_encounter in all_data:
            if len(an_encounter) > 0:
                #This is a good encounter
                good_data.append(an_encounter)
        train_amt = int(round(len(good_data)*test_train_split))
        test_amt = len(good_data) - train_amt
        this_train_data = good_data[:train_amt]
        if(len(this_train_data) > 0):
            this_train_data = list(np.concatenate(np.asarray(this_train_data)))
            labels_training += [label] * len(this_train_data)
            data_training += this_train_data
         
        this_test_data = good_data[train_amt:]
        if(len(this_test_data)>0):
            this_test_data = list(np.concatenate(np.asarray(this_test_data)))
            data_testing += this_test_data
            labels_testing += [label] * len(this_test_data)
         
 
    le = LabelEncoder()
    le.fit(labels_training + labels_testing)
    labels_training_fit = le.transform(labels_training)
    labels_testing_fit = le.transform(labels_testing)
    num_cat = max(labels_training_fit)+1
    labels_training_cat = np_utils.to_categorical(labels_training_fit, num_cat) 
    labels_testing_cat = np_utils.to_categorical(labels_testing_fit, num_cat)  
     
    x_train = np.array(data_training)
    x_test = np.array(data_testing)
    y_train = np.array(labels_training_cat)
    y_test = np.array(labels_testing_cat)    
    model = Sequential()
    model.add(Dense(200, input_dim=200, activation="relu",kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(200, activation="relu",kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(num_cat))
    model.add(Activation("softmax"))
    
    model.compile(optimizer = "Adam",  
                  loss = "binary_crossentropy",
                  metrics = ['categorical_accuracy'])
    

    print("Done Training!")

    #driver()