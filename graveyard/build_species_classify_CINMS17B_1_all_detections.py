'''
Created on Jul 31, 2018

@author: jmerindol
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense

from SampleExtractor import SampleExtractor
import csv
import dateutil.parser
from builtins import input
from docutils.nodes import row

def driver():
    
    model_name = 'test_species_classify.h5'
    num_nodes_per_layer = 100
    epochs = 10
    batch_size = 200
    
    num_samples_for_training = 2000000
    train_labels_path = 'CINMS17B_1.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
    csv_files = "/mar/home/jmerindol/Desktop/dclde_csv/CINMS-B.csv"
    Fs = 200000
    N = 200
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    #Training data
    
    
    with open(csv_files) as csvfile:
        reader = csv.reader(csvfile,delimiter=",") #lecture des donnees sous forme de tableau.
        csv_rows = [] #colonnes sous forme de liste.
        for row in reader:
            csv_rows.append(row)
        csv_row_index = 0
        row = csv_rows[csv_row_index]
        row_start = dateutil.parser.parse(row[3]) #quatrieme colonne = depart du signal.
        row_stop = dateutil.parser.parse(row[4]) #cinquieme colonne = fin du signal.
        data = [] #donnees pour le training.
        labels = [] #output pour le training.

        
        for i in range(0,num_samples_for_training):  #click count range
            if i % 10000 == 0:
                print(i)
            while(sampEx.absoluteStartOfClicks[i] > row_stop):
                csv_row_index += 1
                row = csv_rows[csv_row_index]
                row_start = dateutil.parser.parse(row[3]) #quatrieme colonne = depart du signal.
                row_stop = dateutil.parser.parse(row[4]) #cinquieme colonne = fin du signal.
                
            if (row_start < sampEx.absoluteStartOfClicks[i] 
                    and row_stop > sampEx.absoluteStartOfClicks[i]): #si i est superieur au start de la pre AND si i est inferieur au stop

                labels.append(row[2]) #ajout des row[2] (=especes) a nos donnees training qu'on vient de determiner grace a la boucle while.
                samps = sampEx.getSamplesForClickIdx(i)
                if(len(samps) != 200):
                    print("Error, incorrect sample length for detection: "+str(i))
                    continue
                data.append(samps)
                
        
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    labels = np_utils.to_categorical(labels, 2)   
    
    x_train = np.array(data)
    y_train = np.array(labels)
    
    
    ''' Model Training '''
    #
    model = Sequential()
    model.add(Dense(num_nodes_per_layer, input_dim=200, activation="relu"))
    model.add(Dense(num_nodes_per_layer, activation="relu"))
    model.add(Dense(2))
    model.add(Activation("softmax"))
    
    model.compile(optimizer = "Adam",  
                  loss = "binary_crossentropy",
                  metrics = ['categorical_accuracy'])
    
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size)
    model.save(model_name)
    
if __name__ == '__main__':
    driver()