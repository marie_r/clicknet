'''
Created on Aug 3, 2018

@author: jmerindol
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense

from SampleExtractor import SampleExtractor
import csv
import dateutil.parser
from builtins import input
from docutils.nodes import row

def driver():
    
    model_name = 'test_species_classify.h5'
    num_nodes_per_layer = 100
    epochs = 10
    batch_size = 200
    
    num_samples_for_training = 10000
    num_labels_for_training = 300
    train_labels_path = 'CINMS17B_1.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
    csv_files = "/mar/home/jmerindol/Desktop/dclde_csv/CINMS-B.csv"
    Fs = 200000
    N = 200
    sampEx_train = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    #Training data_training
    
    with open(csv_files) as csvfile: 
        reader = csv.reader(csvfile,delimiter=",") #lecture des donnees sous for;e de tableau. 
        csv_rows = [] #colonnes sous forme de liste.
        
        for row in reader:
            csv_rows.append(row)
        csv_row_index = 0100
        row = csv_rows[csv_row_index]
        row_start = dateutil.parser.parse(row[3]) #quatrieme colonne = depart du signal.
        row_stop = dateutil.parser.parse(row[4]) #cinquieme colonne = fin du signal.
        data_training = [] #donnees pour le training. 
        labels_training = [] #output pour le training.
        cur_detection_index = 0
        
        for row in csv_rows:
            print(row)
            row_start = dateutil.parser.parse(row[3]) #quatrieme colonne = depart du signal.
            row_stop = dateutil.parser.parse(row[4]) #cinquieme colonne = fin du signal.
            num_of_collected_detections_training_so_far = 0 #nombre de detections training collectees au debut du run.

            while (cur_detection_index != None 
                   and num_of_collected_detections_training_so_far < num_samples_for_training
                   and sampEx_train.absoluteStartOfClicks[cur_detection_index] < row_stop): #pour limiter le nombre de samples qu'on analyse pour eviter un surplus de memoire.
#                 while (cur_detection_index < len(sampEx_train.absoluteStartOfClicks) 
#                        and sampEx_train.absoluteStartOfClicks[cur_detection_index] < row_start): #on passe toutes les detections avant le debut du labels_training 
#                     cur_detection_index += 1
                if(sampEx_train.absoluteStartOfClicks[cur_detection_index] < row_start):
                    cur_detection_index = sampEx_train.findFirstClickIdxAfterAbsoluteTime(row_start)
                if cur_detection_index == None:
                    break;  #boucle while : skip toutes les detections en dehors du start et stop d'un label.  

            #print(row)
                labels_training.append(row[2])
                samps = sampEx_train.getSamplesForClickIdx(cur_detection_index)
                data_training.append(samps)
                num_of_collected_detections_training_so_far += 1 #passage a la detection training suivante.
            #ajout des row[2] (=especes) a nos donnees training qu'on vient de determiner grace a la boucle while.
            
              
    
    
    le = LabelEncoder()
    labels_training = le.fit_transform(labels_training)
    num_cat = max(labels_training)+1
    labels_training = np_utils.to_categorical(labels_training, num_cat)  
    
    x_train = np.array(data_training)
    y_train = np.array(labels_training)

    
    #
    ''' Model Training '''
    #
    model = Sequential()
    model.add(Dense(num_nodes_per_layer, input_dim=200, activation="relu"))
    model.add(Dense(num_nodes_per_layer, activation="relu"))
    model.add(Dense(num_cat))
    model.add(Activation("softmax"))
    
    model.compile(optimizer = "Adam",  
                  loss = "binary_crossentropy",
                  metrics = ['categorical_accuracy'])
    
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size)
    predictions = model.predict(x_train)
    model.save(model_name)
    
if __name__ == '__main__':
    driver()