'''
Created on Aug 3, 2018

@author: jmerindol
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from keras import regularizers
from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense
 
from collections import defaultdict, Counter, OrderedDict
from os import listdir

from encounter_groups import SpeciesGroups

from pathlib import Path
    
from timer import Timer

import h5py

from nfold import Nfold
from nis import cat

from collections import namedtuple
import socket

class PrecisionRecall(namedtuple("PrecisionRecall", ['precision', 'recall'])):
    __slots__ = ()  # No need for class dict, saves memory
    def __str__(self):
        "str() - Human readable representation"
        return "(P=%.3f,R=%.3f)"%(self.precision, self.recall)

    def F1(self):
        """F1 - F1 score of precision/recall
        Harmonic mean of precision & recall
        """
        
        return 2.0 * (self.precision*self.recall) \
            / (self.precision + self.recall)
            

def error_analysis(decisions, labels, names):
    """error_analysis(decisions, labels, names)
    Given a set of decisions and the correct labels, return a dictionary
    with error rate measurements.
    
    decisions - numpy array category index for each trial
    labels - numpy array correct category index for each trial
    names - category names corresponding to category 0, 1, ... N-1

    
    Keys and interpretation
    overall_error - error rate across all categories
    label (name is used instead of cat # if supplied)
        (precision, recall) for this category                
    """    
    
    PRtuple = namedtuple("PrecisionRecall", ["precision", "recall"])
    
    results = {}
    
    # overall results
    indicator = decisions == labels
    correct = np.where(indicator)[0]
    N = len(decisions)
    results['overall_error'] = (N-len(correct)) / N    
    
    # compute category precision-recall
    for c in range(len(names)):
        targets = np.where(labels == c)[0]  # ground truth targets
        targets_N = len(targets)
        # Intersection between detections and ground truth
        correct_N = len(np.where(indicator[targets])[0])
        # How many things did we detect? 
        detected_N = len(np.where(decisions == c)[0])
        
        if detected_N > 0:
            p = correct_N / detected_N
        else:
            p = 1.0
        if targets_N > 0:
            r = correct_N / targets_N
        else:
            r = 1.0
            
        results[names[c]] = PrecisionRecall(p, r)
        
    return results
        
        

    
def run_experiment(sp_data, trial, trialId=None, epochs=10, batch_size=100,
                   model_file="PoorNameChoiceIndeed.h5"):
    """run_experiment
    Given data organized as a dictionary of categories, and a test plan
    indicating what should be used for training and testing, train a model
    
    sp_data[category][encounter_idx][example_idx]
        category - class label
        encounter_idx - encounter group
        example_idx- example
        
    trial - list of named TrainTest tuples:
        .label - class label
        .label_idx - class number
        .train - array of encounter #s to use in training
        .test  - array of encounter #s to use in test
        
    trialId - trial number
    epochs - Number of epochs
    batch_size
    model_file - Save model to this filename
    """
    
    # Build data matrix for training and testing
    
    # Determine how many examples we need to allocate
    
    # Take length of first example (first label, first group, first example
    dim = len(sp_data[trial[0].label][0][0])
    classes_N = len(trial)
    classes = [t.label for t in trial]  # class names
    
    # Loop through training indices counting
    train_N = 0
    for t in trial:
        for grp in t.train:
            train_N += len(sp_data[t.label][grp])
    
    timer = Timer()

    # Allocate & populate training data/labels
    print("Populating training data")
    # Data examples, corresponding labels, and counts of each category
    train_examples = np.zeros([train_N, dim], dtype=np.double)    
    train_labels = np.zeros([train_N], dtype=np.double)
    train_counts = Counter()
    
    idx = 0    
    for t in trial:
        print(t)
        for grp in t.train:
            for ex in sp_data[t.label][grp]:
                train_examples[idx,:] = ex
                train_labels[idx] = t.label_idx
                train_counts[t.label_idx] += 1
                idx += 1
    train_labels_cat = np_utils.to_categorical(train_labels, classes_N)
    
    print("Training data frequencies")
    print(train_counts)
    
    print("Populating test data")
    # Repeat for test (put in function later?)
    test_N = 0
    for t in trial:
        for grp in t.test:
            test_N += len(sp_data[t.label][grp])
            
    test_examples = np.zeros([test_N, dim], dtype=np.double)    
    test_labels = np.zeros([test_N], dtype=np.double)
    test_counts = Counter()
    idx = 0    
    for t in trial:
        for grp in t.test:
            for ex in sp_data[t.label][grp]:
                test_examples[idx,:] = ex
                test_labels[idx] = t.label_idx
                test_counts[t.label_idx] += 1
                idx += 1
    test_labels_cat = np_utils.to_categorical(test_labels, classes_N)
                        

    print("Experiment elapsed %s"%(timer))
    pass

    model = Sequential()
    width = 100
    model.add(Dense(width, input_dim=dim, activation="relu",
                    kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(width, activation="relu",
                    kernel_regularizer=regularizers.l2(0.01)))
    model.add(Dense(classes_N))
    model.add(Activation("softmax"))
    
    model.compile(optimizer = "Adam",  
                  loss = "binary_crossentropy",
                  metrics = ['categorical_accuracy'])
    
    model.fit(x=train_examples, y=train_labels_cat, 
              validation_data=(test_examples, test_labels_cat), 
              epochs=epochs, batch_size=batch_size,
              class_weight = train_counts)
    #     predictions = model.predict(x_test)
    model.save(model_file)
    
    print("Elapsed %s"%(timer))
    
    predictions = model.predict(test_examples)
    # Take log adding small bias term to prevent -Inf
    log_pr = np.log(predictions + 1e-6)
    
    # For now, group by N instead of classifying encounter
    # This strategy may fuse detections from multiple encounters of the
    # same species
    fuse_N = 100
    # class_change is a list with the first index of the next class
    # e.g. class_change[0] is the index of the first example of class 1
    # class_change[-1] is one higher than the last index of the last class
    class_change = list(np.where(np.diff(test_labels) > 0)[0])
    class_change.append(len(test_labels))
    
    ex_begin = 0
    c_idx = 0
    done = False
    fused = []
    fused_labels = []
    while not done:
        ex_end = min([ex_begin+fuse_N, class_change[c_idx]])
        fused.append(np.sum(log_pr[ex_begin:ex_end, :], axis=0))
        fused_labels.append(test_labels[ex_begin])
        
        ex_begin = ex_end
        
        if ex_end == class_change[c_idx]:
            c_idx += 1  # next class
            if c_idx >= classes_N:
                done = True
                
    fused = np.asarray(fused)
    gdecision = np.argmax(fused, axis=1)
    fused_labels = np.asarray(fused_labels)

    # Per click error rates
    cdecision = np.argmax(log_pr, axis=1)
    per_click = error_analysis(cdecision, test_labels, classes)
    per_group = error_analysis(gdecision, fused_labels, classes)
    
    return {'click': per_click, 'group' : per_group}

        
    

#
''' Model Training '''

if __name__ == '__main__':
    
    t = Timer()
    
    basedir = Path.home()  # User's home directory
    cachedir = '/cache/mroch/'
    
    corpus_base = basedir / "tensorflow" / "dclde"
    # species labels (per encounter, CSV)
    encounter_label_dir = corpus_base / "encounters"
    # detected click start times
    clickstart_dir = corpus_base / "click_starts"
    
    # These directories are stored locally in different places on
    # different machines 
    cached_audio = {
        'bottlenose': '/cache/dclde_dev/DCLDE2015_HF_training',
        'dusky' : '/media/dclde/DCLDE2015_HF_training'
    }
    acoustic_dir = Path(cached_audio[socket.gethostname()])

    epochs = 25   # Number of iterations over data in training
    
    # Click criteria
    bandpass_low_Hz = 5000
    bandpass_high_Hz = 92000
    min_ppRL_dB = 110
    
    labels2encounter = {"CINMS17B_1.mat":["CINMS-B.csv","CINMS_B/CINMS17B_1/",200000],
                        "CINMS18B_2.mat":["CINMS-B.csv","CINMS_B/CINMS18B_2/",200000], 
                        "CINMS18C_1.mat":["CINMS-C.csv","CINMS_C/CINMS18C_1/",200000], 
                        "CINMS19C_2.mat":["CINMS-C.csv","CINMS_C/CINMS19C_2/",200000],
#                         "DCPP01A_1.mat":["DCPP-A.csv","DCPP-A/DCPP01A_1/",200000],
#                         "DCPP01A_2.mat":["DCPP-A.csv","DCPP-A/DCPP01A_2/",200000],
#                         "DCPP01B_1.mat":["DCPP-B.csv","DCPP-B/DCPP01B_1/",200000],
#                         "DCPP01B_2.mat":["DCPP-B.csv","DCPP-B/DCPP01B_2/",200000],
#                         "DCPP01C_1.mat":["DCPP-C.csv","DCPP-C/DCPP01C_1/",200000],
#                         "DCPP01C_2.mat":["DCPP-C.csv","DCPP-C/DCPP01C_2/",200000]}#,
                        "SOCAL32E_1.mat":["SOCAL-E.csv","SOCAL_E/SOCAL32E_1/",200000],
                        "SOCAL33E_2.mat":["SOCAL-E.csv","SOCAL_E/SOCAL33E_2/",200000], 
                        "SOCAL35R_1.mat":["SOCAL-R.csv","SOCAL_R/SOCAL35R_1/",200000],
                        "SOCAL38R_2.mat":["SOCAL-R.csv","SOCAL_R/SOCAL38R_2/",200000]}
    # Sort it (processing is slow, let's process the same one each time for now
    labels2encounter = OrderedDict(sorted(labels2encounter.items(), key=lambda t: t[0]))
    
    #This section builds the speciesgroups encounter splitter.
    #The SpeciesGroup object splits a specific deployment E.G.: CINMS17B_1
    # into a set of acoustic encounters by species label.
    # The set of acoustic encounters comes from the ground truth csv
    # The detections comes from the detections mat file (detEdit or other detector)
    # The samples come fromt he xwav files directory.
    # The SpeciesGroups uses a SampleExtractor object internally to extract samples for detections.
    # For now there are several defaults supposed in the SpecicesGroups and the SampleExtractor
    #  For example:
    #    - We are using 0.001 seconds of samples for each detection (This comes out to 200 samples)
    #    - We are assuming everything should be 200kHz (There needs to be the resample code for DCPP to work)
    #    - We are not considering multi-spcies encounters (which could be real bad!)
    #        ... We havent considered when acoustic encounters overlap for training/testing
    #    - We are getting a default of 10000 detections per acoustic encounter (if that many exist)
    #        ... We probably want to do all of them, but do we have enough RAM?
    #            >>> If we want to get all of them we probably have to make a specialized datagenerator for keras
    #            >>> We didn't do this because there are a whole host of other concepts we needed to get through first
    
    samples_N = 10000
    
    all_groups = {}
    for start, value in labels2encounter.items():
        # Set up ground truth, acoustic data file, and start times
        groundtruth_file = encounter_label_dir / value[0]
        xwav_dir = acoustic_dir / value[1]
        detection_file = clickstart_dir / start 
                
        # Convert arguments to strings as not using Path yet and don't want
        # to break other users...
        cur_group = SpeciesGroups(str(detection_file), 
                                  str(groundtruth_file), 
                                  str(xwav_dir), value[2], samples_N,
                                  low_Hz = bandpass_low_Hz, 
                                  high_Hz = bandpass_high_Hz,
                                  min_ppRL_dB = min_ppRL_dB)
        
        all_groups[start] = cur_group
    
    #Once we have created a SpeciesGroup object for each deployment, that 
    # species group object allows us to do two things
    #    1) Get the species labels that are present in that deployment
    #    2) Get ALL of the encounters from that deployment for a 
    #       specific species
    #        - This means if we get-encounters_for_species('UO') that will give us a list of lists
    #        - The outer list is a list of acoustic encounters (ordered from earliest encounter to latest)
    #            ... If we want the exact timing more work needs to be done in the SpeciesExtractor
    #        - The inner list is a list of detections within that encounter
    #    Thus:
    #    uo_encounters = cinms17b_1_group.get_encounters_for_species('UO')
    #        ... is a list of UO acoustic encounters form CINMS17B_1
    #    uo_encounters[0] ... is the list of detections from the first UO acoustic encounter in CINMS17B_1
    #    uo_encounters[0][0] ... is the 200 samples for the first detection in the first UO acoustic encounter in CINMS17B_1
    #
    #Next we want to get all of the detections for each species from each deployment
    # we put all of this information into a single data structure organized by species label
    # this data structure is essentially the same data structure inside a SpeciesEncounter object,
    # but now this is for all deployments, not a single deployment
    #    - We had to do this because of limitations in the way detEdit outputs detection information (one mat file per deployment)
    
    print("Merging data from separate deployments, start at %s"%(t))
    sp_data = defaultdict(list)
    for det_group in all_groups.keys():
        for species_label in all_groups[det_group].get_species_labels():
            sp_data[species_label] += \
                all_groups[det_group].get_encounters_for_species(species_label)
    print("Merged %s"%(t))
                
    print("Number of encounters per species:")
    del sp_data['Other']   # Not one of the species we are modeling
    del sp_data['UPP'] # Calls outside of analysis range
    for label in sp_data:
        print("%s %d"%(label, len(sp_data[label])))
                       
    testplan = Nfold(sp_data, 3)                   
    pass

    results = []
    fold = 0
    for trial in testplan:
        model_name = 'dclde_EncounterLim%06dFold%d.h5'%(samples_N, fold)
        result = run_experiment(sp_data, trial, trialId=fold, 
                                model_file=model_name, epochs=epochs)
        print(result)
        results.append(result)
        fold += 1
    
    
    