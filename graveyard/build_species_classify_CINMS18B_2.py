'''
Created on Jul 31, 2018

@author: jmerindol
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense

from SampleExtractor import SampleExtractor
import csv
import dateutil.parser
from builtins import input

def driver():
    
    model_name = 'test_species_classify.h5'
    num_nodes_per_layer = 100
    epochs = 10
    batch_size = 200
    
    num_samples_for_training = 300
    train_labels_path = 'CINMS17B_1.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS18B_2/'
    csv_files = "/mar/home/jmerindol/Desktop/dclde_csv/CINMS-B.csv"
    Fs = 200000
    N = 200
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    #Training data
    
    
    with open(csv_files) as csvfile:
        reader = csv.reader(csvfile,delimiter=",")
        for row in reader:
            start = dateutil.parser.parse(row[3])
            stop = dateutil.parser.parse(row[4])
            data = []
            labels = []
            for i in range(0,num_samples_for_training):    #click count range
                samps = sampEx.getSamplesForClickIdx(i)
                if(len(samps) != 200):
                    print("Error, incorrect sample length for detection: "+str(i))
                    continue
                data.append(samps[0:300])
                
                for j in range(0, 300):
                #while start > sampEx.absoluteStartOfClicks[i]: #tant que i est inferieur a la premiere ligne de donnees
                    #if(start > sampEx.absoluteStartOfClicks[j])
                    #i += 1 #incrementation de i de 1 a chaque tour de boucle
                    #continue
                    if start < sampEx.absoluteStartOfClicks[j]: #si j est superieur au start de la premiere ligne de donnees
                        print(True)
                        print(row)
                        break
                    if stop > sampEx.absoluteStartOfClicks[j]: #si j est inferieur au stop de la meme ligne de donnees 
                        clicks_i = row
                        print (clicks_i)
                        break 
            
        
#            print (True)
#        labels.append(False)
#            continue
        
#        labels.append(True)
#        data.append(samps[100:200])

        
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    labels = np_utils.to_categorical(labels, 2)   
    
    x_train = np.array(data)
    y_train = np.array(labels)
    
    #
    ''' Model Training '''
    #
    model = Sequential()
    model.add(Dense(num_nodes_per_layer, input_dim=200, activation="relu"))
    model.add(Dense(num_nodes_per_layer, activation="relu"))
    model.add(Dense(2))
    model.add(Activation("softmax"))
    
    model.compile(optimizer = "Adam",  
                  loss = "binary_crossentropy",
                  metrics = ['categorical_accuracy'])
    
    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size)
    model.save(model_name)
    
if __name__ == '__main__':
    driver()