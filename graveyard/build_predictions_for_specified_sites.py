'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math,glob,os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from SampleExtractor import SampleExtractor
from TritonLabeler import TritonLabeler
from keras.models import load_model
import h5py

from build_predictions_for_site import build_predictions_for_site
    
'''
    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    #print(classification_report(y_test,predictions))
'''   
if __name__ == '__main__':
    model_name = 'stupid_2layer_dense2.h5'
        
    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/SOCAL_E_090420_000345_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/SOCAL_E/SOCAL32E_1/'
    build_predictions_for_site(model_name,train_labels_path,train_xwav_path)
    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/SOCAL_E_090605_002615_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/SOCAL_E/SOCAL33E_2/'
    build_predictions_for_site(model_name,train_labels_path,train_xwav_path)
     
    
    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/SOCAL_R_091105_001345_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/SOCAL_R/SOCAL35R_1/'
    build_predictions_for_site(model_name,train_labels_path,train_xwav_path)
    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/SOCAL_R_100413_001230_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/SOCAL_R/SOCAL38R_2/'
    build_predictions_for_site(model_name,train_labels_path,train_xwav_path)
    
    
        
        
        
        