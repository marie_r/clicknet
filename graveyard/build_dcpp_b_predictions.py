'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import scipy.signal
from SampleExtractor import SampleExtractor
from TritonLabeler import TritonLabeler
from keras.models import load_model
   

def driver():
    model_name = 'stupid_2layer_dense.h5'
    #12/26/2011 02:38:02
    numSecondsToPredict = 60*10
    predictAbsTimeStart = datetime(2012, 12, 11, 13, 10)

    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/DCPP_B_121211_001428_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/DCPP_B/DCPP01B_1/'
    Fs = 320000
    target_fs = 200000
    time_step_s = .001
    #N = int(Fs*time_step_s/2)
    input_N = 160
    target_N = 100
    model = load_model(model_name)
    
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    
    #define the data to predictDCPP01B on
    pred_data = sampEx.getResampledSamplesForAbsoluteTime(predictAbsTimeStart,target_fs,numSecondsToPredict)
#     pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
#     sampEx.getResampledSamplesForAbsoluteTime(pred_data, 2, 47.155)
    
    #resample data to being comparable to 200khz 
    #Try all the different types of windows and see what works best.
    
#    blackmanharris_10001 = scipy.signal.get_window('blackmanharris', 10001, fftbins=False)
#    blackmanharris_5 = scipy.signal.get_window('blackmanharris', 5, fftbins=False)
#    blackmanharris_501 = scipy.signal.get_window('blackmanharris', 501, fftbins=False)
#    hamming_5 = scipy.signal.get_window('hamming', 5, fftbins=False)
#    hann_5 = scipy.signal.get_window('hann', 5, fftbins=False)
#    kaiser_5 = scipy.signal.get_window(('kaiser',2), 5, fftbins=False) #This should be the default is it the same?

#     up = 5
#     down = 8
#     blackmanharris_updown = scipy.signal.get_window('blackmanharris', max(up, down) // 1, fftbins=False)
# 
#     filtered_data = scipy.signal.resample_poly(pred_data, 5, 8)    #alternate windows here
    filtered_reformed = pred_data.reshape((int(len(pred_data)/target_N),target_N))
#     reguar_resample = scipy.signal.resample(pred_data,200000*numSecondsToPredict)
    '''
    target_idx = 503
    plt.figure()    
    plt.plot(pred_data[target_idx*160:(target_idx+1)*160])
    plt.figure()
    plt.plot(filtered_data[target_idx*100:(target_idx+1)*100])
    plt.figure()
    plt.plot(reguar_resample[target_idx*100:(target_idx+1)*100])
    print("Done")
    '''
    
    
    ground_truth_idx = sampEx.getAllClickIdxBetweenTimes(predictAbsTimeStart,predictAbsTimeStart+timedelta(seconds=numSecondsToPredict))
    gt_abs_start  = [sampEx.absoluteStartOfClicks[x] for x in ground_truth_idx]
    tl = TritonLabeler("groundTruth_.tlab")
    tl.write(gt_abs_start,gt_abs_start)
            
    predictions = model.predict(filtered_reformed,batch_size=1000,verbose=1)
            
            
    thresholds = np.linspace(0.1,.9,9)
    precision = []
    recall = []
    for threshold in thresholds:
        found = [i for i in range(0,len(predictions)) if predictions[i][1] > threshold]
        foundAST = [sampEx.cvtIdxToAbsTimeRelativeToAbsTime(predictAbsTimeStart, x*input_N) for x in found]
        tl = TritonLabeler("foundDetections_"+str(threshold)+"_.tlab")
        tl.write(foundAST,foundAST)
        foundClickIdx = [sampEx.getClickIdxForAbsTime(x) for x in foundAST]
        badClicks = [foundAST[i] for i in range(0,len(foundClickIdx)) if foundClickIdx[i] == None]
        goodClicks = [foundAST[i] for i in range(0,len(foundClickIdx)) if foundClickIdx[i] != None]
        tl = TritonLabeler("truePositive_"+str(threshold)+"_.tlab")
        tl.write(goodClicks,goodClicks)
        tl = TritonLabeler("falsePositive_"+str(threshold)+"_.tlab")
        tl.write(badClicks,badClicks)
        truePositiveIdx = [x for x in foundClickIdx if x != None]
        if(len(found) > 0):
            precision.append(len(truePositiveIdx)/len(found))
        else:
            precision.append(1)
        if(len(ground_truth_idx) > 0):
            recall.append(len(truePositiveIdx)/len(ground_truth_idx))
        else:
            recall.append(1)
         
    plt.figure()
    plt.plot(recall,precision)
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim([0,1.01])
    plt.xlim([0,1.01])
    plt.title('PR by threshold')
    for i in range(0,len(thresholds)):
        plt.annotate("{:.1f}".format(thresholds[i]),xy=(recall[i],precision[i]))
    plt.show()
    print("done!")
'''
    #define the data to predict on
    pred_data = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToPredict)
    pred_data = pred_data.reshape((int(len(pred_data)/100),100))
    
    #print(classification_report(y_test,predictions))
    
    
    30sec(the random time passed in for samples)/stepsize is no. of bins
    getclickidxforabsolutetime, call it in getGT.
    pass each startTime of bins to getclickidxforabstime:
    if returns None then F, else T. This will be saved in the bins, represented as an array.
'''   
if __name__ == '__main__':
    driver()