'''
Created on Apr 17, 2018

@author: gsingh
'''
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math,pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense

from SampleExtractor import SampleExtractor

from TritonLabeler import TritonLabeler
from keras.models import load_model

def driver():
    
    model_name = 'stupid_2layer_dense2.h5'
    num_nodes_per_layer = 100
    epochs = 10
    batch_size = 200
    
    testing_sample_start = [151426,166864,313375,600000,1353322]
    num_samples_for_training = 10000
    
    train_labels_path = '/mar/lab/corpora/siodeteditclicks/GofMX_DT03_disk01_DolphinLabels.mat'
    train_xwav_path = '/media/internal/GofMX_DT03_disk01'
    
    model = load_model(model_name)

    Fs = 200000
    N = 200
    print("Setup")
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    #Testing data
    data = []
    labels = []
    print("Getting data")
#     for si in testing_sample_start:
#         for i in range(si,si+num_samples_for_training):    #click count range
#             print("\r"+str(i-si)+"/"+str(num_samples_for_training),end="")
#             click_time = sampEx.absoluteStartOfClicks[i]
#             samps = sampEx.getBandPassedSamplesForAbsoluteTime(click_time,5000,92000,.001)
#             if(len(samps) != 200):
#                 print("Error, incorrect sample length for detection: "+str(i))
#                 continue
#             data.append(samps[0:100])
#             labels.append(True)
#             data.append(samps[100:200])
#             labels.append(False)
#         
#     le = LabelEncoder()
#     labels = le.fit_transform(labels)
#     labels = np_utils.to_categorical(labels, 2)   
#     
#     x_train = np.array(data)
#     y_train = np.array(labels)
#     print("Testing")
#     predictions = model.predict(x_train,batch_size=1000,verbose=1)
    
    with open('gofm_data.pkl','rb') as f:
        data = pickle.load(f)
    with open('gofm_labels.pkl','rb') as f:
        labels = pickle.load(f)
    with open('gofm_y_train.pkl','rb') as f:
        y_train = pickle.load(f)
    with open('gofm_x_train.pkl','rb') as f:
        x_train = pickle.load(f)
    with open('gofm_predictions.pkl','rb') as f:
        predictions = pickle.load(f)
    
    
    thresholds = np.linspace(0.1,.9,9)
    #thresholds = [.7]
    precision = []
    recall = []
    for threshold in thresholds:
        # select detection above current threshold and determine their start times
        found = [predictions[i][1] > threshold for i in range(0,len(predictions))]
        
        gt_found = [found[i] for i in range(0,len(labels)) if labels[i][1] == 1]
        
        det_found = [labels[i][1]==1 for i in range(0,len(labels)) if found[i] == True]
        
        precision.append(np.sum(det_found)/len(det_found))
        recall.append(np.sum(gt_found)/len(gt_found))
        
    plt.figure()
    plt.plot(precision,recall)
    plt.ylabel("Recall")
    plt.xlabel("Precision")
    plt.ylim([0,1])
    plt.xlim([0,1])
    plt.title('PR by threshold')
    for i in range(0,len(thresholds)):
        plt.annotate("{:.1f}".format(thresholds[i]),xy=(precision[i],recall[i]))
    plt.show()
    print("done!")
    
    
if __name__ == '__main__':
    driver()