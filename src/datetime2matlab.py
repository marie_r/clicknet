"""
Conversion between Python datetime and Matlab serial dates
"""

from datetime import timedelta, datetime

triton_epoch = 2000   # Start of time for Triton audio analysis

class Datetime2MatlabSerial:
    """
    Convert Python datetimes to Matlab serial dates
    """
    def __init__(self):
        self.s_per_day = 24.0 * 60.0 * 60.0  # seconds per day
        self.us_per_day = 24.0 * 60.0 * 60.0 * 1000000.0  # microseconds per day
        self.epochdelta = timedelta(days=366) # Delta btw Python & Matlab epochs

    def datetime2matlabdn(self, dt, triton=False):
        """datetime2matlabdn - Convert datetime or list of datetimes
        to Matlab serial date (datenum)
        :param dt: datetime or list of datetimes
        :param triton: shift serial dates relative to specified epoch
           Triton produces dates relative to the Jan 1 2000
        :return: list of serial dates
        """

        # Preallocate array of Matlab datenums
        matlab_dn = []
        if not isinstance(dt, (list, tuple)):
            dt = (dt)
        # Populate
        for idx in range(len(dt)):
            # Convert to Matlab datenum
            timestamp = dt[idx] + self.epochdelta  # Adjust to Matlab epoch
            if triton:
                timestamp = timestamp.replace(
                    year=timestamp.year - triton_epoch)
            # Fractions of a day
            midnight = timestamp.replace(hour=0, minute=0, second=0, microsecond=0)
            past_midnight = timestamp - midnight
            frac_s = past_midnight.seconds / self.s_per_day
            frac_us = past_midnight.microseconds / self.us_per_day
            matlab_dn.append(timestamp.toordinal() + frac_s + frac_us)

        return matlab_dn

    def matlabdn2datetime(self, dn):
        """matlabdn2datetime
        Conversion from Matlab datenum to datetime"
        :param dn: Matlab datenum array
        :return: list of DateTime
        """
        dt = []
        # See:  https://stackoverflow.com/questions/13965740/converting-matlabs-datenum-format-to-python
        # Matlab dates start year 0, Python year 1
        epochdelta = timedelta(days = 366)
        for tstamp in dn:
            ord = int(tstamp)  # Number of days past year 0
            frac = timedelta(days=tstamp % 1)  # fraction of day
            dt.append(datetime.fromordinal(ord) + frac - epochdelta)

        return dt
