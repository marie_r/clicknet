'''
Created on Oct 23, 2018

@author: mroch
'''

# standard libraries
import math
import pdb
import tempfile

# add-on libraries
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve, roc_curve
from datetime import timedelta
from scipy.stats import norm

class BinaryMetrics:
    """BinaryMetrics - class for metrics related to binary classification
    where decisions are made over a defined interval (input stream is
    tokenized into discrete units to classify)
    """

    def __init__(self, p_detection, labels, token_width_s=.0005,
                 below_thresh=(0, 0)):
        """BinaryMetrics

        Class for generating information about results on binary classification
        trials.  Supports measurements such as ROC, DET, Precision/Recall
        and writing files for Drummond's cost curve software.

        Some of the methods associated with the class need to know
        the frame length of the data being classified.

        :param p_detection: Set of scores produced by a binary classifier
        :param labels:  0/1 labels for each measured item
        :param token_width_s:  Duration of the detection trials in seconds
        :param below_thresh:  tuple with (pos_count, neg_count)
           Used when low scores associated with
           some of the bins were not reported.  This is useful when there is a
           large class imbalance and we don't want to save results for some
           detections.  As an example, there are many more bins without calls
           than with calls in a very long deployment.  We might choose to not
           store the results of any bin that had a score of less than
           some threshold (e.g. .45).  We still need to know the number of
           positive and negative examples that were discarded to produce
           correct results.  Note that any of the result curves will not
           show any variation across this region (curve will be truncated)
        """
        
        self.N = len(p_detection)

        # Find order of ascending probability and reorder the data
        # from smallest to largest detection probability
        self.p_detection = p_detection
        self.labels = labels

        # Use memory mapped arrays when things get too large
        large_N = 10e7
        N = p_detection.shape[0]
        if N > large_N:
            # Create files that will be removed the object dies
            # according to: https://stackoverflow.com/questions/44691030/numpy-memmap-with-file-deletion

            with tempfile.NamedTemporaryFile() as tdet, \
                tempfile.NamedTemporaryFile() as t_asc_p, \
                tempfile.NamedTemporaryFile() as tlabels:

                copyN = int(1e5)  # size of block to copy

                # Copy scores into a memory mapped array
                p = np.memmap(tdet, mode='w+', shape=p_detection.shape,
                              dtype=p_detection.dtype)
                start = int(0)
                while start < N:
                    last = np.min((N, start+copyN))
                    p[start:last] = p_detection[start:last]
                    start = last

                # Get the permutation to produce ascending order
                # np.argsort returns a memory mapped array
                self.order = np.argsort(p)

                # Create memory mapped arrays for the permuted values

                self.asc_p_detection = np.memmap(t_asc_p, mode='w+',
                                                 shape=p_detection.shape,
                                                 dtype=p_detection.dtype)
                self.asc_labels = np.memmap(tlabels, mode='w+',
                                            shape=labels.shape,
                                            dtype=labels.dtype)

                # Populate these new arrays
                start = 0
                while start < N:
                    last = np.min((N, start+copyN))
                    self.asc_labels[start:last] = labels[self.order[start:last]]
                    self.asc_p_detection[start:last] = p_detection[self.order[start:last]]
                    start = last
        else:
            self.order = np.argsort(p_detection)
            self.asc_p_detection = p_detection[self.order]
            self.asc_labels = labels[self.order]
        
        self.token_width_s = token_width_s
        # When there are a large number of samples classified, plotting every point in the performance
        # curve does not always make sense.  Plotting routines will linearly sample self.subsample_N
        # points from the curve when there are more than self.max_samples points.
        self.max_samples = 10000
        self.subsample_N = self.max_samples

        self.N_pos = np.sum(self.asc_labels)
        self.N_neg = self.N - self.N_pos
        self.N_pos_below_thresh = below_thresh[0]
        self.N_neg_below_thresh = below_thresh[1]

        self.transparency = 1.0  # default line transparency
        self.plot_step = False  # Render curves as line segments between points
    
    def get_transparency(self):
        "get_transparency() - Return current transparency setting for lines"
        return self.transparency
    
    def set_transparency(self, t):
        "set_transparency(t) - Future plots will use lines w/ transparency t"
        self.transparency = t

    def get_step(self):
        "get_step() - Return whether lines are plotted with a step function or not"
        return self.plot_step

    def set_step(self, usestepfn=True):
        """set_step(bool) - if True, use step function for plotting, otherwise
        connect points.
        """
        self.plot_step = usestepfn

    def reset_transparency(self):
        "reset_transparency() - Set to default (1.0)"
        self.transparency = 1.0

    def subsample(self, low, high):
        """__subsample(low, high)
        Given a range, generate indices that represent a subsample.
        We subsample piecewise linearly.
        Each of the ends have tighter subsampling than the middle portion

        Caveat:  Do not call unless range is large, there is no checking
        for overlap
        """
        samplesN = high - low + 1
        # We sample more frequently in the lower and upper thirds of the range
        thirds = int(samplesN / 3)
        oversample = 10  # Sample N times for frequently on outer
        center_N = thirds / self.subsample_N
        outer_N = oversample * center_N

        # Concatenate samples from the three ranges
        # with the edges sampled every edgeN and the middle every middleN
        indices = np.hstack((
            np.linspace(low, low+thirds, outer_N, dtype="uint64"),
            np.linspace(low+thirds, low+2*thirds, center_N, dtype="uint64"),
            np.linspace(low+2*thirds, high, outer_N, dtype="uint64")
        ))
        return indices


    def precision_recall(self, plot=False,
                         op_points=None,
                         op_type="precision",
                         op_thresh=None,
                         plot_axes=None,):
        """precision_recall(plot, plot_tau, axes)
        Compute a precision recall curve
        
        plot - if True, plot the curve, remaining arguments are only applicable
            if plot is true
        plot_axes - Set of axes on which to plot.  Defaults to plt.gca() if
            not provided.

        Operating points are only interpreted if plot == True

        op_thr (min, max) - Minimum and maximum thresholds to consider when
           generating plot.  Default None uses the lowest and largest score
           in the sample.

        op_points, op_type, - Annotation of operating points.
           Only used if plot is True
           op_type -
             "tau" - Show performance at specified threshold(s)
             "precision" - Show at specified precision points
             "recall" - Shoow at specified recall points
           op_points -
             Specifies where labels will be plotted.  Must be iterable (e.g.
             tuple, list).  Interpretation depends on op_type.  For example,
             if op_type = "tau" and op_points is (.6, .75, .9, .975) points
             will be marked on the curve for the operating point thresholds in
             the list.
        op_points and op_type may be lists in which case multiple types can
        be annotated, although this can quickly lead to cluttered plots.

        thr_range = tuple/list with lower and uppper threshold for plot

        Returns (precision, recall, thresholds)
        """
        
        # Use scikit learn's PR calculation, give it the sorted versions
        # so that its sort won't have too much work to do.
        [precision, recall, thresholds] = \
            precision_recall_curve(self.asc_labels, self.asc_p_detection)
        if self.N_pos_below_thresh > 0:
            # Adjust recall for any bins that were omitted due to truncation
            # Recall = correct detections / available detections
            # Scale recall by revised count of available detections
            recall = \
                recall * (self.N_pos / (self.N_pos + self.N_pos_below_thresh))

        if plot:
            
            if plot_axes is None:
                plot_axes = plt.gca()

            # Determine range of thresholds over which to plot.
            # We will modify this afterwards to remove trivial classifiers
            N = len(precision)
            if op_thresh is None:
                lower = 0
                upper = N-1
            else:
                lower = np.argmax(thresholds > op_thresh[0])
                upper = np.argmin((thresholds < op_thresh[1]))

            # Avoid plotting the trivial classifiers
            # At the highest thresholds, we see precision 1 recall 0
            # frequently followed by a drop to 0 and then increasing
            while abs(upper) > 0 and \
                    (precision[upper] > .99 or precision[upper] < 1e-4) \
                    and recall[upper] < .005:
                upper -= 1
            while lower < N and precision[lower] < .0001 and recall[lower] > .9999:
                lower += 1

            samplesN = upper - lower
            if samplesN > 0:
                # color='b', alpha=0.2,
                if samplesN > self.max_samples:
                    indices = self.subsample(lower, upper)
                else:
                    indices = np.arange(lower, upper+1, dtype="uint64")  # arange goes to one beneath highest value
                if self.plot_step:
                    curve = plot_axes.step(recall[indices], precision[indices],
                                           where='post', alpha=self.transparency)
                else:
                    curve = plot_axes.plot(recall[indices], precision[indices],
                                           alpha=self.transparency)
            else:
                # Only one sample
                curve = plot_axes.plot(recall[lower:upper], precision[lower:upper],
                               '*', alpha=self.transparency)
                
            _t = plot_axes.set_xlabel("Recall")
            _t = plot_axes.set_ylabel("Precision")


            # Show labels at the following percentages through the thresholds
            if op_points is not None and op_type is not None:
                color = curve[0].get_color()  # match text color to line
                # Convert op_points and op_type to lists that can be paired
                if isinstance(op_type, str):
                    op_type = [op_type]
                    # If they only passed in one op_type, op_points should
                    # be a single iterable, e.g. (.7, .8, .95, .975).
                    # Make it a list of iterables
                    op_points = [op_points]
                # Convert op_points to a list of lists if it is not already
                if len(op_points) != len(op_type):
                    if len(op_type) == 1:
                        raise ValueError(
                            'op_points must be a list/tuple of points')
                    else:
                        raise ValueError(
                            'op_points must be list (tuple) of lists')

                for (type, values) in zip(op_type, op_points):
                    # Set target to values that we will be using
                    # If target is in descending order, reverse it and
                    # store an offset we can use to find the correct
                    # position in the original array
                    if type == "tau":
                        target = thresholds  # ascending view
                        offset = 0 #target.shape[0]-1
                        sign = 1
                    elif type == "precision":
                        target = precision
                        offset = 0
                        sign = 1
                    elif type == "recall":
                        target = recall[::-1]  # ascending view
                        offset = target.shape[0]-1
                        sign = -1
                    else:
                        raise ValueError('op_type != tau, precision, or recall')

                    # Find correct indices
                    indices = [
                        offset + sign*np.searchsorted(target, v, side="right")
                        for v in values]
                    indices = np.unique(indices)  # ensure unique

                    for idx in indices:
                        _t = plot_axes.text(
                            recall[idx], precision[idx],
                            ("%.2f"%(thresholds[idx])).rstrip('0'),
                            color=color, alpha=self.transparency)

        return precision, recall, thresholds
            
            
                
    def falsepos_rate(self, duration_h, plot=False, plot_axes=None):
        """falsepos_rate(duration_h, plot, plto_axes, plot_trans)
        Given the duration in hours of the test duration_h, 
        len(p_detections)*duration_of_trial_bin_in_hours,
        compute the false positive rate per hour with respect
        to thresholds and their associated recall.

        Arguments:
        plot - If True, produces a plot
        plot_axes - Only applicable when plot==True.  Plot on the specified
            axes set.  If not provided, plots on current axes.
        
        returns a list of vectors, (thresh, recall, fp_per_h)
        thresh[i] is the value of the operating threshold that 
        produces a recall rate of recall[i] with a false positive rate
        per hour of fp_per_h[i]
        """
        
        # positives are 1, negatives are 0, create negatives indicator
        negatives = (self.asc_labels + 1) % 2
        # For each threshold, we need to count all of the negatives at or 
        # above it in the negative array.  A cumulative sum of the array 
        # almost does that, but it is in the wrong order.  We reverse
        # the order, computer the cumulative sum, then flip again.
        # Finally, we divide by the number of hours    
        fp_per_h = np.cumsum(negatives[::-1]) / duration_h

        recall = np.cumsum(self.asc_labels[::-1])/np.sum(self.asc_labels)
        
        if plot:
            if plot_axes is None:
                plot_axes = plt.gca()
            if self.asc_p_detection.size >= self.max_samples:
                # Sample the curve.  The beginning of the curve usually changes
                # more quickly as these are the highest thresholds, so
                # sample more intensely there
                sample = self.subsample(0, self.N-1)
                curve = plot_axes.plot(recall[sample], fp_per_h[sample], alpha=self.transparency)
            else:
                curve = plot_axes.plot(recall, fp_per_h, alpha=self.transparency)
            plot_axes.set_yscale('log')  # Usually useful to have a log scale
            _t = plot_axes.set_xlabel('Recall')
            _t = plot_axes.set_ylabel('False positives per hour')

            # Plot thresholds for several false positive rates
            color = curve[0].get_color()
            # Determine fp/h rates to show thresholds
            # Pick powers of 10 within range
            low = np.log10(np.min(fp_per_h))
            if np.isinf(low):
                low = np.log10(fp_per_h[np.argmax(fp_per_h > 1e-3)])
            low = int(np.ceil(low))
            high = int(np.floor(np.log10(np.max(fp_per_h))))
            plot_points = [10.0**v for v in np.arange(low, high+1, 1)]
            for rate in plot_points:
                if rate > fp_per_h[-1]:
                    # stepped off the end
                    rate = fp_per_h[-2]
                    truncated_plot = True
                closest = np.searchsorted(fp_per_h, rate, side="right")
                thresh = self.asc_p_detection[self.N-closest]
                _t = plot_axes.text(recall[closest], rate,
                                    '%.2f'%(thresh), color=color)

        return (self.asc_p_detection, recall, fp_per_h)
    
    def distance_vector_to_nearest_positive(self, direction="either", 
                                            labels=None, verbose=True):
        """distance_vector_to_nearest_positive(direction="either"):
        For every prediction, returns an integer indicating
        how many bins to the closest positive label.  
        
        If labels are not provided, groundtruth labels are used.
        
        CAVEATS:
        Assumes no gaps in effort.
        """
        
        assert(direction in ["left", "right", "either"])
        
        if labels is None:
            labels = self.labels    # use ground truth
        
        # Pass through the labels, computing the distance
        # to the closest true label on the left and right
        
        left = np.zeros([self.N, 1])
        right = np.zeros([self.N, 1])
        
        positive_indices = np.where(labels == 1)[0]
        positive_N = len(positive_indices)
        curr_pos = 0  # position within positive_indices
        more_pos = curr_pos < positive_N
                
        closest_right = positive_indices[curr_pos]
        closest_left = -2*self.N # no label to the left
        if verbose:
            print("Finding closest positive %s side: "%(direction), end="")
            
        progress_count = 0  # verbose counter for newlines        
        for idx in range(self.N):
            if verbose and idx % 1000000 == 0:
                progress_count +=1
                endline = "\n" if progress_count % 10 == 0 else ""
                print("%d%% "%(idx/self.N*100), end=endline)
                
            if more_pos and positive_indices[curr_pos] == idx:
                # We have reached a positive example, see if there are more
                # and set control variables appropriately
                curr_pos += 1   # next positive location (if there are more)
                more_pos = curr_pos < positive_N

                # update our closest left and right pointers
                closest_left = closest_right
                if more_pos:
                    # As we move past the current positive label,
                    # that label becomes the closest label on the
                    # left for future elements
                    closest_right = positive_indices[curr_pos]
                else:
                    # This was the last positive, there are no
                    # more positives to the right
                    closest_right = 2*self.N
            else:
                # Save the distance to the closest positive on either side
                left[idx] = idx - closest_left
                right[idx] = closest_right - idx
        
        if verbose:
            print()
            
        # Take the smallest distance on the left or right
        if direction == "either":
            combined = np.hstack([left, right])        
            closest = np.min(combined, axis=1)
        elif direction == "left":
            closest = left
        else:
            closest = right
        
        return closest
        
        
    def distance_to_nearest_positive(
            self, plot=False, direction="either", 
            thresholds=[.01, .05, .1, .5, .8, .9, .95, .975, .99]):                                     
        """distance_to_nearest_positive(plot, direction, thresholds)
        Return a matrix describing the distribution of distances of false
        positives to the nearest positive label.  False positives are evaluated
        at the thresholds provided in the vector thresholds which defaults to a
        small number of thresholds ranging from very low thresholds to ones
        close to 1.  At the lowest thresholds, these histograms provide a good
        indication of the distribution of how far apart positive labels are from
        one another.
        
        plot - If true, produces a plot on the current figure/axes at various
        thresholds
        
        direction - left, right, either (default)  Show closest label to
            left, right or either direction

        thresholds - vector of operating points for the classifier
        
        Returns (thresh, distances)
        where thresh is the operating threshold and distances is a threshold
        by distance matrix.  That is, distances[t,:] shows the number distribution
        of distances between the nearest label and false positives at operating
        threshold thresh[t].
        
        Caveats:  
        
        Assumes all classifications are next to one another,
        and does not account for gaps in the record.       
        """
        
        # Find the number of bins to the closest ground truth positive
        # on the left/right/either as directed
        closest = self.distance_vector_to_nearest_positive(direction)

        # Reorder these by threshold
        closest_bythresh = closest[self.order]
        # Convert to seconds
        closest_s = closest_bythresh * self.token_width_s
               
        # Determine histogram bin edges
        low_idx = int(thresholds[0] * self.N)
        low_edge = np.min(closest_s[low_idx:])
        high_edge = np.max(closest_s[low_idx:])
        edges = np.logspace(
            np.log10(low_edge+self.token_width_s),np.log10(high_edge), 100)
        distances = np.zeros([len(edges)-1, len(thresholds)]) 

        # histogram values for each threshold in
        # different columns
        col = 0
        for tau in thresholds:
            print("%.2f "%(tau), end = "")
            # Find everything that would be marked as positive and then
            # take a histogram of the distances to the nearest label
            idx = int(self.N * tau)  # find idx in sorted distribution
            # Get histogram, returns list: counts, edges
            h = np.histogram(closest_s[idx:], bins=edges)
            distances[:,col] = h[0]
            col += 1
        print()
            
        if plot == True:
            plt.plot(edges[:-1], distances, alpha=self.transparency)    
            plt.gca().set_xscale("log")
            plt.xlabel('s to nearest labeled click on %s side'%(direction))
            plt.ylabel('Count')
            legend_text = ["$\\tau\\geq$ %.3f"%(tau) for tau in thresholds]
            plt.legend(legend_text)
            # To do, label curves based on threshold
        
        return (thresholds, distances)

    @classmethod
    def is_outlier(cls, points, thresh=3.5):
        """
        Returns a boolean array with True if points are outliers and False 
        otherwise.
    
        Parameters:
        -----------
            points : An numobservations by numdimensions array of observations
            thresh : The modified z-score to use as a threshold. Observations with
                a modified z-score (based on the median absolute deviation) greater
                than this value will be classified as outliers.
    
        Returns:
        --------
            mask : A numobservations-length boolean array.
    
        References:
        ----------
            Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
            Handle Outliers", The ASQC Basic References in Quality Control:
            Statistical Techniques, Edward F. Mykytka, Ph.D., Editor. 
            
        Contributed by Joe Kington,
        https://stackoverflow.com/questions/11882393/matplotlib-disregard-outliers-when-plotting
        """
        if len(points.shape) == 1:
            points = points[:,None]
        median = np.median(points, axis=0)
        diff = np.sum((points - median)**2, axis=-1)
        diff = np.sqrt(diff)
        med_abs_deviation = np.median(diff)
    
        modified_z_score = 0.6745 * diff / med_abs_deviation
    
        return modified_z_score > thresh
            
    def plot_detections(self, tau, sampEx, start, N=20):
        """plot_detections(threshold, sampEx, N)
        Given a threshold, extract and plot N detections.
        
        Note that we do not save the start time of each detection.
        In this function, we assume that all predictions/labels are contiguous
        and that they start at time start.
        
        The plot will contain a random selection of time series data
        corresponding to predictions that are above threshold.  
        
        25% of the plots will be from ground-truth positive bins
        75% will be false positives
        
        Parameters:
        tau - Operating threshold
        sampEx - SampleExtractor instance to access data
        start - Start time of Encounter
        N - Number of samples ot plot
        
        Returns figure and axes handles
        """
        tp = int(.25*N)
        fp = int(N - tp)
        
        rows = 4
        cols = int(N / rows + .5)
        
        detectedI = self.p_detection >= tau
        labelI = self.labels == 1
        # Find the indices that qualify and  take a random sample
        truepos = np.where(detectedI & (labelI == 1))[0]
        falsepos = np.where(detectedI & (labelI == 0))[0]
        truepos_bins = np.random.choice(truepos, tp, replace=False)
        falsepos_bins = np.random.choice(falsepos, fp, replace=False)
        
        delta = timedelta(seconds = self.token_width_s)
        
        lf = 5000
        hf = 92000
        
        # plot title is time or index
        show_click_time = False
        
        count = 0
        fig, ax = plt.subplots(rows, cols,
                              sharex="all", sharey="all", 
                              num="Time series examples")
        flatax = ax.flatten()
        scale = 2**15
        for idx in range(N):
            if tp > 0:
                trial_idx = truepos_bins[-tp]
                tp -= 1
                color = 'blue'
            else:
                trial_idx = falsepos_bins[-fp]
                fp -= 1
                color = 'red'
            read_t = start + trial_idx * delta
            data = sampEx.getBandPassedSamplesForAbsoluteTime(
                read_t, lf, hf, numSeconds=self.token_width_s) 
            flatax[idx].plot(data, color, alpha=self.transparency)
            
            # Compute and display RMS
            rms_dB = 20 * np.log10((np.max(data) - np.min(data))*scale)
            # p2p ratios
            seg_dB = []
            for seg in range(3):
                f = seg*33
                l = f + 33
                seg_dB.append(
                    20*np.log10((np.max(data[f:l]) - np.min(data[f:l]))*scale))
            ratio = max(seg_dB) - min(seg_dB)
                
            bot, top = flatax[idx].get_ylim()
            flatax[idx].text(0, bot + .8*(top - bot), 
                             "db rel: p2p %.1f dB, SNR %.1f"%(rms_dB, ratio))
            
            if show_click_time:
                flatax[idx].set_title(read_t)
            else:
                flatax[idx].set_title("%d"%(trial_idx))
            
        return fig, ax
                
    def plot_fp_incontext(self, tau, sampEx, start, N=10, trial_s=0.0005):
        """plot_fp_incontext(threshold, sampEx, N)
        Given a threshold tau, extract and plot N false positives with
        context information.
        
        Note that we do not save the start time of each detection.
        In this function, we assume that all predictions/labels are contiguous
        and that they start at time start.
                
        Parameters:
        tau - Operating threshold
        sampEx - SampleExtractor instance to access data
        start - Start time of Encounter
        N - Number of samples ot plot
        trial_s - Duration of one trial
        
        Returns figure and axes handles
        """
        
        # Amount of context to provide   
        context_s = .75
        context_offset_s = context_s / 2.0  # +/- half about center
           
        detectedI = self.p_detection >= tau
        labelI = self.labels == 1
        # Find the indices that qualify and  take a random sample
        fp = N
        falsepos = np.where(detectedI & (labelI == 0))[0]
        falsepos_bins = np.random.choice(falsepos, fp, replace=False)
        
        delta = timedelta(seconds = self.token_width_s)
        
        context_half = timedelta(seconds = context_s / 2.0) # +/- offset seconds from start 
        
        lf = 5000       # band pass filter limits
        hf = 92000
        Fs = sampEx.Fs
        
        # plot title is time or index
        show_click_time = False
        
        count = 0
        rows = fp
        cols = 1
        fig, ax = plt.subplots(rows, cols,
                              sharex="all", sharey="all", 
                              num="examples labeled as false positives")
        flatax = ax.flatten()
        scale = 2**15
        color = 'red'
        for idx in range(N):
        
            trial_idx = falsepos_bins[-fp]
            fp -= 1
            read_t = start + trial_idx * delta - context_half
            data = sampEx.getBandPassedSamplesForAbsoluteTime(
                read_t, lf, hf, numSeconds=context_s) 
            taxis = np.linspace(-context_offset_s, context_offset_s, 
                                data.shape[0])
            flatax[idx].plot(taxis, data, color, alpha=self.transparency)
            
            # Compute and display RMS
            #
            trial_samples = int(Fs * trial_s)
            trial_third = int(trial_samples / 3)
            trial_start = int(Fs * context_half.total_seconds())
            fpdata = data[trial_start:trial_start+trial_samples]
            rms_dB = 20 * np.log10((np.max(fpdata) - np.min(fpdata))*scale)
            # p2p ratios
            seg_dB = []
            for seg in range(3):
                f = seg * trial_third
                l = f + trial_third
                seg_dB.append(
                    20*np.log10((np.max(fpdata[f:l]) - np.min(fpdata[f:l]))*scale))
            ratio = max(seg_dB) - min(seg_dB)
                
            bot, top = flatax[idx].get_ylim()
            flatax[idx].text(0, bot + .8*(top - bot), 
                             "db rel: p2p %.1f dB, SNR %.1f"%(rms_dB, ratio))
            
            if show_click_time:
                flatax[idx].set_title(read_t)
            else:
                flatax[idx].set_title("%d"%(trial_idx))
            
        return fig, ax

    def roc(self, plot=False, plot_axes=None):
        """roc
        Compute the receiver operating characteristic (ROC) curve.
        If plot == True, plots the curve on the current axes (plot_axes == None)
        or on the specified plot_axes handle.

        Returns false_pos_rate, true_pos_rate, thresholds
        Where thresholds is a set of operating points and the other arrays
        show the miss (false_pos) and hit (true_pos) rates at each operating
        point.
        """

        false_pos_rate, true_pos_rate, thresholds = roc_curve(
            self.asc_labels, self.asc_p_detection)

        if plot == True:
            if plot_axes is None:
                plot_axes = plt.gca()
                
            samplesN = false_pos_rate.shape[0]
            if samplesN > self.max_samples:
                indices = self.subsample(0, samplesN-1)
            else:
                indices = np.arange(0, samplesN-1, dtype="uint64")

            if samplesN == 1:
                    # Only one sample
                    plot_axes.plot(false_pos_rate, true_pos_rate,
                                   '*', alpha=self.transparency)
            else:
                # Subsample the array (may be the whole sample if small)
                fpr = false_pos_rate[indices]
                tpr = true_pos_rate[indices]
                # Remove trivial classifiers
                idx = 0
                N = indices.shape[0]
                while idx < N and fpr[idx] == 0 and tpr[idx] == 0:
                    fpr[idx] = np.NaN
                    tpr[idx] = np.NaN
                    idx += 1
                idx = N-1
                while idx > 0 and fpr[idx] == 1 and tpr[idx] == 1:
                    fpr[idx] = np.NaN
                    tpr[idx] = np.NaN
                    idx -= 1
                if self.plot_step:
                    plot_axes.step(fpr, tpr,
                                   where='post', alpha=self.transparency)
                else:
                    plot_axes.plot(fpr, tpr, alpha=self.transparency)


            _t = plot_axes.set_xlabel('False positive rate')
            _t = plot_axes.set_ylabel('True positive rate')

        return false_pos_rate, true_pos_rate, thresholds

    def det(self, plot=True,
            false_pos_rate=None, true_pos_rate=None,
            pticks = (0.00001,0.00002,0.00005,0.0001, 0.0002,  0.0005,
                  0.001,  0.002,  0.005,  0.01,   0.02,    0.05,
                  0.1,    0.2,    0.4,    0.6,    0.8,     0.9,
                  0.95,   0.98,   0.99,   0.995,  0.998,   0.999,
                  0.9995, 0.9998, 0.9999, 0.99995,0.99998, 0.99999),
            plot_axes=None):
        """det
        Compute the detection error tradeoff curve

        plot - If True, plots the curve on the current axes (plot_axes == None)
               or on the specified plot_axes handle
        false_pos_rate, true_pos_rate - rates for false and true positives.
            They are computed if not passed in, but if an ROC curve was
            previously constructed, they can be passed in to
            save computation time.
        pticks - Tick marks are shown at the specified locations
        plot_axes - If specified and plot==True, DET curve is produced on the
               specified axis, otherwise uses the current axes

        Returns false_pos_rate, true_pos_rate, thresholds
        Where thresholds is a set of operating points and the other arrays
        show the miss (false_pos) and hit (true_pos) rates at each operating
        point.

        The return values are nearly identical to the ROC plot.  Values of 0
        and 1 are changed to be +/- epsilon to avoid the inverse cdf returning
        -Inf and +Inf.

        The DET plot plots on a normal deviate
        scale.  The probabilities associated with each trial are assumed to be
        drawn from a standard normal distribution and we plot this on an
        inverse deviate scale.  This emphasizes the region around the center
        and deemphasizes the outliers.  In general, DET plots are better for
        showing the results between competing systems, for details see:

       	Martin, A., Doddington, G., Kamm, T., Ordowski, M. and
       	Przybocki, M. (1997). The DET curve in assessment of detection
       	task performance. In Eurospeech, vol. 4, pp. 1895-1898. Rhodes, Greece.

        """

        # Find the false positive and true positive rates from a ROC curve

        if false_pos_rate is None:
            false_pos_rate, true_pos_rate, thresholds = roc_curve(
                self.asc_labels, self.asc_p_detection)


        if plot == True:
            if plot_axes is None:
                plot_axes = plt.gca()

            # Find limits of range to plot.  Anything outside the range
            # of pticks (if set, [1e-5 : 1-(1e-5)] otherwise) will be
            # truncated from the plot.
            if pticks is not None:
                # Position in normal deviate space
                pticklocations = [norm.ppf(val) for val in pticks]
                smallest = np.min(pticklocations)
                largest = np.max(pticklocations)
            else:
                # No ticks provided, very small & very large...
                smallest = norm.ppf(0.00001)
                largest = norm.ppf(0.99999)

            # Examine number of samples and subsample if necessary
            samplesN = false_pos_rate.shape[0]
            if samplesN > self.max_samples:
                indices = self.subsample(0, samplesN-1)
            else:
                # ara nge goes to one beneath highest value
                indices = np.arange(0, samplesN - 1, dtype="uint64")


            # Sample arrays
            # false negatives and false positive rates of sample data
            fp = false_pos_rate[indices]
            fn = 1 - true_pos_rate[indices]

            # Fix any 0 or 1 values that will return -Inf/+Inf in inv cdf
            # eps = np.finfo(false_pos_rate.dtype).eps  # small number
            # localdict = locals()
            # for var in ("fn", "fp"):
            #     val = localdict[var]  # Get a pointer to variable
            #     # Fix low values
            #     idx = 0
            #     while idx < val.shape[0] and val[idx] == 0:
            #         val[idx] = eps
            #         idx += 1
            #     idx = val.shape[0] - 1
            #     while idx > 0 and val[idx] == 1:
            #         val[idx] -= eps
            #         idx -= 1

            # Convert to normal deviate space
            fn = norm.ppf(fn)
            fp = norm.ppf(fp)

            # don't plot things that are past the tick locations; if these
            # are set reasonably we're only truncating trivial classifiers
            fn[fn < smallest] = np.NaN
            fn[fn > largest] = np.NaN
            fp[fp < smallest] = np.NaN
            fp[fp > largest] = np.NaN

            # Plot values using inverse cumulative normal distribution
            if samplesN == 1:
                # Only one sample
                h = plot_axes.plot(fn, fp, '*', alpha=self.transparency)
            else:
                if self.plot_step:
                    h = plot_axes.step(fn, fp,
                                       where='post', alpha=self.transparency)
                else:
                    h = plot_axes.plot(fn, fp, alpha=self.transparency)



            # False alarm
            _t = plot_axes.set_xlabel('False Negative probability (%)')
            # Miss probability
            _t = plot_axes.set_ylabel('False Positive probability (%)')

            # Add tick marks
            if pticks is not None:
                pticklabels = ["%.3f" % (val * 100)
                               for val in pticks]
                plot_axes.set_xticks(pticklocations)
                plot_axes.set_yticks(pticklocations)
                plot_axes.set_xticklabels(pticklabels)
                plot_axes.set_yticklabels(pticklabels)
                range = (np.min(pticklocations), np.max(pticklocations))
                plot_axes.set_xlim(range)
                plot_axes.set_ylim(range)
                plot_axes.tick_params(axis='x', labelrotation=90)
                pass

    def write_cost_curve_data(self, fileH,
                              false_pos_rate=None, true_pos_rate=None,
                              name=""):
        """write_cost_curve_data
        Write out data for generating a cost curve to open file handle fileH.

        false_pos_rate and true_pos_rate are rates for false and true positives.
        They are computed if not passed in, but if an ROC curve was previously
        constructed, they can be passed in to save computation time.

        name - Cost curve name written to fileH.
        """

        if false_pos_rate is None:
            false_pos_rate, true_pos_rate, _thresholds = self.roc()

        fileH.write("%% %s\n"%(name))

        # Examine number of samples and subsample if necessary
        samplesN = false_pos_rate.shape[0]
        if samplesN > self.max_samples:
            indices = np.linspace(0, samplesN - 1, self.subsample_N, dtype="uint64")
        else:
            # ara nge goes to one beneath highest value
            indices = np.arange(0, samplesN - 1, dtype="uint64")

        # Call false pos rate false negatives and convert false positive
        # rate to hit rate (correct detection rate).
        fn = false_pos_rate[indices]
        tp = true_pos_rate[indices]

        for idx in range(fn.shape[0]):
            fileH.write('%.4f %.4f\n'%(fn[idx], tp[idx]))


