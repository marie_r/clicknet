'''
Created on Apr 17, 2018

@author: gsingh
'''
from sklearn.cross_validation import train_test_split
from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense
from attr._make import validate


class FeedForward:
    def __init__(self, x_train, x_test, y_train, y_test, epochs, pred_data):
        self.x_train = x_train
        self.x_test = x_test
        self.y_train = y_train
        self.y_test = y_test
        self.epochs = epochs
        self.pred_data = pred_data
    
    def NNtrain(self):
        
        # define the architecture of the network
        model = Sequential()
        model.add(Dense(100, input_dim=100, activation="relu"))
        model.add(Dense(100, activation="relu"))
        model.add(Dense(2))
        model.add(Activation("softmax"))
        
        model.compile(optimizer = "Adam",  
                      loss = "binary_crossentropy",
                      metrics = ['categorical_accuracy'])
        
        model.fit(self.x_train, self.y_train, epochs=self.epochs, batch_size=500)
        if self.x_test is not None:
            #Calculate accuracy of model
            (loss, accuracy) = model.evaluate(self.x_test, self.y_test,
            batch_size=128, verbose=1)
            print("[INFO] loss={:.4f}, accuracy: {:.4f}%".format(loss,
            accuracy * 100))
        if self.pred_data is not None:
            predictions = model.predict(self.pred_data,batch_size=100,verbose=1)
        else:
            predictions = model.predict(self.x_test)
        return predictions
    
        