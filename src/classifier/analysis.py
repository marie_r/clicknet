"""
analysis -
Analysis of individual detection characteristics and experiment results
"""

# Standard library modules
import datetime
import os
from datetime import timedelta
import pytz

#add on libraries
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as pltdt
from matplotlib.patches import Rectangle
import numpy as np
from cycler import cycler
import scipy
import pandas as pd


# Select which real discrete Fourier transform we will use
dftpack = "fftw"
if dftpack == "fftw":
    # conda/pip install pyfftw
    from pyfftw.interfaces.numpy_fft import rfft as dftreal
else:
    from np.fft import rfft as dftreal

# Our modules
import io_interfaces.tritonlabel as tlab
import io_interfaces.detedit as detedit
from timer import Timer
from encounter_data import build_encounter_managers
from metrics import BinaryMetrics
from .experimentresult import CVResultReader
from datetime2matlab import Datetime2MatlabSerial

def round_minutes(timestamp, direction, resolution_m):
    """round_minutes
    Round a timestamp up or down to the nearest number of minutes
    :param timestamp:
    :param direction:
    :param resolution_m:
    :return: new timestamp
    """
    s_per_min = 60.0
    m_per_h = 60
    # Get fractional minutes past hour, ignores anything less than a second
    minutes = timestamp.minute + timestamp.second / s_per_min
    new_minute = int((minutes // resolution_m + (1 if direction == 'up' else 0)) * resolution_m)
    hourdelta, new_minute = divmod(new_minute, m_per_h)

    return timestamp.replace(minute=new_minute, second=0, microsecond=0) + \
        timedelta(hours=hourdelta)

class DetectionAnalyzer:
    """
    DetectionAnalyzer
    Extracts information about detections
    """

    us_per_s = 1000000.0  # For conversion to samples

    # We will be extracting sample data and computing its DFT
    # These define extraction boundaries before and after the most
    # intense portion of the sound
    # of the sound.
    pad_front_us = 200  # padding before & after in microseconds
    pad_back_us = 200


    def __init__(self, encounter):
        """
        DetectionAnalyzer
        :param encounter:  EncounterData object with details about recording,
           how the data were split into trials, calibration, etc.
        """

        # Trial information.  Number of samples / trial, advance, and timing
        self.frame_len = encounter.trial_duration_N
        self.frame_adv = encounter.trial_advance_N
        self.delta_adv = encounter.trial_advance_delta

        # Sample rate and bit depth (# bits / sample)
        self.Fs = encounter.get_Fs()
        self.bits = encounter.get_bit_depth()
        self.scaleup = 2 ** (self.bits - 1)  # Multipley [-1,1] samples to original samples

        # We will be extracting sample data and computing its DFT
        # These define what we extract after finding the most intense part of
        # of the sound.
        self.pad_front_N = int(self.Fs * self.pad_front_us / self.us_per_s)
        self.pad_back_N = int(self.Fs * self.pad_back_us / self.us_per_s)
        # Number of samples retained for a click
        self.samples_N = self.pad_front_N + self.pad_back_N

        self.calibration = encounter.get_calibration()
        # Get calibration adjustment curve
        self.response = self.calibration.get_response(self.samples_N, self.Fs)
        # Compute left edge of spectrogram frequencies
        self.f_Hz = self.response.Hz
        self.f_kHz = self.f_Hz / 1000.0

        # Peak to peak calibration will be adjusted based on a static
        # peak frequency to be consistent with DetEdit
        self.p2p_calibration_Hz = 25000
        peak_freq_bin = int(np.round(self.p2p_calibration_Hz / self.Fs * self.samples_N))
        self.p2p_adjustment_dB = self.response.dB[peak_freq_bin]

    def get_ax_Hz(self):
        "get_ax_Hz() - Return frequency axis in Hz"
        return self.f_Hz

    def get_ax_kHz(self):
        "get_ax_kHz() - Return frequency axis in Hz"
        return self.f_kHz

    @classmethod
    def get_pad_front(cls):
        """get_pad_front() - Return timedelta indicating amount of padding
        done before the center time of the detection
        """
        return datetime.timedelta(microseconds=cls.pad_back_us)

    @classmethod
    def get_pad_back(cls):
        """get_pad_back() - Return timedelta indicating amount of padding
        done after the center of the detection
        """
        return datetime.timedelta(microseconds=cls.pad_back_us)

    def analyze(self, datablock, indices, start):
        """
        analyze - Pull measurements from candidate detections
        :param datablock: samples in this block
        :param indices: Candidate detection frames
        :param start: datetime showing start of datablock & frames
        :return:
           clickstart, samples, spectra, p2p
           Each of these is a list describing the clicks.  Items of lists contain:
           clickstart - timestamp of the strongest sample in the click
           samples - timeseries of the click of a fixed length positioned so that
              the click peak is in a near constant position near the beginning of
              the series
           spectra - DFT of click, adjusted for transfer funciton if available
           p2p - peak to peak recieved level of each click
        """
        click_times = []
        sample_list = []
        dft_list = []
        p2p_list = []
        for idx in indices:
            # Compute the offsets into the datablock
            first = idx * self.frame_adv
            next = first + self.frame_len

            # Use half-wave recifited sample max as provides better alignment
            # than Teager energy
            maxind = np.argmax(np.abs(datablock[first:next]))
            center_time = \
                start + (idx + maxind / self.frame_len) * self.delta_adv
            click_times.append(center_time)
            center_idx = first + maxind

            # Extract samples associated with click - May need to shift left/right
            # if the signal is too short.
            start_n = center_idx - self.pad_front_N
            stop_n = center_idx + self.pad_back_N
            if start_n < 0:
                # Too early, shift right
                stop_n = stop_n - start_n
                start_n = 0
            if stop_n > datablock.size:
                # Too late, shift left
                start_n = start_n - (stop_n - datablock.size)
                stop_n = datablock.size
            # Rescale samples to original values
            samples = datablock[start_n:stop_n] * self.scaleup
            sample_list.append(samples)

            # Compute spectra of samples
            dft = 20 * np.log10(np.abs(dftreal(samples * self.response.window)))
            # Floor values with small enough energy to result in not-a-number
            dft[np.isnan(dft)] = -50
            # Adjust for DFT bin width (divide -> subtract in log domain) and
            # transfer function
            dft = dft - self.response.bin1Hz_dB + self.response.dB
            dft_list.append(dft)

            # Compute peak 2 peak received level
            # We use an adjustment based on a fixed frequency to be consistent
            # with the work done in DetEdit by MBARC collaborators
            p2pRL_dB = 20*np.log10(np.max(samples) - np.min(samples))
            p2pRL_dB = p2pRL_dB + self.p2p_adjustment_dB  # calibration
            p2p_list.append(p2pRL_dB)

        p2p_array = np.asarray(p2p_list)


        debug = False
        if debug:
            # Show clicks and spectra for "strong" clicks
            strong = np.where(p2p_array >= 120)[0]
            if strong.size > 0:
                plt.figure()
                t = np.linspace(0, samples.size, samples.size) \
                    * (self.pad_front_us + self.pad_back_us)
                for idx in strong:
                    plt.plot(t, sample_list[idx])
                plt.xlabel('µs')
                plt.ylabel('counts')

                plt.figure()
                f_ax = self.get_ax_kHz()
                for idx in strong:
                    plt.plot(f_ax, dft_list[idx])
                plt.xlabel('kHz')
                plt.ylabel('dB rel. 1 uPa')

        return click_times, sample_list, dft_list, p2p_list


def sort_order(data):
    "sort_order(data) - Return indices required to sort the data"

    # Creates list 0:N and sorts it using the values in data
    # http://code.activestate.com/recipes/306862-list-permutation-order-indices/
    return sorted(range(len(data)), key = data.__getitem__)


def score_fold(encounters, results, tolerance,
               basename, fold_idx=0,
               ax_pr=None, ax_fpperh=None, RLpp_thresh=-np.Inf,
               detlab=True,
               clickRLhist=False,
               tritonlab=True):
    """
    score_fold
    Performance metrics on predictions  If used in a cross validation context,
    set foldidx

    :param encounters: List of EncounterData objects that contain ground truth
       for the trial. Each encounter data object corresponds to one effort
       group within the trial
    :param results:  CVResult object with detection results
    :param tolerance:  Detections are considered correct if within tolerance
       of ground truth.  tolerance is a datetime.timedelta
    :param basename: If additional files are written, they will be derived
       from this prefix. e.g. if Triton label files are generated, and basename
       is set to /foo/bar/someexperiment where /for/bar is a directory,
       a label file for false positives will be in:
            /foo/bar/someexperiment_fp_f0.tlab if foldidx == 0
    :param fold_idx:   Fold number for a cross validation experiment
    :param ax_pr: If present, show precision/recall plot on this axis
    :param ax_fpperh: If present, show false positive per hour plot on this axis
    :param RLpp_thresh:  Only consider detections with peak-to-peak
           received levels > RLpp_thresh dB
    :param detlab:  if True, generates a DetEdit files TPWS1 and FD1 relative to
        basename.
    :param tritonlab: if True, generate Triton label files for
        matched - detection matches ground truth
        miss - ground truth was not matched by a detection
        fp - detection does not match a ground truth entry
        Note that these are regardless of threshold.
    :param clickRLhist: if True, shows a histogram of received levels
        for correct detections
    :return:
    """

    f = results.get_fold(fold_idx)  # Get fold result
    f = f['0']  # Assume single channel data for now

    # Num encounters/contiguous blocks
    encountersN = results.get_effortN(fold_idx)

    # Encounter start/end information
    starts = results.get_effort_start(fold_idx)

    detectionsN = len(f.times)  # detection count for fold fold_idx
    # Vector showing which detections are matched to ground truth
    matched = np.zeros((detectionsN,), dtype=np.int) # none yet...
    # Vector showing which detections meet threshold
    meetsthr = f.received_level_dB >= RLpp_thresh
    # missed ground truth times
    miss_times = []

    zerotime = timedelta(microseconds=0)  # For checking pos/neg diff
    # Detections are center of click. Triton labels will be padded by this amount
    offset = timedelta(microseconds=0)  # DetectionAnalyzer.get_pad_back()

    det_times = f.times  # current fold's detection times and received level

    discardedN = 0  # Number of discarded low intensity detections

    d_idx = 0  # index into detectionsN
    matches_timediff = []  # List of delta times from ground truth for matches

    # one off code to count clicks above certain received levels
    RLpresent = hasattr(encounters[0].sampEx, 'p2pRL')  # Assume all/none have
    if RLpresent:
        RLs = (112, 115)
        RLcounts = dict((RL, 0) for RL in RLs)


    for eidx in range(encountersN):
        # some timespan in target encounter
        startutc = pytz.utc.localize(starts[eidx])  # Enc Mgr needs UTC
        span = (startutc, startutc)
        # Get appropriate encounter
        enc_data = encounters.get_encounter_containing_time(span)
        enc_end = enc_data.get_end()

        # Determine ground truth timestamps in this interval
        sampEx = enc_data.sampEx  # SampleExtractor for encounter
        gt_times = sampEx.absoluteStartOfClicks  # ground truth timestamps
        # Find range of gt_times within the current encounter
        # gt_start is the index of the first click after the encounter start
        gt_start = sampEx.findFirstClickIdxAfterAbsoluteTime(starts[eidx])
        # gt_end is the index of the last click before the encounter ends
        gt_end = sampEx.findFirstClickIdxBeforeAbsoluteTime(enc_end)
        # Index of first ground truth click not in encounter (useful for
        # loop boundaries)
        gt_onepast = gt_end + 1

        # We now have the ground truth click indices and can do a merge
        # sort between the detections and the ground truth
        gt_idx = gt_start
        more = det_times[d_idx] <= enc_end  # more to proceess?
        while more:
            # Verify that the detection is above RL threshold
            if meetsthr[d_idx]:
                # Is this detection near current ground truth detection?
                delta = det_times[d_idx] - gt_times[gt_idx]
                near = abs(delta) < tolerance
                if near:
                    # Detection is close enough to ground truth time
                    matched[d_idx] = 1
                    if RLpresent: # update RL threshold counts
                        for rl in RLs:
                            if f.received_level_dB[d_idx] >= rl:
                                RLcounts[rl] += 1

                    d_idx = d_idx + 1
                    matches_timediff.append(delta)

                else:
                    if delta < zerotime:
                        d_idx = d_idx + 1  # detection before ground truth
                    else:
                        gt_idx = gt_idx + 1  # detection after ground truth
            else:
                discardedN = discardedN + 1
                d_idx = d_idx + 1  # Too quiet, move on to next

            # More to do if there are remaining detections
            # and ground truth timestamps to process within the encounter
            more = not (d_idx >= detectionsN or gt_idx >= gt_onepast or
                        det_times[d_idx] > enc_end)

        # If there are any remaining ground truth detections, they
        # have been missed; save them
        if gt_idx < gt_onepast:
            miss_times.extend(gt_times[gt_idx:gt_onepast])

    # Save label files for fold so they can be inspected in Triton
    if tritonlab or detlab:
        # Will need a list of false positive times
        fp = np.logical_and(matched == 0, meetsthr)
        fp_times = [det_times[idx] for idx in np.where(fp == 1)[0]]
        # Possibly correct detections dependent on threshold
        candidates = np.logical_and(matched == 1, f.received_level_dB > RLpp_thresh)
        m_times = [det_times[idx] for idx in np.where(candidates == 1)[0]]

    if tritonlab:
        # Missed detections
        tlab.write_padcenter(basename + "_miss_f%d.tlab" % (fold_idx),
                                miss_times, "miss", offset)
        # False positives
        fpscores = ["%.2f %d" % (f, rl) for f, rl in zip(f.scores[fp], f.received_level_dB[fp])]
        tlab.write_padcenter(basename + "_fp_f%d.tlab" % (fold_idx),
                                fp_times, fpscores, offset)
        # Matched detections
        m_scores = ["%.2f %d" % (f) for f in zip(f.scores[candidates], f.received_level_dB[candidates])]
        tlab.write_padcenter(basename + "_matched_f%d.tlab" % (fold_idx),
                                m_times, m_scores, offset)

    # Write out DetEdit match and false positive times
    if detlab:
        # Convert bad and good detection times to Matlab serial dates
        # Must be saved as a column vector for DetEdit

        converter = Datetime2MatlabSerial()
        # false detections
        detfpname = basename + "f%d_FD1.mat"%(fold_idx)
        fp_serial = np.asarray(converter.datetime2matlabdn(fp_times))
        detfp = {"zFD" : fp_serial, 'score' : f.scores[fp], 'RL' : f.received_level_dB[fp]}
        scipy.io.savemat(detfpname, detfp, oned_as='column')
        # true detections
        dettdname = basename + "f%d_TD1.mat"%(fold_idx)
        m_serial = np.asarray(converter.datetime2matlabdn(m_times))
        dettp = {"zTD" : m_serial, 'score':f.scores[candidates], 'RL':f.received_level_dB[candidates]}
        scipy.io.savemat(dettdname, dettp, oned_as='column')

    if clickRLhist:
        # Show RLpp histogram for all ground truth (assuming that they are
        # all matched if the threshold is low enough, use this:
        ppsnr_h = plt.figure(f"Received level dB fold {fold_idx}")
        ppsnr_ax = ppsnr_h.gca()
        # Add cumulative=-1 to see inverse cdf
        histresult = ppsnr_ax.hist(f.received_level_dB[np.logical_and(matched, meetsthr)],
                      bins=200)
        # compute bin centers
        edges = histresult[1]
        centers = edges[:-1] + 0.5 * np.diff(edges)
        counts = histresult[0]
        # Remove any 0 count bins for fit as we do not want these to go
        # to negative infinity
        posbins = np.where(counts > 0)[0]
        slope, intercept, rval, pval, serr = \
            scipy.stats.linregress(centers[posbins], np.log10(counts[posbins]))
        ppsnr_ax.plot(centers, 10 ** (intercept + slope * centers))
        ppsnr_ax.semilogy()
        # Fit might take y limit under 1.  Set to 1
        ylimit = ppsnr_ax.get_ylim()
        ppsnr_ax.set_ylim([1, ylimit[1]])
        # Add R valuse to title
        print(f'{ppsnr_h.get_label()} histogram fit line R={rval:.03}')
        ppsnr_ax.set_xlabel('$RL_{pp} rel. 1 \mu Pa$')
        ppsnr_ax.set_ylabel('Validated echolocation click count')

    # Show metrics for fold
    # Number of ground truth clicks where no retained click was close
    # enough to be considered a match.  If we had not pruned, there
    # would never be any misses
    missedN = len(miss_times)
    noiseBelowThresh = 0
    metr = BinaryMetrics(f.scores[meetsthr], matched[meetsthr],
                         below_thresh=(missedN,
                                       noiseBelowThresh))
    metr.precision_recall(True, plot_axes=ax_pr,
                          op_type="recall", op_points=[.1, .25, .50, .75, .9, .99],
                          op_thresh=(0.4, .9999))

    # Get duration in hours for current fold
    duration_h = np.sum(results.get_effort_h(fold_idx))
    metr.falsepos_rate(duration_h, plot=True, plot_axes=ax_fpperh)

    print("Fold %d.  Detections %d, Detections meeeting RL %d"%(
        fold_idx, len(f.scores), len(f.scores)-discardedN))

    if RLpresent:
        for rl in RLs:
            print("#clicks(RL >= %d)=%d"%(rl, RLcounts[rl]))

    # Return dataframe of false positive time
    fp_df = pd.DataFrame(fp_times, columns=("fp",))
    return fp_df


def score_results(spec, RLpp_thresh=-np.Inf, perfcurves_multifig=False):
    """
    score_results - Compute precision/recall of an experiment
    :param spec:  Experiment specification
    :param RLpp_thresh:  Only consider detections with peak-to-peak
           received levels > RLpp_thresh dB
    :param perfcurves_multifig: Performance curves are plotted on a multiple
           figures (True)
    :return:
    """
    stopwatch = Timer()

    # encounter managers to access the ground truth data
    enc_mgrs = build_encounter_managers(spec)

    # Open the experiment and set parameters
    basefname = os.path.join(spec['saveroot'], 'result')
    r = CVResultReader(basefname + '.h5')
    folds = r.get_foldsN()

    blk_size = 250000  # Number of trials to process in a batch
    rl_thresh = 0  # Discard trials < N dB
    rl_thresh_analyst = 120 # Analyst threshold
    # Detections must differ no more than this timedelta
    # to be considered correct
    tolerance = timedelta(microseconds=spec['duration_us'])

    # Set up colors and line styles
    # Cycle line styles and use default color order
    mystyles = \
        cycler(linestyle=['-', '-.', '--', ':', '-', '-.', '--', ':', '-', '-.']) + \
        cycler(color=plt.rcParams['axes.prop_cycle'].by_key()['color'])
    plt.rc('axes', prop_cycle=mystyles)

    # Set up precision-recall and false positives per hour figures
    if perfcurves_multifig:
        fig_pr, ax_pr = plt.subplots(num="precision recall %s" % (spec['name']))
        fig_fpperh, ax_fpperh = plt.subplots(num="fp/h %s" % (spec['name']))
    else:
        fig_perf = plt.figure(num="Performance curves %s"%(spec['name']))
        grid = fig_perf.add_gridspec(1, 2)  # side by side curves
        ax_pr = fig_perf.add_subplot(grid[:,0])
        ax_fpperh = fig_perf.add_subplot(grid[:,1])

    # get hours of effort for each fold
    duration_h = [np.sum(r.get_effort_h(idx)) for idx in range(folds)]


    fp_list = []
    for fold_idx in range(folds):
        # Need to think about what encounter managers list means here.

        # This is a bad way to pass in the master TWPS file for the fold,
        # we need a better mechanism
        # The master file is guaranteed to have a one-to-one correspondence
        # with the set of detections in the fold that are contained in the
        # result file.  This lets us split out matches and false positives,
        # creating new DET files for these.
        #detmaster = os.path.join(spec['saveroot'], 'fold%d'%(fold_idx),
        #                        "master_Delphin_TPWS.mat")
        fold_fp = score_fold(enc_mgrs[0], r, tolerance, basefname, fold_idx,
                   ax_pr=ax_pr, ax_fpperh=ax_fpperh, RLpp_thresh=RLpp_thresh,
                   detlab=True, tritonlab=True)
        fp_list.append(fold_fp)  # Note false positives

        # Add fold legends to the plots
        for ax in [ax_pr, ax_fpperh]:
            if folds > 1:
                ax.legend(["fold %d: %.1f h" % (fidx, fdur)
                       for fidx, fdur in enumerate(duration_h)])
            else:
                ax.legend(["effort: %.1f h"%(duration_h[0])])

    gtfp = True
    if (gtfp):
        gtfp_plot(enc_mgrs, fp_list, bin_m=20, name=spec['name'])
    print("Experiment analysis time: %s"%(stopwatch))
    print()  # breakpoint line

def gtfp_plot(enc_mgrs, fp_list, bin_m=30, name="log GT/FP over time"):
    """
    gtfp_plot(enc_mgrs, fp_list, bin_m)
    Show a log count plot of clicks available for detection and the number of
    false positives detected in that same time period.

    :param enc_mgrs:  list of EncounterManager
    :param fp_list: List of per fold fold false positives at some operating
                    point
    :param bin_m: Bin size in minutes for plots
    :return: None
    """

    # Show false positive analysis
    fp_fig = plt.figure()

    # Create data frame of false positives and create per time index
    all_fp = pd.concat(fp_list)
    all_fp.set_index('fp', inplace=True)   # Add index so that we can group

    # Get counts over bin time interval
    bin_pd = "%dMin"%(bin_m)  # bin size in Pandas notation
    fp_per_bin = all_fp.resample(bin_pd).size()  # per bin false positive counts
    max_fp_per_bin = max(fp_per_bin)

    # Get effort
    encounters = []
    for mgr in enc_mgrs:
        for e in mgr.get_encounters():
            encounters.append(e)

    # Kludge, we want to merge some multiple sets of effort for one
    # year of the Dry Tortugas data.  Easiest to simply put them
    # together
    kludge = True
    if kludge:
        site = encounters[0].get_site()
        efforts = [e.get_span() for e in encounters]
        if site == 'DT':
            # Dry Tortugas data in two strata (by year)
            strata = [[efforts[0],], efforts[1:]]
        elif site == 'MC':
            # Missippi Canyon data is all on one stratum
            strata = [[efforts[0]]]  # For MC03 data
    else:
        # Write code to stratify as appropriate (per month, year, etc.)
        # strata should be a list of lists.  Each inner list should contain
        # a set of efforts associated with the stratum
        # e.g. strata = [ [efforts[0], eforts[1]], [efforts[2:5], ... ]
        raise NotImplementedError("Need to design a stratification strategy")

    # Each stratum will be plotted in a separate row
    strataN = len(strata)
    fp_overt = plt.figure(num="False positives over time %s" % (name))
    grid = fp_overt.add_gridspec(strataN, 1)  # stacked plots

    # Process each stratum
    for sidx, stratum in enumerate(strata):
        print("idx %d count %d"%(sidx, len(stratum)))
        # Set up axis for this stratum
        ax_fpperh = fp_overt.add_subplot(grid[sidx, :])
        # Reformat dates
        dtfmt = pltdt.DateFormatter('%m/%d %H:%M')
        ax_fpperh.xaxis_date(tz=None)
        ax_fpperh.xaxis.set_major_formatter(dtfmt)

        first = stratum[0][0]  # start time of first effort
        last = stratum[-1][1]  # end time of last effort

        no_effort = []  # Time spans with no effort
        gt_times = []  # Times of ground truth signals

        # Populate ground truth times in stratum, noting any gaps in effort
        prev_end = None
        for eff in stratum:  # loop over each effort in stratum
            (start, end) = eff
            # Find ground truth times within effort and add to gt_times
            gt_startidx = encounters[sidx].sampEx.findFirstClickIdxAfterAbsoluteTime(start)
            gt_endidx =  encounters[sidx].sampEx.findFirstClickIdxBeforeAbsoluteTime(end)
            gt_times.extend(encounters[sidx].sampEx.absoluteStartOfClicks[gt_startidx:gt_endidx+1])

            if prev_end is not None and prev_end < start:
                # Note gap in effort
                no_effort.append((prev_end, start))
            prev_end = end

        # Convert ground truth to Pandas data frame and count
        # in bins of duration bin_pd (bin_m)
        stratum_gt = pd.DataFrame(gt_times, columns=('gt',))
        stratum_gt.set_index('gt', inplace=True)
        stratum_gt_binned = stratum_gt.resample(bin_pd).size()

        # Find FP in range
        stratum_fp = all_fp[first:last]
        # Get binned counts
        stratum_fp_binned = stratum_fp.resample(bin_pd).size()

        gp_gt0 = np.where(stratum_gt_binned > 0)[0]
        fp_gt0 = np.where(stratum_fp_binned > 0)[0]
        ax_fpperh.set_yscale('log')
        ax_fpperh.plot(stratum_fp_binned[fp_gt0], 'x',
                       stratum_gt_binned[gp_gt0], 's',
                       markersize=3)
        ax_fpperh.set_ylabel('False positives ($log_{10}$)')

        # Plot gray patches for no effort ranges
        if len(no_effort) > 0:
            round_m = np.round(bin_m/2.0)  # Round to nearest half bin
            height = np.max((np.max(stratum_fp_binned), np.max(stratum_gt_binned)))
            for gap in no_effort:
                startgap = round_minutes(gap[0], 'up', round_m)
                stopgap = round_minutes(gap[1], 'down', round_m)
                width = stopgap - startgap
                r = Rectangle(
                    (startgap, 0), # horiz anchor
                    width, height, color='lightgray')
                ax_fpperh.add_patch(r)

    print('here')



