'''
Created on Jan 9, 2019

@author: mroch
'''

from keras.callbacks import Callback

class EveryNBatchLogger(Callback):
    """
    A Logger that log average performance per `everyN` steps.
    From wenmin-wu's suggestion in 
    https://github.com/keras-team/keras/issues/2850
    """
    def __init__(self, everyN, firstN=10):
        """EveryNBatchLogger(everyN)
        Build logger class that only produces output on everyN'th callback
        or when the current batch step is less than firstN
        """
        self.step = 0
        self.everyN = everyN
        self.firstN = firstN

    def on_batch_end(self, batch, logs={}):
        """on_batch_end(batch, logs)
        Standard on_batch_end API
        """
        
                
        # build metrics dictionary
        # Probably should move inside modulo test, but we'll leave it for now
        metric_cache = {}
        for k in self.params['metrics']:
            if k in logs:
                metric_cache[k] = metric_cache.get(k, 0) + logs[k]
                
        # Only process if needed.
        if self.step % self.everyN == 0 or self.step < self.firstN:
            metrics_log = []
            for (k, v) in metric_cache.items():
                val = v / self.everyN
                if abs(val) > 1e-3:
                    metrics_log.append('- %s: %.4f' % (k, val))
                else:
                    metrics_log.append('- %s: %.4e' % (k, val))
            print('step: {}/{} ... {}'.format(self.step,
                                          self.params['steps'],
                                          " ".join(metrics_log)))
            
        self.step += 1

