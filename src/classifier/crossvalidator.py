'''
Created on Dec 2, 2017

@author: mroch
'''

# Standard libraries
import dateutil
import os


# Add-on libraries
from sklearn.model_selection import StratifiedKFold, KFold
import numpy as np
import matplotlib.pyplot as plt
import h5py
import keras.models


# Our modules
from timer import Timer
import metrics
from classifier.feedforward import grouped_train, predict_groups
from classifier.click_arch import architectures 
from classifier.buildmodels import build_model
from classifier.experimentresult import CVResultWriter

        
class CrossValidator:

    debug = False
    
    # If not None, N-fold cross validation will abort after processing N folds
    abort_after_N_folds = None
    
    def __init__(self, Examples, Labels, model, model_train_eval,
                 n_folds=10, batch_size=100, epochs=100): 
        """CrossValidator(Examples, Labels, model_spec, n_folds, batch_size, epochs)
        Given a list of training examples in Examples and a corresponding
        set of class labels in Labels, train and evaluate a learner
        using cross validation.
        
        arguments:
        Examples:  feature matrix, each row is a feature vector
        Labels:  Class labels, one per feature vector
        model: Keras model to learn
            e.g. result of buildmodels.build_model()
        model_train_evel: function that can be called to train and test
            a model.  Must conform to an interface that expects the following
            arguments:
                examples - list or tensor of examples
                labels - categories corresponding to examples
                train_idx - indices of examples, labels to be used
                    for training
                test_idx - indices of examples, labels to be used to
                    evaluate the model
                model - keras network to be used
                batch_size - # examples to process per batch
                epochs - Number of passes through training data
                name - test name
        n_folds - # of cross validation folds
        batch_size - # examples to process per batch
        epochs - Number of passes through training data       
        """
        
        # Create a plan for k-fold testing with shuffling of examples
        # http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.StratifiedKFold.html    #
        kfold = StratifiedKFold(n_folds, shuffle=True)
    
        foldidx = 0
        errors  = np.zeros([n_folds, 1])
        models = []
        losses = []
        timer = Timer()
        self.splits = []
        
        for (train_idx, test_idx) in kfold.split(Examples, Labels):
            # Save encounters used for each fold
            self.splits.append([train_idx, test_idx])
            
            (errors[foldidx], model, loss) = \
                model_train_eval(
                    Examples, Labels, train_idx, test_idx, model,
                    batch_size, epochs, name="f{}".format(foldidx)) 
            models.append(model)
            losses.append(loss)
            print(
                "Fold {} error {}, cumulative cross-validation time {}".format(
                    foldidx, errors[foldidx], timer.elapsed()))
            foldidx = foldidx + 1

            # Useful for debugging an architecture
            if self.abort_after_N_folds is not None:
                if foldidx >= self.abort_after_N_folds:
                    break                     
                    
        # Show architecture of last model (all are the same)    
        
        print("Model summary\n{}".format(model.summary()))
        
        print("Fold errors:  {}".format(errors))
        print("Mean error {} +- {}".format(np.mean(errors), np.std(errors)))
        
        print("Experiment time: {}".format(timer.elapsed()))
        
        self.errors = errors
        self.models = models
        self.losses = losses
                    
    def get_models(self):
        "get_models() - Return list of models created by cross validation"
        return self.models
    
    def get_errors(self):
        "get_errors - Return list of error rates from each fold"
        return self.errors
    
    def get_losses(self):
        "get_losses - Return list of loss histories associated with each model"
        return self.losses
                  
class GroupedBinaryCV:
    """GroupeBinaryCV - binary (detector) cross validation.  
    examples from each group never cross the train/test barrier within an
    experiment.  
    
    Groups are expected to conform to the interface fo the 
    encounter_data.EncounterData class
    """
    
    s_per_h = 3600
    abort_after_N_folds = None  # for debugging
    
    def __init__(self, Groups, model, n_folds=3, 
                 batch_size=100, epochs=20,
                 saveroot=None, gen_negatives = "negatives", minP=.5,
                 train_models=True,
                 description=""): 
        """GroupedCrossValidator(Groups, model_spec, n_folds, batch_size, epochs)
        Given a list of Groups (examples and labels) and a corresponding
        set of class labels in Labels, train and evaluate a learner
        using cross validation.
        
        arguments:
        Groups - Objects that conform to the encounter_data.EncounterGroup
            API
        model: Keras model to learn
            e.g. result of buildmodels.build_model()
        n_folds - # of cross validation folds
        batch_size - # examples to process per batch
        epochs - Number of passes through training data
        gen_negatives - Strategy for generating negative examples.
            See EncounterData.get_encounter_training_features() for details.
        minP - Only retain predictions >= specified value.  Setting very low
            can result in out of memory errors for large data sets
        train_models - Models are trained by default.  If set to false, we
            expect models to be available in the location that saved models
            are written to and we load them in instead of training.
        description - Text description string. If the cross validator is
            saved, this description is saved as an attribute "description"
        saveroot - Root filename for figures, saving etc.  Can be a file
            basename or a full path.
            The experiment results and any generated figures will be
            saved to variants of this path.
            e.g. "boobear" might produce plots such as "boobear_pr.png"
            
            If None, nothing will be save and no figures will be produced
        """
        
        if len(Groups) == 1:
            print("Single group passed in, assume debug.")
            print("Results will be inconclusive as we will test on training data")
            if n_folds != 2:
                n_folds = 2
                print("Overriding n_folds to %d"%(n_folds))
            Groups.append(Groups[0])
        
        self.n_folds = n_folds
        
        
        # Create a plan for k-fold testing 
        # We do not shuffle as the number of encounters is low and 
        # we will keep the splits consistent across experiments
    
        kfold = KFold(n_folds, shuffle=False)
        
        foldidx = 0
        self.models = []
        self.losses = []
        self.train_effort = []
        self.test_effort = []
        self.test_effort_duration_h = [] 
        self.description = description
        self.Groups = Groups
        self.rootname = saveroot
        if self.rootname is not None:
            baseroot = os.path.basename(self.rootname)
            # Define data output name depending on whether user specified a
            # base directory or a base filename
            if os.path.isdir(self.rootname):
                self.data_file = os.path.join(self.rootname, 'result.h5')
                self.model_pattern = os.path.join(self.rootname, 'model_f%d.h5')
            else:
                self.data_file =  self.rootname + "_result.h5"
                self.model_pattern = self.rootname + "_model%_%d.h5"
            # Open up the experiment result writer
            self.results = CVResultWriter(self.data_file, n_folds)
            print("Experiment results stored in %s ")
            print("Data can be accessed with classifier.experimentresult.CVResultReader")
        else:
            self.data_file = None
            self.model_pattern = None
            self.results = None
            print("WARNING: Cross validation will run, " +
                  "but results will not be stored. ")
            baseroot = "unamed_model"

        timer = Timer()



        
        for (train_idx, test_idx) in kfold.split(Groups):

            # Conduct one experiment
            train_groups = [Groups[idx] for idx in train_idx]
            test_groups = [Groups[idx] for idx in test_idx]

            # Create untrained model
            empty_model = build_model(model, name="%s_f%d"%(baseroot,foldidx))

            fold_name = "f%d-%s"%(foldidx, baseroot)
            model_fname = self.model_pattern % (foldidx)  # filename for model

            if train_models:
                # Train it and get predictions for test fold
                fitted_model, loss = \
                    grouped_train(
                        train_groups, empty_model,
                        batch_size, epochs,
                        result_store = self.results, # HDF5 container for results
                        result_fold = foldidx,
                        log_root=saveroot,
                        name=fold_name,
                        gen_negatives=gen_negatives,
                        minP = minP,
                        timer=timer)
                self.losses.append(loss)
                # Store model if user specified a root directory for saves
                if self.rootname is not None:
                    fitted_model.save(model_fname)
            else:
                # Using model that was previously trained.
                fitted_model = keras.models.load_model(model_fname)

            if foldidx == 0:
                # Model summaries should all be the same, display the first one
                # although you will never see it unless in a log file as it
                # will scroll by quickly
                fitted_model.summary()
 
            predict_groups(fitted_model, test_groups, self.results, foldidx,
                    minP, name=fold_name, write_detedit=True, write_tlab=True,
                    timer=timer)




            # merge across encounters
            # Todo:  Rewrite for new structure
            # plotting = False # Turned off for now as dies when we have > billion trials
            # if plotting:
            #
            #     test_predictions = np.hstack(fold_predict)
            #     test_labels = np.hstack(fold_labels)
            #     m = metrics.BinaryMetrics(test_predictions, test_labels)
            #     m.precision_recall(True, plot_axes=ax_pr)
            #     m.falsepos_rate(np.sum(split_test_eff_h), True, plot_axes=ax_fpperh)
            #
            #     plt.plot()  # force plot update (not sure it works)

            foldidx = foldidx + 1

            # Useful for debugging an architecture
            if self.abort_after_N_folds is not None:
                if foldidx >= self.abort_after_N_folds:
                    break                     

        if self.results is not None:
            self.results.h5.close()

        print("Experiment elapsed time: {}".format(timer))
        print()

        
    def get_predictions(self, fold_idx, enc_idx=None):
        """"get_predictions(fold)
        Return predictions for a specified fold.
        If enc_idx is None, data are merged across all encounters for this fold,
        otherwise a specific encounter is returned        
        """
        if enc_idx is None:
            # merge across encounters
            data = np.hstack(self.predictions[fold_idx])  
        else:
            data = self.predictions[fold_idx][enc_idx]
        return data
    
    def get_labels(self, fold_idx, enc_idx=None):
        """"get_predictions(fold)
        Return predictions for a specified fold
        If enc_idx is None, data are merged across all encounters for this fold,
        otherwise a specific encounter is returned        
        """
        if enc_idx is None:
            # merge across encounters
            data = np.hstack(self.labels[fold_idx])  
        else:
            data = self.labels[fold_idx][enc_idx]
        return data

    
    def get_test_effort_for_fold(self, fold_idx):
        """get_test_effort_for_fold(fold_idx)
        List of start and end times of test effort in a given fold
        """
        return self.test_effort[fold_idx]
    
    def get_test_duration_for_fold(self, fold_idx):
        """get_test_duration_for_fold(fold_idx)
        List of duration in hours of test effort for fold
        """
        # end - starts then convert to hours
        durations_h = [(t[1] - t[0]).total_seconds()/self.s_per_h
                     for t in self.test_effort[fold_idx]]
        return durations_h
    
    @staticmethod
    def derive_fname(filename, add_to_basename="", ext="h5"):
        """"derive_fname(filename, add_to_basename, ext)
        Given an existing filename, modify it as follows:
        
        add_to_basename:  string to be added to the basename of the file.
            (default "")
        ext: new extension of file (default "h5")
        
        Examples:  filename = "/boo/results.xml"
        derive_fname("/boo/results.xml", add_to_basename="_dropoutP20", ext="h5")
        returns /boo/results_dropoutP20.h5
        
        CAVEATS:  A dot (.) is added before the extension, do not put
            a dot in ext.
        
        HINT:  Format strings can be embedded in the string
            e.g. add_to_basename="_model_fold%d" 
            and the returned string can be formatted deriv_fname(...)%(fold_idx)
        """ 
        # Construct pattern for model filenames
        path, _ext = os.path.splitext(filename)  # remove old extension
        model_file = "%s%s.%s"%(path, add_to_basename, ext)
        return model_file
        

        
