'''
Created on Oct 28, 2018

@author: mroch
'''


from keras.layers import Dense, Dropout
from keras import regularizers
from tensorflow.python.layers.normalization import BatchNorm
from keras.layers.normalization import BatchNormalization

outputN = 2

architectures = {
    # Scott's baseline model, used with 100 us waveforms and a width of 100
    "baseline": lambda input_nodes, layer_width : 
        [(Dense, [layer_width], {'activation':'relu', 'input_dim':input_nodes}),
         (Dense, [layer_width], {'activation':'relu', 'input_dim':layer_width}),
         (Dense, [outputN], {'activation':'softmax', 'input_dim':layer_width})
        ]
    ,        
    "L2" : lambda input_nodes, layer_width, penalty :
        [(Dense, [layer_width], {'activation':'relu', 'input_dim':input_nodes,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dense, [layer_width], {'activation':'relu', 'input_dim':layer_width,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dense, [outputN], {'activation':'softmax', 'input_dim':layer_width})
        ]
    ,
    "dropout" : lambda input_nodes, layer_width, penalty, Pdrop : 
        [(Dense, [layer_width], {'activation':'relu', 'input_dim':input_nodes,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (Dense, [layer_width], {'activation':'relu', 'input_dim':layer_width,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (Dense, [outputN], {'activation':'softmax', 'input_dim':layer_width})
        ],

    "dropout_d4" : lambda input_nodes, layer_width, penalty, Pdrop : 
        [(Dense, [layer_width], {'activation':'relu', 'input_dim':input_nodes,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (Dense, [layer_width], {'activation':'relu', 'input_dim':layer_width,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (Dense, [layer_width], {'activation':'relu', 'input_dim':layer_width,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (Dense, [layer_width], {'activation':'relu', 'input_dim':layer_width,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (Dense, [outputN], {'activation':'softmax', 'input_dim':layer_width})
        ],
        
        "dropout_batchnorm" : lambda input_nodes, layer_width, penalty, Pdrop : 
        [(Dense, [layer_width], {'activation':'relu', 'input_dim':input_nodes,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (BatchNormalization, [], {}),
         (Dense, [layer_width], {'activation':'relu', 'input_dim':layer_width,
                           'kernel_regularizer':regularizers.l2(penalty)
                           }),
         (Dropout, [Pdrop], {}),
         (BatchNormalization, [], {}),
         (Dense, [outputN], {'activation':'softmax', 'input_dim':layer_width})
        ],

    }
