# Standard library imports
from collections.abc import Iterable
from collections import defaultdict, OrderedDict
import dateutil.parser
import datetime
import pytz


# Add on library imports
import h5py
import numpy as np

# Our libary imports
from . import BinaryPredictions


def CVResultWriter(filename, folds=1):
    # Create a new file, overwrites any existing file
    return CVResult(filename, 'w', folds)

def CVResultReader(filename):
    return CVResult(filename, 'r')

class CVResult:

    """CVResult
    Class for saving/loading results of a cross validation experiment to secondary storage

    Usage:
    with CVResult(filename, folds) as result:
        for f i folds:
           fresult = run experiment on fold f
           result.save_result(f, ...)

    filename - Name to store HDF5 data in
    folds - Number of folds in the experiment

    Note that many of the getters will fail with KeyError if they are accessed
    before data are populated.

    Hierarchy of HDF 5 file:
    folds - Number of folds present
    fold - group containing fold indices 0, 1, ..., folds
       i - fold group i (0, 1, ..., folds)
          efforts - group containing sets of contiguous effort (numbered)
             start - list of start times for each effort
             end - list of end times
             duration_h - list of durations in hours
             j - effort j
                 channels - list of channels present (these data are
                     not present in older files and are supported for
                     reading only)
                 chk - group containing data for channel k (e.g. ch0)
                     Contains parallel arrays containing
                     received_level_dB
                     scores
                     times
    """

    # Define HDF5 variable-length NULL-terminated ASCII string type
    hdfstr = h5py.string_dtype(encoding='ascii')

    def __init__(self, filename, mode='a', folds=1):
        """
        Open/create a result file

        :param filename:  CVResult file
        :param folds: Only used when a new file is created.
            When used for cross validation, specify the number of
            folds in the experiment.  Use the default if using a CVResult
            in a non cross-validation setting.

            If the file exists and is not opened in 'w' mode (see below),
            the number of folds is taken from the existing file

        :param mode: Data are stored in HDF5.  This is the HDF5 file opening mode
            r	Readonly, file must exist
            r+	Read/write, file must exist
            w	Create file, truncate if exists
            w- or x	Create file, fail if exists
            a	Read/write if exists, create otherwise (default)
        """

        self.filename = filename
        self.h5 = h5py.File(filename, mode)

        # We group the data by fold index
        #   e.g. /fold/i where i is the fold number
        # fgrp will contain a list of handles to /fold/i
        self.fgrp = []  # Handle associated with each fold

        # Depending on the mode we opened in, data may already be in the file
        # If so, grab it, otherwise create it.
        if 'folds' in self.h5:
            # Data are present in the file retrieve them
            self.foldsN = self.h5['folds'][0]  # number of folds
            # Retrieve fold groups
            for f in range(self.foldsN):
                if "%d"%(f) not in self.h5['fold']:
                    raise ValueError("Format error in existing file, " +
                        "no fold was created for fold %d"%(f))
                else:
                    self.fgrp.append(self.h5['/fold/%d'%(f)])
        else:
            # new result file, create information
            self.foldsN = folds
            # Store the number of folds in the file
            self.h5.create_dataset("folds", (1,), dtype=np.int, data=self.foldsN)
            # Create fold data sets and store
            self.fgrp = []  # Handle associated with each fold
            for idx in range(folds):
                self.fgrp.append(self.h5.create_group("/fold/%d"%(idx)))


    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, exc_tb):

        self.h5.close()  # data set invalid without proper closure

        if not exc_type is None:
            #Abnormal termination, return True if we want to
            #treat the exception as handled, but no identified need
            #for that now.
            pass

        # False or None will any exception to be propagated.
        return False

    def fold_init(self, fold_idx, effort):
        """fold_init(fold_idx)
        Initialize a fold so that results can be written.
        We must know how many groups of contiguous effort will be written to
        preallocate structures.  This is derived from the length of the effort.

        :param fold_idx: Fold number to be initialized
        :param effort: List of EncounterData.encounter_data objects showing
                       the effort in the fold
        :param dataid: Optional data descriptor for humans (e.g. site/deployment)
        """

        effN = len(effort)
        # Group to contain results from each
        eff = self.fgrp[fold_idx].create_group("efforts")

        # Allocate space for storing information about the test groups
        vecshape = (len(effort),)
        starts = eff.create_dataset(
            'start', vecshape, dtype=np.float64)
        ends = eff.create_dataset(
            'end', vecshape, dtype=np.float64)

        # Need to encode dataids as ASCII
        dataids = eff.create_dataset("dataid", vecshape, dtype=self.hdfstr)

        duration_h = eff.create_dataset('duration_h', vecshape, dtype=np.float64)

        # Store site and duration information
        s_per_h = 3600.0
        for idx in range(effN):
            # Save duration and human readable name of data set
            s = effort[idx].get_effort_duration().total_seconds()
            duration_h[idx] = s / s_per_h
            dataids[idx] = effort[idx].get_dataset_descriptor().encode("ascii")
            # Save effort timestamps
            starts[idx] = effort[idx].get_start().timestamp()
            ends[idx] = effort[idx].get_end().timestamp()



    def save_effort_to_fold(self, fold_idx, eff_idx, result):
        """save_effort_to_fold
        Add results to the specified fold.
          User must have previously called fold_init.

        :param fold_idx: Fold to which results should be saved.
        :param eff_idx:  Index into list of efforts provided in fold_init.
             For example, if there were three blocks of test time (Jan 1-5,
             Jan 6-10, Jan 11-15 2020), these results describe which one of those
             efforts.
        :param result:
            Dictionary whose keys are channel numbers and values are
            BinaryPredictions named tuples (see classifier/init.py).

            See classifier/init.py for BinaryPredictions: (scores, rl_dB, times)
            where scores[i], rl_dB[i], and times[i] show the predicted
            score, label, received level in decibels and timestamp of
            the ith entry that was >= minP.

            scores and received levels are cast to 32 bit floats to reduce
            storage size

        CAVEATS - It is assumed that the results of each effort is only
        recorded once.  Calling this with the same fold and effort twice
        will break.
        """

        # Organize data within fold as efforts/i where i is an effort group
        # representing an encounter
        effgrp = self.fgrp[fold_idx]["efforts"].create_group("%d"%(eff_idx))

        # Get channels from dictionary
        channels = list(result.keys())
        channels.sort()

        # Write out list of channels
        effgrp.create_dataset("channels", data=channels, dtype=np.int)

        # Write out the data
        for ch in channels:
            # Create channel group for data
            target = effgrp.create_group(f"ch{ch}")

            for name in result[ch]._fields:
                data = getattr(result[ch], name)
                if data is None:
                    continue   # No data, skip

                if type(data) == list:
                    if len(data) > 0 and type(data[0]) == datetime.datetime:
                        # Convert datetime to 64 bit floating point numbers
                        N = len(data)
                        doubles = np.zeros((N,), np.float64)
                        for didx in range(N):
                            doubles[didx] = data[didx].timestamp()
                        data = doubles
                        target.attrs[name] = "datetime.timestamp"  # Note encoded datetime
                    else:
                        continue  # Empty list, nothing to do
                else:
                    # Store field type, not really needed as encoded in HD5, but
                    # now we have a type for every attribute, not just the special
                    # case of timestamps
                    target.attrs[name] = str(data.dtype)
                # Write some type of numpy array
                target.create_dataset(name, data=data, dtype=data.dtype)

        return

    def get_effort_h(self, foldidx=0):
        """get_effort_h
        Return array of hours of effort within fold
        :param foldidx:  fold specification
        :return:  numpy array with hours of effort in each contiguous
           effort block
        """

        return (self.h5["fold/%d/efforts/duration_h"%(foldidx)])

    def get_effort_start(self, foldidx=0):
        """
        get_effort - Return a list of starting times for each effort in the fold
        :param foldidx:
        :return: List of datetimes indicating start of each effort block in fold
        """

        starts = self.h5["fold/%d/efforts/start"%(foldidx)]
        # Convert numeric timestamps to datetimes
        timestamps = [datetime.datetime.fromtimestamp(ts)
                      for ts in starts]
        return timestamps

    def get_effort_end(self, foldidx=0):
        """
        get_effort_end - Return a list of ending times for each effort in the fold
        :param foldidx:
        :return: List of datetimes indicating start of each effort block in fold
        """

        ends = self.h5["fold/%d/efforts/end"%(foldidx)]
        # Convert numeric timestamps to datetimes
        timestamps = [datetime.datetime.fromtimestamp(ts)
                      for ts in ends]
        return timestamps

    def get_effortN(self, foldidx):
        """get_effortN
        :param foldidx:
        :return:  Returns number of effort regions associated with effort
        """
        return self.h5["fold/%d/efforts/start" % (foldidx)].len()

    def get_fold(self, foldidx=0, effidx=None, getfields=None):
        """get_fold
        Retrieve information from a specific fold
        :param foldidx:  Experiment fold
        :param effidx: Effort span index or list-like set of effort span
            indices within fold, use None (default) to retrieve all folds
        :param getfields: If None, returns all fields. To only populate certain
            fields of the returned BinaryPredictions variable, provide an
            iterable with the names of the desired fields.  e.g.
            ('scores', 'labels', 'miss_times', 'positivesN')
        :return:
           Returns a dictionary.  Each key is a channel number or "unknown"
           if no channel information is available.  The value is a
           BinaryPredictions  named tuple (see classifier/init.py) containing
           details about experimental result.

           Legacy experiments may not contain channel information.  These
           will always be treated as containg a single channel, channel 0.
        """

        effN = self.get_effortN(foldidx)  # number of efforts within fold
        if effidx is None:
            effrange = range(effN)
        else:
            # User is selecting 1 or more sets of effort.
            if isinstance(effidx, Iterable):
                effrange = effidx
            else:
                effrange = (effidx,)  # make iterable

        h5eff = self.h5["fold/%d/efforts/"%(foldidx)]

        result = OrderedDict()

        # Determine how many items we will need so that we can preallocate
        channels = set()
        scorecounts = defaultdict(lambda : 0)
        for eidx in effrange:
            effresults = h5eff['%d'%(eidx)]  # Results fold / effort
            if "channels" in effresults:
                # HDF file has channel information
                channels = channels | set(effresults.channels)
                for ch in effresults.channels:
                    scorecounts[ch] = scorecounts[ch] + \
                        effresults[f'%{ch}']['scores'].shape[0]
            else:
                # Legacy, no channel information, report as channel 0
                # We can get into trouble if some efforts are in the legacy
                # format and other are not, but this should never be the case.
                channels = channels | {"0"}
                scorecounts["0"] += effresults['scores'].shape[0]

        # predicate function for determining if we should be retrieving
        # a specific field.  Field f must be in effort eff and
        # either the user wants all fields (getfields is None) or
        # the field is in the list of requested fields (getfields)
        retrieve = lambda f, eff : f in eff.keys() and \
                    (getfields is None or f in getfields)

        # The following fields have 1 value per test token (if present)
        per_token_fields = ('scores', 'received_level_dB')
        # The following fields will be converted to lists of datetime objects
        date_fields = ('times')

        # process each channel
        for ch in channels:

            # Two stage process
            # First analyze what we are retrieving to determine what type
            # each item is and preallocate the appropriate number of items

            # Set channel within effort
            if "channels" in effresults:
                chan_eff = effresults['channels'][f'{ch}']
            else:
                chan_eff = effresults   # legacy unnamed channel

            # preallocate to a dictionary (convert values to a tuple later)
            # Fields that are not present will default to None
            # This will hold the values for each data type
            d = defaultdict(lambda : None)

            keys = chan_eff.keys()
            for f in keys:
                if f in per_token_fields and retrieve(f, chan_eff):
                    # Use most recent effort result to get type
                    d[f] = np.zeros((scorecounts[ch],), dtype=chan_eff[f].dtype)
                elif f in date_fields and retrieve(f, chan_eff):
                    # Lists of datetime objects, don't preallocate
                    d[f] = []

            # Second stage - populate the values
            start = 0
            for eidx in effrange:
                # Scores are always present, so we can rely on them for the size
                # of the arrays.  Labels and received_level_dB, if present, will
                # always be the same size.
                effresults = h5eff['%d'%(eidx)]  # Results fold / effort
                # print("effort %d"%(eidx))
                # print("eidx = %d keys = %s"%(eidx, ", ".join(effresults.keys())))
                for f in effresults.keys():
                    if f in per_token_fields and retrieve(f, effresults):
                        stop = start + effresults[f].shape[0]
                        # Copy in values
                        d[f][start:stop] = effresults[f][:]
                    elif f in date_fields and retrieve(f, effresults):
                        # Convert values to datetimes and append
                        dtlist = [datetime.datetime.fromtimestamp(dt)
                                  for dt in effresults[f]]
                        d[f].extend(dtlist)
                start = stop

            result[ch] = BinaryPredictions(d['scores'],
                                           d['received_level_dB'],
                                           d['times'])

        return result

    def get_foldsN(self):
        return self.foldsN

if __name__ == "__main__":
    # Run a quick test
    import tempfile
    import os


    fname = os.path.join(tempfile.gettempdir(), 'quicktest.h5')
    w = CVResultWriter(fname, 2)
    eff = [("2018-01-01T00:00:00Z", "2018-01-31T12:59:59.999Z"),
           ("2019-01-01T00:00:00Z", "2019-01-31T12:59:59.999Z")
           ]
    eff_h = (1,2)  # Wrong values, but irrelevant for test
    predictions = [np.array((.7, .8, .9)), np.array((.1, .2))]
    labels = [np.array((0, 1, 1)), np.array((0, 0))]
    rl_dB = [np.array((105, 110, 120)), np.array((80, 90))]
    w.save_result(0, eff, eff_h, predictions, labels, rl_dB)

    eff2 = [("2018-02-28T00:00:00Z", "2018-02-28T12:59:59.999Z"),
           ("2019-02-01T00:00:00Z", "2019-02-28T12:59:59.999Z")
           ]
    predictions = [np.array((.8, .85, .8, .87)), np.array((.99, .5))]
    labels = [np.array((0, 0, 1, 1)), np.array((1, 0))]
    w.save_result(1, eff2, eff_h, predictions, labels)
    del w

    r = CVResultReader(fname)

    for f in range(r.get_foldsN()):
        info = r.get_fold(f)
        print('fold: %d'%(f))
        print(info)

    pass




