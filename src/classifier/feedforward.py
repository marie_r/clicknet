'''
Created on Sep 30, 2017

@author: mroch
'''

# standard library modules
import math
import time
import datetime
import itertools
import gc
import os
import re
from collections import namedtuple

# add-on modules
from keras.callbacks import TensorBoard
from keras.utils import np_utils
from keras import metrics
import numpy as np
import matplotlib.pyplot as plt

# our modules
from timer import Timer
from .batchgenerator import PaddedBatchGenerator
from .histories import ErrorHistory, LossHistory
from .batchlogger import EveryNBatchLogger
from . import BinaryPredictions
from io_interfaces import detedit, tritonlabel
import classifier.analysis as analysis

def train_and_evaluate(examples, labels, train_idx, test_idx, 
                              model, batch_size=100, epochs=100, 
                              name="model"):
    """train_and_evaluate(examples, labels, train_idx, test_idx,
            model, batch_size, epochs)
            
    Given:
        examples - List of examples in column major order
            (# of rows is feature dim)
        labels - list of corresponding labels
        train_idx - list of indices of examples and labels to be learned
        test_idx - list of indices of examples and labels of which
            the system should be tested.
        model- Keras model to learn
            e.g. result of buildmodels.build_model()
    Optional arguments
        batch_size - size of minibatch
        epochs - # of epochs to compute
        name - model name
        
    Returns error rate, model, and loss history over training
    and probability, label, and RL_dB for test
    """

    # Convert labels to a one-hot vector
    # https://keras.io/utils/#to_categorical
    onehotlabels = np_utils.to_categorical(labels)

    error = ErrorHistory()
    loss = LossHistory()
  
    model.compile(optimizer = "Adam",
                  #loss = seq_loss,  
                  loss = "categorical_crossentropy",
                  metrics = [metrics.categorical_accuracy])
    
    model.summary()  # display
    
    examplesN = len(train_idx)  # Number training examples
    # Approx # of times fit generator must be called to complete an epoch
    steps = int(math.ceil(examplesN / batch_size))  
    
    # for debugging
    tensorboard = TensorBoard(
        log_dir="logs/{}".format(time.strftime('%d%b-%H%M')),            
        histogram_freq=0,
        write_graph=True,
        write_grads=True
        )

    # Find the longest sequence so that we can zero-pad both
    # the training and test data to be sequences of the same
    # size with appropriate zero-padding.
    longest = PaddedBatchGenerator.longest_sequence(examples)
    
    # Evaluation data
    # Reformat examples into zero-padded numpy array
    # Let PaddedBatchGenerator do the work, generating
    # one single "batch" for the epic.  
    #
    # model.fit will take care of breaking it up into minibatches    
    testgen = PaddedBatchGenerator(examples[test_idx],
                                   onehotlabels[test_idx],
                                   batch_size = len(test_idx),
                                   pad_length = longest,
                                   flatten=True)    
    (testexamples, testlabels) = next(testgen)

    # Training data (similar)
    traingen = PaddedBatchGenerator(examples[train_idx], 
                                     onehotlabels[train_idx],
                                     batch_size=len(train_idx),
                                     pad_length = longest,
                                     flatten=True)    
    (trainexamples, trainlabels) = next(traingen)
    
    # train the net
    model.fit(trainexamples, trainlabels,
              validation_data=(testexamples, testlabels),
              epochs=epochs, callbacks=[loss, tensorboard])
    
    print("Training loss %s"%(["%f"%(loss) for loss in loss.losses]))
    
    result = model.evaluate(testexamples, testlabels,
                            verbose=False)

    return (1 - result[1], model, loss) 

def grouped_train(train_groups,
      model, batch_size=100, epochs=10,
      gen_negatives = "next",
      result_store = None,
      result_fold = 0,
      log_root = "logs",
      name="model", minP=.4, timer=None):
    """
    grouped_train(...)
    :param train_groups:  Encounter objects used in training
    :param model: Keras model architecutre used in training
    :param batch_size: size of minibatch
    :param epochs: # of epochs to compute
    :param gen_negatives: Strategy used to select negative examples
       See encounter_data.EncounterData.get_training_examples for details
       on supported methods
    :param result_store:  experimentresult.CVResultWriter object for
       storing information related to CV experiments
    :param result_fold:   Specifies fold index of the result_store
    :param log_root: oot directory for output.  Logs will be created
            as a subdirectory of this directory. Defaults to creating
            a logs directory in the current directory.
    :param name: model name.  The log directory is generated from this
            name and a timestamp of when training begins and placed
            as a subdirectory of log_root.
    :param timer: Timer object used for reporting elapsed time
    :return:  tuple contaning model and loss sequence
    """


    if timer is None:
        timer = Timer()
        
    # Read in the training data and labels
    
    train_data = []
    labels = []
    group_lengths = []
    for g in train_groups:
        # Get training data from this encounter
        g.cache_features()  # build feature cache if it does not already exist
        gexamp, glabels = g.get_encounter_training_features(gen_negatives)
        train_data.append(gexamp)
        labels.append(glabels)
        group_lengths.append(glabels.shape[0])
        
    print("Total number of examples:  %d"%(sum(group_lengths)))
    
        
    # Concatenate training data
    examples = np.vstack(train_data)
    labels = np.hstack(labels)
    
    # Convert labels to a one-hot vector
    # https://keras.io/utils/#to_categorical
    onehotlabels = np_utils.to_categorical(labels)

    error = ErrorHistory()
    loss = LossHistory()
    logger = EveryNBatchLogger(1000)  # Log fit progress every N
  
    model.compile(optimizer = "Adam",
                  #loss = seq_loss,  
                  loss = "categorical_crossentropy",
                  metrics = [metrics.categorical_accuracy])
    
    model.summary()  # display
    
    # for debugging
    tensorboard = TensorBoard(
        log_dir="{}/{}_{}".format(log_root, name, time.strftime('%d%b-%H%M')),            
        histogram_freq=0,
        write_graph=True,
        write_grads=True
        )

    # examples will be N x sample values x channels
    # In training, we always expect single channel data
    # and remove the singleton dimension
    if examples.shape[2] > 1:
        raise ValueError("Training data should not contain multiple channels")
    else:
        examples = np.squeeze(examples, axis=2)

    # train the net
    model.fit(examples, onehotlabels,
              epochs=epochs, callbacks=[loss, tensorboard, logger])
    
    print("Training complete.  Loss: first %f, last %f"%(
        loss.losses[0], loss.losses[-1]))

    return (model, loss)

    


def predict_groups(model, test_groups, result_writer, result_fold=0,
                   minP=.5, channels=None, name="test", timer=None,
                   write_detedit=True, write_tlab=True):
    """
    predict_groups
    Given a trained model and list of EncounterData objects to be tested,
    evaluate the model.  Results are only retained for trial tokens whose
    probability of the binary class exceed a specified threshold
    :param model: Model used to predict presence/absence
    :param test_groups:  List of EncounterData groups to test
    :param result_writer: classifier.experimentresult.CVResult object for saving
        the results of the experiment.  Must be writable, use CVResultWriter
        to generate a CVResult in this mode or call appropriately.
    :param result_fold: In a cross-validation experiment, indicates which fold
       to save result to.  Results are always arranged in folds, use 0 (default)
       when applying a model to data
    :param minP: Only retain trials whose probability of detection exceeds minP
    :param channels: Test on specified channels only (None for all channels)
    :param name: Name displayed in progress messages
    :param timer: timer object for elapsed wall clocktime, creates one if not specified
    :param write_detedit: If True, write a detedit file for detections above threshold
       minP.  Not recommended for low minP as these files will become very large
       and may run into memory problems.
    :param write_tlab: If True write Triton label files showing detections
        that are above threshold minP
    :return: None
    """

    if timer is None:
        timer = Timer()

    Fs = test_groups[0].get_Fs()  # Assume all encounters at same Fs

    # Store effort information about this test
    result_writer.fold_init(result_fold, test_groups)

    # Determine where DET edit  file will go.  A bit kludgy, we steal from
    # the result_writer filename and location for now
    detroot, _ = os.path.split(result_writer.filename)

    results = []
    effortidx = 0

    """
    DetEdit processes from one LTSA group at a time.
    LTSAs can currently only be created from a single directory of
    audio files.  Consequently, we need to create a DETEdit for each
    group. For now, we will assume that an encounter never spans
    more than one disk although the EncounterData class has no such
    restrictions.
    
    LTSAs also need to be in a single directory.  If they were created in
    a directory hierarchy, you can easily create a directory of LTSAs as
    hard links with the following bash commands:
    
    # Get list of LTSA files:
    files=$(find . -name '*.ltsa' -type f)  # Assumes curdir is root of data
    mkdir ltsa  # Create directory where they will be stored
    # Create hard links to ltsas, renaming them with a pattern based on parent
    # directory.  e.g. GofMX_DT03/GofMX_DT03_disk01/LTSAout.ltsa
    # would be stored in ltsa/GofMX_DT03_disk01.ltsa
    for f in $files ; do newname=$(echo $f | cut -d/ -f3) ; ln $f ltsa/${newname}.ltsa; done
    """

    # Determine where DET files will be stored
    if write_detedit or write_tlab:
        # Identify directory where these annotation files will be stored
        if result_fold is None:
            dettargetdir = detroot
        else:
            # Put result in a fold directory, create it if needed
            folddir = "fold%d" % (result_fold)
            dettargetdir = os.path.join(detroot, folddir)
            if not os.path.exists(dettargetdir):
                os.makedirs(dettargetdir)

        # Regexp to extract a directory name based on deployment
        # This pattern is specific to Marine Bioacoustics Research Collaborative
        # (http://acoustics.ucsd.edu/) HARP data logger deployments and would
        # need to be changed for other types of deployments
        disk_re = re.compile(".*_(?P<dir>disk\d+).*")

        if write_detedit:
            # Dictionary of per deployment and channel DetEdit files
            # (files can't span mulitple deployments/disks)
            detwriters = {}

        if write_tlab:
            # Don't offset center of click
            trioffset = datetime.timedelta(microseconds=0)
            # Per deployment Triton writer
            tritonwriters = {}

    # Make sure encounters are sorted chronologically as this is needed
    # for annotation formats such as DetEdit and Triton labels
    test_groups = sorted(test_groups, key=lambda d: d.get_start())

    for g in test_groups:
        test_timer = Timer()
        # Find LTSA group for detedit
        detgroup = "%s_%s_%s"%(
            g.get_project(), g.get_site(), g.get_deploymentId())
        subsite = g.get_subsite()
        if subsite is not None:
            detgroup = detgroup + "_%s"%(subsite)
        # pull a representative filename frm the deployment and try to parse it
        # to get a unique ID
        fname = g.get_filename(g.get_start())
        match = disk_re.match(fname)
        if match is None:
            print("Unable to determine LTSA group from %s, detgroup=%s"%(fname, detgroup))
        else:
            detgroup = detgroup + "_" + match.group("dir")

        channels_available = g.get_channelsN()
        if channels is None:
            channels = [c for c in range(channels_available)]
        elif max(channels) > channels_available:
            raise ValueError(f"Specified channels: {channels}, " +
                             f"but there are only {channels_available} channels")

        # initialize EncounterData test group by channel
        # We record received level, scores, time, waveform & Fourier transform
        ChanResult = namedtuple(
            'ChanResultType',
            ('p2pRL_dB', 'scores', 'times', 'signal', 'spectra'),
            defaults=([], [], [], [], []))
        results = dict()
        for ch in channels:
            results[ch] = ChanResult([], [], [], [], [])

        print('Testing effort group "%s" blocksize=%d s, effort #%d of %d: %s - %s'%(
            name, g.get_block_duration().total_seconds(),
            effortidx+1, len(test_groups), g.get_start(), g.get_end()))
        # Approximate number of blocks to process
        blocksN = np.ceil((g.get_end()-g.get_start()) / g.get_block_duration())
        # Number blocks in N minutes for reporting progress
        block_reportN = int(datetime.timedelta(minutes=30) / g.get_block_duration())

        analyzer = analysis.DetectionAnalyzer(g)

        block = 0
        blocks_with_content = 0
        for data, gt, start, delta, datablock in g:

            block_has_content = False
            for ch in channels:
                # Set up pointers to this channel's data
                chdata = data[:,:,ch]
                chdatablock = datablock[:,ch]

                # Compute the received level.
                # Requires a DFT, but this is simply to apply the transfer function at the peak, we
                # could pick a constant and avoid the DFT.

                # detect signals
                result = model.predict(chdata)
                p = result[:, 1]  # Retain probability of detection

                thr_pred = p >= minP  # meets threshold indicator
                abovethr = np.where(thr_pred)[0]

                if abovethr.size > 0:
                    block_has_content = True
                    # If two clicks are predicted within a frame of one another,
                    # suppress the second one.  Possible reasons to suppress:
                    # 1. Likely echo (not guaranteed)
                    # 2. Trial boundary split a click.  We might predict both frames
                    consecutive = np.where(np.diff(abovethr) == 1)[0]
                    if consecutive.size > 0:
                        remove = [cidx + 1 for cidx in consecutive]
                        abovethr = np.delete(abovethr,remove)

                    # Run analysis on all trials that have scores above threshold
                    # This will determine the precise position of the hypothesized
                    # click, return samples, spectra and peak-to-peak received level
                    starts, samples, dft, p2p = \
                        analyzer.analyze(chdatablock, abovethr, start)

                    # Retain information for each click
                    results[ch].times.extend(starts)
                    results[ch].scores.append(p[abovethr].astype(np.float32))
                    results[ch].p2pRL_dB.append(np.array(p2p, dtype=np.float32))
                    results[ch].signal.extend(samples)
                    results[ch].spectra.extend(dft)

            if block_has_content:
                blocks_with_content += 1

            block = block + 1

            if (block % block_reportN) == 0:
                print("%s predicted block %d of %d (%.1f%%), #blocks with detections %d, elapsed %s" % (
                    name, block, blocksN, block / blocksN * 100.0,
                    blocks_with_content, timer))
                gc.collect()


        # Merge results from all blocks within the encounter group
        for ch in channels:
            # Merge current channel
            results[ch] = ChanResult(
                np.hstack(results[ch].p2pRL_dB),
                np.hstack(results[ch].scores),
                results[ch].times,
                np.vstack(results[ch].signal),
                np.vstack(results[ch].spectra),
                )

        gc.collect()
        # Extract relevant portion for saving to experiment result file
        grpresults = dict()
        for ch in channels:
            grpresults[ch] = BinaryPredictions(
                results[ch].scores,
                results[ch].p2pRL_dB,
                results[ch].times
            )

        # Add to annotation files if there are detections

        for ch in channels:
            if results[ch].spectra.shape[0] > 0:
                # Detections exist for this channel
                chgroup = f'{detgroup}_ch{ch}'
                if write_detedit:
                    # Get or create DetEdit file for this group
                    if chgroup in detwriters:
                        detwriter = detwriters[chgroup]
                    else:
                        detwriter = detedit.Detections(g.get_Fs(), analyzer.get_ax_kHz())
                        detwriters[chgroup] = detwriter

                    # Add to the group associated with this deployment
                    detwriter.add(results[ch].times,
                                  results[ch].p2pRL_dB,
                                  results[ch].spectra,
                                  results[ch].signal,
                                  results[ch].scores)


                if write_tlab:
                    if chgroup in tritonwriters:
                        triwriter = tritonwriters[chgroup]
                    else:
                        triwriter = tritonlabel.Labels()
                        tritonwriters[chgroup] = triwriter

                    # Add to the group associated with this deployment
                    triwriter.addcenter(results[ch].times,
                                        ["%.3f"%(p)
                                         for p in results[ch].scores],
                                        trioffset)

        # Save to experiment result file
        result_writer.save_effort_to_fold(result_fold, effortidx, grpresults)

        effortidx = effortidx+1

    # All test files in this group processed.  Show processing times
    test_elapsed = test_timer.elapsed()
    test_processed = block * g.get_block_duration()
    print("Group %s (%s - %s) done: %s %d channels of %d channel data processed in %s : %.2f times real time"%(
        name, g.get_start(), g.get_end(),
        test_processed, len(channels), channels_available, test_elapsed,
        test_processed.total_seconds() / test_elapsed.total_seconds()))

    # Write out all of the detection editor writers
    if write_detedit:
        for key, detwriter in detwriters.items():
            # Create
            detfile = os.path.join(dettargetdir, "%s_Delphin_TPWS1.mat"%(key))
            detwriter.save(detfile)

    if write_tlab:
        for key, triwriter in tritonwriters.items():
             triwriter.save("%s_Delphin.tlab"%(key))

    return




