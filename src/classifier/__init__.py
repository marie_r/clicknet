"""
classifier module
"""


from collections import namedtuple

"""
 BinaryPredictions
 tuple of predictions for a binary classifier
 Contains:
(scores, rl_dB, times) where
       scores[i], rl_dB[i], labels[i], and times[i] show the predicted
          score, label, received level in decibels and timestamp of
          the ith entry that met some selection criterion (e.g. min score).
          
   If you are unfamiliar with named tuples, you can access these using names
   e.g. t = Prediction(.8, 115 
   instea
"""
BinaryPredictions = namedtuple('Predictions',
    ['scores', 'received_level_dB', 'times'])