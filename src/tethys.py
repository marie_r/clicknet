'''
Created on Jan 30, 2019

@author: mroch
'''


import pdb
import xml.etree.cElementTree as et 
from collections import namedtuple

# add on libraries
import requests
import xmltodict
from scipy.interpolate import interp1d
import scipy.signal
import numpy as np


class Tethys:
    '''
    classdocs
    '''

    
    ty = "http://tethys.sdsu.edu/schema/1.0"  # Tethys namespace
    # These namespaces are automatically added to queries.
    namespaces = "\n".join([
        'import schema namespace ty="%s" at "tethys.xsd";'%(ty),
        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";'])
    
    # Query templates fill in clauses at %s --------------------------
    deployment_xq = """    
<ty:Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
{
for $deployment in collection("Deployments")/ty:Deployment
  %s
  return $deployment
}
</ty:Result>
"""

    calibrations_xq = """
<ty:Result>
{ 
for $cal in collection("Calibrations")/ty:Calibration
    %s
    return $cal
}
</ty:Result>
"""
    # ----------- end templates --------------------------------------


    valid_return_types = [
        "xml",    # Text
        "dict",   # dictionary representation of the XML
        "etree"   # ElementTree representation of the XML
        ]
    
    def __init__(self, server="breach.ucsd.edu", port=9779, protocol="http",
                 rtype="etree"):
        '''
        Tethys(server, port, rtype)
        Communication with a Tethys server
        server - Server name or IP
        port - Server port
        protocol - http or https (secure socket http)
        rtype - How results will be returned, can be changed with 
            set_return_type(type), see below for details
        '''
        
        self.server = server
        self.port = port
        
        self.xquery_url = "%s:%d/XQuery"
        self.protocol = protocol + "://"
        
        self.rtype = rtype
 
    def set_return_type(self, type):
        """set_return_type(type)
        How should the data be returned
            xml - as an XML string
            dict - as a Python dictionary (namespaces stripped)
            etree - as a Python cElementTree        
        """
        if type in self.valid_return_types:
            self.rtype = type
        else:
            raise ValueError("Not a valid type, must be in [%s]"%(
                ",".join(self.valid_return_types)))
    
    def xquery(self, xq, type=None):
        "post_request(xq) - Run an XQuery, namespaces are prepended"
        
        # POST parameter pacakge
        params = {'XQuery' : "\n".join([self.namespaces, xq])}
        print(params['XQuery'])
        # set up the URL with the active server
        url = self.protocol + self.xquery_url%(self.server, self.port)
        # Run the query        
        result = requests.post(url, params)   
        
        if result.status_code != requests.codes.ok:
            raise RuntimeError("HTTP XQuery error code %d: %s"%(
                result.status_code, result.text))
    
        # Output conversion as needed
        if type == None:
            type = self.rtype
        if type == "xml":
            rval = result.text
        elif type == "dict":
            # Convert to dictionary, stripping namespaces
            rval = xmltodict.parse(result.text,
                                   process_namespaces=True,
                                   namespaces = {
                                       self.ty : None})
        elif self.rtype == "etree":
            rval = et.fromstring(result.text)            
        
        return rval
        
        
    def get_deployment(self, project, site, deployment):
        """get_deployment(project, site, deployment)
        Returns specified deployment(s)
        """
        
        if isinstance(deployment, str):
            deployment = int(deployment)
        # Build up the clause and put it in the query template
        clause = 'where ' + " and ".join([
            '$deployment/Project = "%s"'%(project),
            '$deployment/Site = "%s"'%(site),
            '$deployment/DeploymentID = %d'%(deployment)
            ])
        
        xq = self.deployment_xq%(clause)
                
        return self.xquery(xq)
        
                  
    def get_calibration(self, id, type="amplifier", rtype=None):
        """"get_calibration(id, type)
        Retrieve the calibration results for specified calibration type
        """     
        if isinstance(id, str):
            clause = "where $cal/ID = %s"%(id)
        else:            
            clause = "where $cal/ID = %d"%(id)
        clause = clause + ' and $cal/Type = "preamplifier"'
        xq = self.calibrations_xq%(clause)
        
        result = self.xquery(xq, rtype)
        
        return result 
    
    def get_calibration_response(self, id, type="amplifier"):
        """"get_calibration_response(id, type)
        Similar to get_calibration, but parses out the frequency
        response.  Return four items:
            Hz - List of frequencies
            dB - List of responses at those frequencies
            interpfn - Interpoloation function.  Call with a list of
              frequencies to get the linearly interpolated response.
              Interpolation beyond the bounds is enabled, be careful!
            cal - etree representation of calibration object
        """
        
        cal = self.get_calibration(id, type, rtype="etree")     
        Hz = cal.find(".//FrequencyResponse/Hz").text
        dB = cal.find(".//FrequencyResponse/dB").text
        Hz = [float(h) for h in Hz.split()]
        dB = [float(d) for d in dB.split()]

        # We allow extrapolation as the calibration starts at 1 Hz
        # and our DFT axis at 0.
        interp_fn = interp1d(Hz, dB, fill_value="extrapolate")
        return Hz, dB, interp_fn, cal 
    
    def get_calibration_object(self, id, type="amplifier"):
        """get_calibration_object(id, type)
        Returns a Calibration that can be used to compute calibration curves
        for a variety of frequencies.
        """

        cal = self.get_calibration(id, type, rtype="etree")
        Hz = cal.find(".//FrequencyResponse/Hz").text
        dB = cal.find(".//FrequencyResponse/dB").text
        Hz = [float(h) for h in Hz.split()]
        dB = [float(d) for d in dB.split()]
        return Calibration(id, type, Hz, dB)



class Calibration:
    # window - Window function
    # dB - calibration function response in dB
    # Hz - Hz[i] frequency at which response dB[i] occurs
    # bin1Hz_dB - Conversion factor for energy per 1 Hz bin
    response = namedtuple("response", ('window', 'Hz', 'dB', 'bin1Hz_dB'))

    def __init__(self, id, type, Hz, dB):
        """Calibration(id, type, Hz, dB)
        Create a calibration object.
        """

        self.id = id
        self.type = type

        self.Hz = Hz
        self.dB = dB

        # We turn off the bounds error as the calibration starts at 1 Hz
        # and our DFT axis at 0.
        self.interp_fn = interp1d(Hz, dB, fill_value="extrapolate")

        self.calibrations = {}

    def get_response(self, windowN, Fs, binloc="center"):
        """
        get_response
        :param windowN:  Number of samples in analysis window
        :param Fs:  Sample rate
        :parm binloc:
           "center" - Compute response at bin centers
           "left" - Compute response at left edge of frequency bins
        :return: response named tuple with fields:
           window - Hann window function
           Hz - Bin frequency labels up to Nyquist
           dB - Calibration offset
           bin1Hz_dB - Normalization factor for energy/1 Hz bin

           Calibrations are applied to 20 log10 mag spectra:
               subtract off dB to divide by bin width
               add dB to account for system response
        """

        key = (windowN, Fs)
        try:
            result = self.calibrations[key]
        except KeyError:
            # We've never seen this one before, create a new one
            h = scipy.signal.hann(windowN)
            # Interpret to DFT resolution
            delta_Hz = Fs / windowN
            # Assume same number of bins as returned by np.fft.rfft
            Nyquist = Fs / 2
            spacing = np.linspace(0, 1, windowN // 2 + 1)
            if windowN % 2 == 0:
                # even - last bin centered on Nyquist
                Hz = spacing * Nyquist
                if binloc == "center":
                    Hz = Hz + delta_Hz / 2
                    Hz[-1] = Nyquist
            else:
                # odd - last bin just before Nyquist
                Hz = spacing * (Nyquist - delta_Hz)
                if binloc == "center":
                    Hz = Hz + delta_Hz / 2

            offset_dB = self.interp_fn(Hz)
            bins1Hz_dB = 10 * np.log10(delta_Hz)
            result = self.response(h, Hz, offset_dB, bins1Hz_dB)
            self.calibrations[key] = result  # Save for next time

        return result






