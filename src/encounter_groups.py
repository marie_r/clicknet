'''
Created on Aug 15, 2018

@author: jmerindol
'''  
from keras.utils import np_utils
from sklearn.metrics import precision_recall_curve, classification_report
import numpy as np
import math
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from keras import regularizers
from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Dense

import csv
import dateutil.parser
from builtins import input
from collections import defaultdict, namedtuple

# Our classes
from timer import Timer
from sampler import SpreadSamples
from SampleExtractor import SampleExtractor


Interval = namedtuple("Interval", ["start", "end"])

class SpeciesGroups: #definition de notre classe
    """Classe wich defined one species characterized by :
    - start of the detection
    -end of the detection
     
    Classe definissant une espece caracterisee par :
    -le debut de sa detection.
    -la fin de sa detection.
    Constructeur :
    - fichier matlab avec detections.
    - fichier csv avec donnees verifiees.
    - emplacement du repertoire xwav_. 
    """


    
    def __init__(self,start,groundtruth_file,xwav_dir,Fs,samples_N=100000, 
                 low_Hz=5000, high_Hz=92000, min_ppRL_dB = 120):
        """definition des attributs
        start - Contains features for clicks found in a specific deployment
        groundtruth_file - Contains analyst encounters for a specific deployment
        xwav_dir - Audio for deployment
        Fs - sample rate
        samples_N - Each encounter uses no more than samples_N calls
        low_Hz, high_Hz - bandpass samples between these frequencies
            If low_Hz is None, no filtering is done
        min_ppRL_dB - Remove clicks < specified peak-to-peak received level            
        """
        self.data = defaultdict(list)
        self.train_labels_path = start
        self.train_xwav_path = xwav_dir
        self.csv_files = groundtruth_file
        self.Fs = Fs
        self.samples_N = samples_N
        self.low_Hz = 5000
        self.high_Hz = 92000
        
        stopwatch = Timer()
        self.calls = SampleExtractor.build(
            self.train_xwav_path, self.train_labels_path,
            self.Fs, .001, history_s = 0.5)
        print("SampleExtractor %s"%(stopwatch))
    
        # Read the ground truth encounters
        # GT table has following fields:
        # project, site, species, start, end
        Proj, Site, Species, Start, End = range(5)  # indices for accessing        
        with open(groundtruth_file) as csvfile: 
            reader = csv.reader(csvfile,delimiter=",")
            self.encounters = [row for row in reader]
            
        # Remove any encounters that overlap
        self.encounters_filtered = \
                self.non_overlapping_encounters(self.encounters) 

        # Should read this out of Tethys, but everything is
        # 16 bits for now.  Calibration is for counts, not
        # normalized data
        counts_range = np.power(2, 15)
            
        row_idx = 0
        for row in self.encounters_filtered:
            row_idx += 1
            
            species_label = row[Species]
            
            # Sample clicks for this encounter
            
            # first click in encounter
            first = self.calls.findFirstClickIdxAfterAbsoluteTime(row[Start])
            if first == None:
                print("No detections encounter %d: %s"%(row_idx, str(row)))
                continue
            
            first_time = self.calls.getTimeOfClickIdx(first)
            
            if first_time < row[End]:
                # Detected clicks in this encounter
                
                last = self.calls.findFirstClickIdxBeforeAbsoluteTime(row[End])
                # first click outside of encounter
                onepast = last+1
                try:
                    available = last - first+1
                except Exception as e:
                    print(e)
                    pass
                
                    
                
                calls = []
                if onepast - first > samples_N:
                    # Take sample from range                    
                    step_by = np.int(np.floor(available / samples_N))
                    rng = SpreadSamples(available, step_by, first=first)
                else:
                    # samples_N accommodates all samples in this encounter
                    rng = np.arange(available)    
                
                stopwatch.reset()
                used = 0
                skipped = 0
                all_dB = []
                for idx in rng:
                    if low_Hz is not None:
                        # filtering is significantly slower
                        samples = self.calls.getBandPassedSamplesForClickIdx(
                            idx, low_Hz, high_Hz)
                    else:
                        samples = self.calls.getSamplesForClickIdx(idx)
                    # filter click by peak to peak received level
                    # For now, use the same calibration for everything:
                    adj_dB = 70.40 # CINMS 17 B at 24 kHz
                    pp_RL = 20*np.log10(
                        counts_range*(max(samples) - min(samples))) \
                        + adj_dB
                    all_dB.append(pp_RL)
                    if pp_RL >= min_ppRL_dB:                    
                        calls.append(samples)
                        used += 1
                        if used >= samples_N:
                            break
                    else:
                        skipped += 1
                        
                    # debug
                    if (skipped + used) % 1000 == 0:
                        print("used %d skipped %d "%(used, skipped))
                    
                print("%d clicks extracted, processing time %s"%(len(calls), stopwatch))
                if (used > 0):                        
                    # Think about whether we should convert this to a numpy array here
                    self.data[species_label].append(calls)
                else: 
                    print("No clicks with RL_dB > %d in this encounter"%(min_ppRL_dB))
            else:
                print("No clicks detected in encounter")
                
        pass    # processed encounter
    
    """ Verifying data reading = products of the acoustics encounters bases on the csv files from the detections matlab files. 
    (Lecture des donnees verifiees = produits des acoustics encounters bases sur les fichiers csv a partir des fichiers de detections matlab.)
    """
        
    def get_encounters_for_species (self, species_label):
        """get_encounters_for_species(species_label)
        Return list of data for each encounter associated with this species
        """
        return self.data[species_label]

    @staticmethod
    def non_overlapping_encounters (csv_rows):
        """non_overlapping_encounters(labels)
        Given a set of groundtruth encounter labels (DCLDE 2015 format:
        last two columns are start and end time in ISO 8601 format, 
        return the set of encounters that do not overlap with one another.
        The new set has the last two columns parsed using the Python
        dateutil module.
        """
        # Last two columns are start and end time of encounters        
        spans = [Interval(dateutil.parser.parse(x[-2]), dateutil.parser.parse(x[-1]))
                 for x in csv_rows]
        
        # Build sets with indices of overlapping indices
        associated = [None for idx in range(len(spans))]  # Assume nothing overlaps
        last = spans[0].end
        lastidx = 0
        for idx in range(1, len(spans)):
            if spans[idx].start < last:
                # Overlaps with another range
                if associated[lastidx] is None:
                    # First overlap for spans[lastidx]
                    associated[lastidx] = set([lastidx, idx])
                    # Create a pointer to the set for our current index
                    # This is one object with multiple pointers to it.
                    associated[idx] = associated[lastidx]
                else:
                    # Point to the overlap set and note that this one
                    # overlaps too!
                    associated[idx] = associated[lastidx]
                    associated[idx].add(idx)
            if spans[idx].end > last:
                # Current span is longer than any of the others...
                lastidx = idx
                last = spans[idx].end
        
        # Anything that overlaps now has an associated set with it
        # that tells us with which rows it overlaps.  We just return
        # things that don't have an associated set.
        non_overlap = []
        for idx in range(len(spans)):
            if associated[idx] is None:
                # This row was not overlapped.  Use timestamps we
                # already parsed
                row = csv_rows[idx][:-2]
                row.extend([spans[idx].start, spans[idx].end])
                non_overlap.append(row)
                
        return non_overlap
        
    def get_species_labels (self):
        return self.data.keys()

        
    def get_good_data(self):
        return self.good_data
    
    def get_data_testing(self):
        return self.data_testing
    
    def get_data_training(self):
        return self.data_training
    
    def get_labels_training_cat(self):
        return self.labels_training_cat
    
    def get_testing_cat(self):
        return self.labels_testing_cat
        
    """Retourne tous les labels des differentes especes.
    """
