'''
Created on Sep 17, 2018

Sampling schemes

@author: mroch
'''

class SpreadSamples:
    def __init__(self, N, step_by, first=0):
        """SampleUniformly(N, step_by)
        Iteration over this class deterministically takes samples 
        uniformly from a set of N samples.  Each consecutive sample
        is step_by samples from the last one until the end is reached.
        At that point, we wrap around and start taking samples again.
        
        Optional first allows ranges from first:first+N
        
        Example:
        >>> s = SpreadSamples(10,3)
        >>> [v for v in s]
            [0, 3, 6, 9, 2, 5, 8, 1, 4, 7]
        """
        
        self.N = N
        self.step_by = step_by
        self.current = 0
        self.first = first
        
    def __iter__(self):
        """iter(self) - Resets the sampling.  
        Only one iterator at a time is supported
        """
        
        self.current = 0  # reset
        return self
    
    def __next__(self):
        "next() - next sample index"
        
        idx = (self.current * self.step_by) % self.N + self.first
        self.current = (self.current + 1)
        if self.current > self.N:
            raise StopIteration
        return idx
        
