from sklearn.model_selection._split import StratifiedKFold

from collections import namedtuple

TrainTestGrp = namedtuple("TrainTest", ["label", "label_idx", "train", "test"])

class Nfold:
    """Nfold tests for data whose examples must be grouped.
    
    """
    def __init__(self, data, N=3):
        """Nfold(data, N)
        data - dictionary with labels as keys.  Each entry is a list
            of groups of examples.  The group should not be split
            across the train/test boundary
        """
        self.data = data
        self.N = N
        
        self.sizes = {}
        self.foldgen = {}
        
        # Convert label names to numbers 0:N-1
        self.encoder = dict()
        idx = 0
        for k in self.data.keys():
            self.encoder[k] = idx
            idx += 1
        
        for category in data:
            self.sizes[category] = len(data[category])
            foldfactory = StratifiedKFold(N)            
            self.foldgen[category] = foldfactory.split(
                [x for x in range(self.sizes[category])],
                [self.encoder[category] for x in range(self.sizes[category])])
            
    def __iter__(self):
        return self
    
    def __next__(self):
        "next() - Next fold, returns list of TrainTestGrp named tuple"
        splits = []
        for category in self.data:
            (train, test) = next(self.foldgen[category])
            splits.append(TrainTestGrp(category, self.encoder[category],
                                       train, test))
            
        return splits
    
             
