'''
Created on Dec 30, 2018

@author: mroch
'''

# standard modules
import re
import copy
import dateutil.parser
import pytz

# local modules
import xmltodict
from collections import OrderedDict

# Matches list, list<type_name>
list_re = re.compile("list(\((?P<kind>.*)\))?")

def parse(stream):
    """parse(stream)
    Given an input stream, parse the XML and then process type information    
    """

    d = xmltodict.parse(stream)  # Create an XML dictionary
    typed_d = parse_typed_dictionary(d)  # Process type information
    return typed_d
    
def parse_typed_dictionary(d):
        """xmltodictTyped(d)
        Given a dictionary created from XML with type attributes,
        apply the type attributes to the fields.
        
        If no type information is present, field will be treated as a string.
        
        Recognized types:
            int - integer
            float - floating point
            list<kind> - list of int, float, or string, separated by comma
                delimiters unless the field delimiter is present.  if kind
                is string, <string> may be omitted.
                For strings, leading and trailing whitespace between
                delimiters is stripped:  e.g. a,   b,  c --> ['a','b','c']
        """
        
        new_d = copy.deepcopy(d)
        process_types(new_d)
        return new_d

utc = pytz.timezone('UTC')  # Universal coordinated time

def parse_datetime(timestamp):
    """parse_datetime(timestamp)
    parse a timestamp.  Handles timezone aware timestamps (e.g. ISO 8601)
    
    Returns a timestamp.  
    """
    dt = dateutil.parser.parse(timestamp)
    dt_utc = dt.astimezone(utc)  # Force to be stored at UTC time zone
    dt_utc = dt_utc.replace(tzinfo = None)# Strip off the timezone information
    return dt_utc
        
def process_types(d, type_attr="type", list_delimiter=","):
    
    type_attr = "@" + type_attr
       
    for key,value in d.items():
        
        # If the value is a dictionary, it is either a group of subelements
        # or a typed value.

        if isinstance(value, OrderedDict):
            if type_attr in value.keys():
                # obtain the typename and check if it is a list
                typename = value[type_attr]  
                if typename == "list(datetime)":
                    pass
                m = list_re.match(typename)
                list_pred = not m is None   # A list?
                if list_pred:
                    typename = m.group("kind")

                # Determine cast typename
                if typename == "int":
                    cast = int
                elif typename == "float":
                    cast = float
                elif typename == "datetime":
                    cast = parse_datetime
                else:
                    cast = None
                    
                # When attributes are present, the element value is 
                # stored as #text   
                newv = value['#text']
                
                if list_pred:
                    # Split, strip white space and cast if needed
                    replacement = [v.strip() for v in newv.split(list_delimiter)]
                    if cast is not None:
                        replacement = [cast(v) for v in replacement]
                else:
                    replacement = newv.strip()
                    if cast is not None:
                        replacement = cast(newv)
                
                # Replace the OrderedDict with a simple value that has been
                # processed.        
                d[key] = replacement
            else:
                d[key] = process_types(value)
                
        elif isinstance(value, list):
            # key was an element that appeared multiple times in the XML
            # process each of the values associated with it.
            new_values = []
            for v in value:
                new_values.append(process_types(v))
            d[key] = new_values
    return d
        

        
         
        
        
        
        
        