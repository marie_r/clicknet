
import os
import sys
import h5py
from collections import defaultdict
def validate(directory):
    "validate(directory) - Validate feature cache files in directory"

    caches = defaultdict(list)
    for d, _subdir, files in os.walk(directory):
        for f in files:
            if f.endswith(".hd5"):
                try:
                    h5 = h5py.File(os.path.join(d, f), 'r')
                    status = h5.attrs.get('status')
                    h5.close()
                    caches[status].append(f)

                except Exception as e:
                    caches[str(e)].append(f)

    # Show keys and files associated with them
    for k in caches.keys():
        print("%s:\n\t"%(k), end="")
        print("\n\t".join(caches[k]))




if __name__ == "__main__":
    for d in sys.argv[1:]:
        validate(d)