'''
Created on Oct 22, 2018

@author: mroch
'''

# Standard libraries
import cProfile, pstats, io
from datetime import datetime, timedelta
import math, os.path
import argparse
import inspect
import h5py
import pytz


# Add on libraries, add with conda/pip if missing
from keras.utils import np_utils
from keras.models import load_model

from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
import numpy as np
import scipy

# our libraries
from SampleExtractor import SampleExtractor
from timer import Timer
import metrics
from metrics import BinaryMetrics
from encounter_data import (EncounterData, EncounterManager, 
                            build_encounter_managers,
                            precache_encounters)
from classifier.crossvalidator import GroupedBinaryCV
from classifier.click_arch import architectures
from xmltodict_typed import parse
from classifier.analysis import DetectionAnalyzer, score_results
from classifier import feedforward
import classifier.experimentresult
from classifier.experimentresult import CVResultReader

def baseline(spec, t):
    """baseline(spec, t)
    Run Scott & Gurisht's baseline experiments.  We test on a click and the 
    500 us adjacent to it
    
    spec - experiment specification dictionary
    t - Timer object
    
    returns (predictions, labels)  - 0/1 arrays
    0 no click, 1 click    
    """

    model_name = spec['model']
    
    testing_sample_start = [int(s) for s in spec['sample_start'].split(",")]
    clicks_N = int(spec['clicks_N'])
    
    label_file = spec['labels']
    data_path = spec['audio']
    
    model = load_model(model_name)

    Fs = int(spec['Fs'])
    N = 200
    print("Setup")
    sampEx = SampleExtractor.build(data_path,label_file,Fs,.001, history_s=.3)
    #Testing data
    data = []
    labels = []
    
    low_Hz = float(spec['filtering']['low_Hz'])
    high_Hz = float(spec['filtering']['high_Hz'])
    
    print("Getting data")
    clickcount = 0 
    encounter = 0
    for si in testing_sample_start:
        encounter += 1
        print("Starting encounter %d, elapsed time %s"%(encounter, t.elapsed()))
        for i in range(si,si+clicks_N):    #click count range
            clickcount += 1
            click_time = sampEx.absoluteStartOfClicks[i]
            samps = sampEx.getBandPassedSamplesForAbsoluteTime(
                click_time, low_Hz, high_Hz,.001)
            if(len(samps) != N):
                print("Error, incorrect sample length for detection: "+str(i))
                continue
            data.append(samps[0:100])
            labels.append(True)
            data.append(samps[100:200])
            labels.append(False)
            if clickcount % 1000 == 0:
                print("%d clicks, elapsed  %s"%(clickcount, t.elapsed()))
                return
         
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    labels = np_utils.to_categorical(labels, 2)   
     
    examples = np.array(data)
    labels = np.array(labels)
    
    print("Testing")
    predictions = model.predict(examples,batch_size=1000,verbose=1)

    return (predictions[:,1], labels)


# Feature extraction/enhancement
def FE_add_SNR_ppRL_dB(encounterdata, frames, N = 3, bits=16, replace=False):
    """FE_add_SNR_ppRL_dB(frames)
    Compute SNR of peak to peak RL received level in N portions of frames
    
    frames - row oriented audio data, assumed to be floating point
    and will be scaled up to the dynamic range provided by bits
    
    replace - If True, only returns SNR of peak to peak received level (default
        False)
    
    CAVEATS:  Use N responsibly, no error checking is done
    """
    
    if len(frames.shape) > 2:
        framesN, samples, channels = frames.shape
    else:
        framesN, samples = frames.shape
        channels = 1

    seglen = int(samples / N)
    blocks = np.zeros((framesN, N, channels))
    for ch in range(channels):
        for col in range(N):
            blocks[:,col, ch] = encounterdata.compute_peak2peakRL_dB(
                frames[:, col*seglen:(col+1)*seglen, ch],
                bits=bits, peak_freq=False)

    snr = np.ptp(blocks, axis=1)  # peak-to-peak: max - min

    if replace:
        new_frames = snr
    else:
        if channels > 1:
            # snr has a singleton dimension, make explicit for multichannel
            # or we will not be able to append
            snr.shape = (framesN, 1, channels)
        # Append SNR to end of frame
        new_frames = np.concatenate((frames, snr), axis=1)
    return new_frames

def FE_normalize_add_SNR_ppRL_dB(encounterdata, frames, N=3, bits=16):
    """FE_normalize_add_SNR_ppRL_dB
    Compute and append the peak to peak RL as in FE_add_ppRL_dB
    and normalize portion of the data such that the half-wave rectified
    maximum value of the resulting signal is 1.
    The received level SNR is normalized by dividing by 60 dB, a value
       that is associated with the highest clicks
    :param encounterdata: EncounterData object, used for computing RL
    :param frames: frame data to analyze
    :param N: frame paritioned into N pieces for SNR estimate
    :param bits: Original number of sample bits, needed for appropriate RL
    :return: adjusted data
    """

    # SNR is normalized by dividing by this number
    snr_ratio_denom = 60.0

    # Compute SNR RL on unnormalized data
    snr = FE_add_SNR_ppRL_dB(encounterdata, frames, N, bits, replace=True)
    # Reshape SNR to be samples X frames x channels
    snr_reshaped = snr.reshape((snr.size, 1, 1))

    # normalize data
    frames = frames / np.amax(np.abs(frames))
    snr_normalized = snr_reshaped / snr_ratio_denom
    # Add normalized SNR to frame
    new_frames = np.concatenate((frames, snr_normalized), axis=1)

    return new_frames


def FE_TeagerEnergy(_encounterdata, frames):
    """FE_TeagerEnergy(frames, MA)
    Return a smoothed version of the Teager energy (MA frames) 
    """
    # half-wave rectified Teager energy
    teager = np.abs(frames[:,1:-1]**2 - frames[:,1:-1] * frames[:,2:])
    return np.max(teager, axis=1)
        
def FE_diff(_encounterdata, frames):
    "FE_diff(frames) - Return first difference of frames"
    return np.diff(frames, axis=1)    
    
def set_postprocessing(spec, enc_mgrs):
    """
    Given an experiment specificaiton and a list of EncounterManagers,
    set any feature postprocessing for all encounter managers.
    :param spec:
    :param enc_mgrs:
    :return:
    """
    try:
        norm = spec['normalize']
    except KeyError:
        norm = None
    for e in enc_mgrs:
        e.set_normalization(norm)

    # Add postprocessing feature extraction if specified
    try:
        fe = "FE_" + spec['feature_transform']
    except KeyError:
        fe = None
    if fe:
        possibles = globals().copy()
        possibles.update(locals())
        method = possibles.get(fe)
        if not method:
            raise NotImplementedError("feature_transform %s not implemented" % (
                fe))
        else:
            for e in enc_mgrs:
                e.set_feature_extractor(method)

def adhoc(spec):
    """
    adhoc - Create adhoc measurements on data
    :param spec: Experiment specification dictionary
    :return: None
    """
    min_rl = 115  # minimum received level

    enc_mgrs = build_encounter_managers(spec) # Build encounter managers
    set_postprocessing(spec, enc_mgrs)  # Add any postprocessing

    encounters = enc_mgrs[0]  # only one set of encounters

    block = 0
    blocks_with_content = 0
    clicks = []
    noise = []
    progress_every_N = 100
    for encounter in encounters:
        analyzer = DetectionAnalyzer(encounter)  # needed for RL
        block_idx = 0
        for data, gt, start, delta, datablock in encounter:
            block_idx += 1
            indices = np.arange(data.shape[0]) # Compute RL on all frames
            starts, samples, dft, p2p = \
                analyzer.analyze(datablock, indices, start)

            # Find frames above received level threshold
            above_thr = np.greater_equal(p2p, min_rl)
            # Find the clicks and noise above threshold
            click_pred = np.logical_and(gt == 1, above_thr)
            click_indices = np.where(click_pred == True)[0]
            noise_pred = np.logical_and(gt == 0, above_thr)
            if click_indices.size > 0:
                # Save SNR of signal frames above threshold
                clicks.append(data[click_indices, -1])
                # Remove any noise frames immediately following a click
                # as we know there are clicks that span frames
                next_clicks = click_indices + 1
                if next_clicks[-1] == above_thr.size:
                    # Edge case, click in last frame
                    noise_pred[next_clicks[:-1]] = False
                else:
                    noise_pred[next_clicks] = False

            # Save SNR of noise frames above threshold
            noise_indices = np.where(noise_pred == True)[0]
            if noise_indices.size > 0:
                noise.append(data[noise_indices,-1])

            if block_idx % progress_every_N == 0:
                print(f"Block {block_idx} at {datetime.now()}")

        noise_vec = np.hstack(noise)
        clicks_vec = np.hstack(clicks)
        plt.figure(f"{encounter}")
        bins = 250
        plt.hist(noise_vec, bins, alpha=0.5, density=True, label='non-click')
        plt.hist(clicks_vec, bins, alpha=0.5, density=True, label='click')
        plt.legend(loc='upper right')
        plt.xlabel('SNR (dB)')
        plt.ylabel(f'% trial bins $\geq$ {min_rl} dB re. 1 $\mu$Pa ')
        pass  # finished encounter


    pass


def apply_model_to_data(spec):
    """
    apply_mdoel_to_data - Use a trained model on specified data
    A results file is created that is readable by the cross validation
    results reader/writer (classifier.CVResult) and treats the experiment as if
    it was a single fold of a cross validation experiment.
    :param spec:  Experiment specification dictionary
    :return: None
    """

    enc_mgrs = build_encounter_managers(spec) # Build encounter managers
    set_postprocessing(spec, enc_mgrs) # Enable any derived features

    # Show contig reports
    for mgr in enc_mgrs:
        print(mgr)
        contigs = mgr.sampEx._streams.contiguous_data()
        for c in contigs:
            print(c)

    # Retrieve the path to the model to be evaluated and load the model
    model_fname = spec['model']['eval']
    model = load_model(model_fname)
    
    # Merge all EncounterData objects handled by the EncounterManagers
    all_encounters = []
    for e_mgr in enc_mgrs:
        all_encounters.extend(e_mgr.get_encounters())

    # Create experiment directory if needed and open result writer

    # Score against model
    fname = "predictions.h5"
    rootdir = spec['saveroot']
    predlog = classifier.experimentresult.CVResultWriter(
        os.path.join(rootdir, fname))

    feedforward.predict_groups(model, all_encounters, result_writer=predlog,
                               name="predictions")
    print("Predictions of model %s complete"%(model_fname))


def cross_validate(spec):

    enc_mgrs = build_encounter_managers(spec)

    set_postprocessing(spec, enc_mgrs)

    input_N = enc_mgrs[0].get_example_dim()
    config = spec['saveroot']
        
    # Retrieve model generation file based on config model/type
    arch = spec['model']['type']
    modelgen = architectures[arch]
    
    # Find out the parameter names needed by the architecture
    # It is assumed that these have elements in model/parameters
    sig = inspect.signature(modelgen)
    variables = sig.parameters.keys()
    params = spec['model']['parameters']
    missing = []
    args = []
    for v in variables:
        if v == 'input_nodes':
            # Special case, input_nodes is driven by dim of feature space
            args.append(input_N)
        else:
            try:
                args.append(params[v])
            except KeyError:
                missing.append(v)
    if len(missing) > 0:
        raise ValueError("Configuration file %s missing model parameters: " +
                         ", ".join(missing))

    
    design = architectures[arch](*args)

    epochs = int(params['epochs'])
    all_encounters = []
    
    seek_tests = False
    if seek_tests:
        # This code was for verifying that we are able to properly find data
        # We loaded in a second, then compared with data observed in Triton
        e = enc_mgrs[0].encounters[0]
        #gexamp, glabels = e.get_encounter_training_features("next")
        timestamps = [
            #datetime(2011,7,13,0,0,18,576500),
            datetime(2011, 8, 27, 3, 26, 14, 611000),
            datetime(2011, 7, 13, 0,  0, 59, 995000),
            datetime(2011, 7, 13, 0,  0, 59, 995500),
            ]
        for t in timestamps:
            samples = e.sampEx.getSamplesForAbsoluteTime(t, 0.0005)
            plt.plot(samples * 2**15)
            print(t)
            True
    
    for e_mgr in enc_mgrs:
        all_encounters.extend(e_mgr.get_encounters())

    # Show contig reports
    for mgr in enc_mgrs:
        print(mgr)
        contigs = mgr.sampEx._streams.contiguous_data()
        for c in contigs:
            print(c)


    print("Starting cross validation experiment")
    cv = GroupedBinaryCV(all_encounters, design, epochs=epochs,
                         description = spec['metadata'],
                         gen_negatives = spec['negatives'],
                         saveroot = config,
                         minP=0.5,
                         train_models=True)
    print("Cross validation on %s complete"%(spec['saveroot']))


def test_encounters(spec, t, process_data = True, process_labels=True, block_s=60):
    """baseline(spec, t)
    Run experiment over defined subsets of the data.  Differs from baseline
    in that all data between the start and end of effort are examined.
    
    spec - experiment specification dictionary
    t - Timer object
    process_data - Perform classification of data  True|False
    process_labels - Process ground truth labels    True|False
    block_s - Block/chunk size for data processing
    
    returns (predictions, labels, 
             encounter_predictions, encounter_labels, 
             encounter_effort)
             
    predictions[idx] = P(click|time idx)
    labels[idx] = ground truth 0/1 (1 = click)
    
    encounter_predictions, encounter_labels are similar, but on a per
    enc_idx basis
    encounter_effort - list of tuple start and end of effort
    
    If process_data is False, all predictions will be empty
    If process_labels is False, all labels will be empty
    """
    
    model_name = spec['model']
    model = load_model(model_name) if process_data else None

    encounters = EncounterManager(spec)
    
    encounter_predictions = []
    encounter_labels = []
    encounter_effort = []
    
    # Process each effort period --------------------------------------
    enc_idx = 0
    for edata in encounters:
        start_effort = edata.get_start()
        end_effort = edata.get_end()
        enc_idx += 1

        edata.set_name("%d"%(enc_idx))
        edata.set_extraction_metadata(spec['metadata'])
        
        print("Encounter %d: (%s to %s), elapsed t:%s"%(
            enc_idx, start_effort, end_effort, t.elapsed()))
        encounter_effort.append((start_effort, end_effort))        
            
        # List of labels and predictions for each block in enc_idx      
        enc_block_labels = []
        enc_block_predictions = []        
        block_idx = 0
        
        for block_frames, block_labels in edata:
            block_idx += 1 

            if process_data:
                # predict and only keep P(detection)
                block_predictions = model.predict(block_frames)
                block_predictions = block_predictions[:,1]
            else:
                block_predictions = np.zeros([0])

            # Block processed, prime for next
            enc_block_predictions.append(block_predictions)
            enc_block_labels.append(block_labels)
            
            # babysit the user...
            if (block_idx % 10 == 0):
                print(edata.progress())
            
        # End of enc_idx, merge
        encounter_predictions.append(np.hstack(enc_block_predictions))
        encounter_labels.append(np.hstack(enc_block_labels))

        pass
                
         

    predictions = np.hstack(encounter_predictions)
    labels = np.hstack(encounter_labels)

    return (predictions, labels, encounter_predictions, encounter_labels,
            encounter_effort)    




def crossval_results(spec):
    score_results(spec)

    print("Complete")
    # for debug...


def postprocess(spec):
    """postprocess
    Routine for postprocessing results of earlier experiments.  Nothing
    is saved.
    """

    # encounter managers to access the ground truth data
    enc_mgrs = build_encounter_managers(spec)
    for enc_mgr in enc_mgrs:
        for e in enc_mgr:
            # Start & stop time of encounter
            (start, stop) = e.get_span()
            # Determine ground truth timestamps in this interval
            sampEx = e.sampEx  # SampleExtractor for encounter
            # Find range of gt_times within the current encounter
            # gt_start is the index of the first click after the encounter start
            gt_start = sampEx.findFirstClickIdxAfterAbsoluteTime(start)
            # gt_end is the index of the last click before the encounter ends
            gt_end = sampEx.findFirstClickIdxBeforeAbsoluteTime(stop)

            print("%s: gt clicks %d"%(e, gt_end - gt_start + 1))

    # MBARCo used 115 dB threshold developing initial ground truth
    # Set RLpp_thresh to limit to detections above received level
    # In our DetEdited detections, we used a threshold of 112 although the
    # final results were reported with a threshold of 115 as the detection
    # function was showing evidence of missed detections.
    score_results(spec, RLpp_thresh=115)

    # Open the experiment and set parameters
    basefname = os.path.join(spec['saveroot'], 'result')
    r = CVResultReader(basefname + '.h5')
    folds = r.get_foldsN()


    print("Complete")


    waveforms = False
    if waveforms:
        f = 0  # fold index
        # Pick from each of the encounters in the fold
        # We know this from how the BinaryCrossVidator is constructed,
        # but it should eventually be stored
        test_encounter_indices = [1] #(0, 1)  
        for e_idx in test_encounter_indices:
            m = BinaryMetrics(deep.get_predictions(f,e_idx), 
                              deep.get_labels(f,e_idx))
            encounter = encounters[e_idx]
            
            N = 5
            while N > 0:  
                _f_h, _ax_h = m.plot_detections(.95, encounters.sampEx, 
                                              encounter.get_start())
                N -= 1
                _f_h.clf()
            


    print('end of line')

def score_model(spec, method="normal"):
    """score_model(spec)
    Drive experiment whose parameters are specified in dictionary spec
    """
    t = Timer()
    s_per_h = 3600.0  # seconds per hour
     

    if method == "crossval":
        # Train and classify using N-fold cross validation
        cross_validate(spec)
    elif method == "crossval_results":
        # Experiment has already been run, interpret results
        # with respect to ground truth and generate metric plots
        crossval_results(spec)
    elif method == "eval":
        apply_model_to_data(spec)
    elif method == "postprocess":
        postprocess(spec)
    elif method == "adhoc":
        adhoc(spec)
    elif method == "train":
        raise NotImplementedError()
    else:
        raise ValueError("Bad method: " + method)
        

    
    print("Breakpoint line")
    
    
if __name__ == '__main__':
    plt.ion()
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--method",
                        default=None,
                        choices=["eval", "crossval", "crossval_results", "gencache", "baseline", "postprocess", "adhoc"],
                        help=""""
Processing method:

  Most methods produce or use HDF5 files containing experimental results.
  All files are stored in a directory that is the same name as the experiment
  specification and placed as a sibling of the specification.
  
  crossval - N-fold cross validation
  
  gencache - Generate feature and label cache files with no other processing
  
  eval - Run an experiment and compute the results.  Writes model and result
  files

  baseline - Older code, only used for regression testing, analysis
  effort undersamples negative cases and is not representative of 
  actual performance.
  
  postprocess - Used for post-processing analysis.
  
  Can be used to override /model/method in the XML configuration file 
""")
    parser.add_argument("xml", help="XML specification file")
    args = parser.parse_args()

    # Read configuration
    print("Method %s, specification file:  %s"%(args.method, args.xml))    
    spec = parse(open(args.xml, 'rb'))
    
    # Save location based on configuration file name
    [fname, ext] = os.path.splitext(args.xml)
    spec['experiment']['saveroot'] = fname
    if not os.path.isdir(spec['experiment']['saveroot']):
        os.mkdir(spec['experiment']['saveroot'])
    fname = fname
    spec['experiment']['save_to'] = fname
    spec['experiment']['name'] = os.path.basename(fname)
    spec['experiment']['metadata'] = open(args.xml, 'r').read() 
    
    if args.method is None:
        try:
            method = spec['experiment']['method']
        except KeyError:
            method = "eval"
    else:
        method = args.method
    #parser.parse_args("-m", method)   # validate method

    if method == "postprocess":
        # Various and sundry analyses of previously computed experiments
        # (playground)
        postprocess(spec['experiment'])
    elif method == "crossval_results":
        # Show results of a previously computed cross validation experiment
        crossval_results(spec['experiment'])
    else:
        score_model(spec['experiment'], method)
        
    plt.show()
    

