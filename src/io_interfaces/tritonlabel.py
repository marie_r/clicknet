"""
tlab_io
An input/output model for creating Triton label files.
Triton is a Matlab program for analysis of long-term timeseries data
https://github.com/MarineBioAcousticsRC/Triton
"""

# standard modules
import datetime
import struct

# our modules
from datetime2matlab import Datetime2MatlabSerial

class Labels:
    def __init__(self, label=None):
        """
        Labels constructor
        :param label: if set, all annotations have the specified label, otherwise
            each annotation is expected to have its own label
        """
        # Serial timestamp lists for start and end
        self.ser_starts = []
        self.ser_ends = []

        # Verify that if label is set, it is a string
        if not (label is None) and not (isinstance(label, str)):
            raise ValueError("When specified, label must be a string")

        self.label = label
        self.labels = []

    def addcenter(self, times, labels=None, offset=datetime.timedelta(microseconds=200)):
        """
        add - Add annotion entries
        :param times:  list of datetimes representing center of annotation
        :param labels: per annotation labels, must be present if object
           was constructed with label=None
        :param offset: pad time around detection center
        :return: None
        """

        # labels must be a single label that will be repeated or of the same length
        if self.label is None and isinstance(labels, list):
            self.labels.extend(labels)

        # Perform padding
        starts = [t - offset for t in times]
        ends = [t + offset for t in times]

        converter = Datetime2MatlabSerial()
        # Convert times to serial dates
        ser_starts = converter.datetime2matlabdn(starts, triton=True)
        ser_ends = converter.datetime2matlabdn(ends, triton=True)

        self.ser_starts.extend(ser_starts)
        self.ser_ends.extend(ser_ends)

    def save(self, filename):
        """
        save
        :param filename:  Write Triton label annotations to file
        :return:
        """
        h = open(filename, mode='wb')

        multi = self.label is None  # Multiple labels?
        if not multi:
            # Only one label, convert it to a byte string
            label = ("%s\n" % (self.label)).encode("ascii")

        for idx in range(len(self.ser_starts)):
            # Convert to binary string and write
            datespan = struct.pack("dd", self.ser_starts[idx], self.ser_ends[idx])
            h.write(datespan)

            # Format annotation specific label if needed, otherwise uses
            # single annotation defined above
            if multi:
                label = ("%s\n" % (self.labels[idx])).encode("ascii")  # update detection label

            h.write(label)


def write_padcenter(filename, times, labels=None,
                    offset=datetime.timedelta(microseconds=200)):
    """write_padcenter - Write a Triton label file.  Requires start and end,
    so we will pad around the detection time by +/- offset (TimeDelta) to determine
    the start and end.

    :param filename:  Triton label file to write
    :param times: List of datetimes
    :param labels:  Single label for all detections or list of labels
    :param offset:  Pad time around detection time
    :return:  None
    """

    # Create a single or multiple label Labels object and add data
    multi = isinstance(labels, list)
    if multi:
        annotations = Labels()
        annotations.addcenter(times,labels,offset)
    else:
        annotations = Labels(label = labels)
        annotations.addcenter(times,offset=offset)

    annotations.save(filename)






