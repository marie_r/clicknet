"""
detedit - Input/output operations for reading and writing detedit files

DetEdit: A graphical user interface for annotating and editing events detected in long-term acoustic monitoring data
Solsona-Berga A, Frasier KE, Baumann-Pickering S, Wiggins SM, Hildebrand JA (2020).
PLoS Computational Biology 16:e1007598. 10.1371/journal.pcbi.1007598

"""

# Standard imports

import os

# 3rd party imports
import scipy
import numpy as np

# our modules
from datetime2matlab import Datetime2MatlabSerial

class Detections:
    """
    Detections
    Read or Write detectionsto be processed by detedit.

    see:  https://github.com/MarineBioAcousticsRC/DetEdit
    and Solsona-Berga et al. 2020, DetEdit in PLOS Computational Biology
        https://doi.org/10.1371/journal.pcbi.1007598

    The DetEditWriter class creates the so-called TPWS file:
    https://github.com/MarineBioAcousticsRC/DetEdit/wiki/How-It-Works

    Writes out Matlab file with:
    MTT - An [N x 1] vector of detection times, where N is the
         number of detections.
    MPP - An [N x 1] vector of peak to peak received level (RL) amplitudes.

    MSP - An [N x nF] matrix of detection spectra, where nF is the length of
        the spectra.
    MSN - An [N x maxSampleWindow] matrix of detection timeseries.

    Pdet - An [N x 1] vector denoting the probability (detection score) of each
         detection (not currently used by DetEdit)

    f = An Fx1 frequency vector associated with MSPs

    Fs - sample rate

    To use detedit, in addtion to this TPWS file, you will need to have:
    1. an LTSA file
    2. A settings file

    Use DetEdit's mkLTSAsessions to create encounter-level LTSAs, or the
    so-called LTSA snippets.
    """

    timestamp_conv = Datetime2MatlabSerial()  # Time converter

    def __init__(self, Fs, freq_bins_Hz):
        self.det = {
            "MTT" : [],  # detection times as Matlab serial dates
            "MPP" : [],  # peak-to-peak RL
            "MSP" : [],  # Spectra
            "MSN" : [],  # Timeseries
            "Pscore" : [], # Detection scores  # Not an official TPWS param
            "Fs" : Fs,   # Sample rate
            "f" : freq_bins_Hz # frequency bin labels
        }

    def clear(self):
        "clear - Remove detection data (other data, e.g. Fs, are retained)"
        for n in ("MTT", "MPP", "MSP", "MSN", "Pscore"):
            self.det[n] = []

    def add(self, starts, peak2peakRL, spectra, timeseries, Pscore):
        """
        add M detections
        :param starts: list of M detection datetimes
        :param peak2peakRL: received level
        :param spectra: MxN matrix of spectra
        :param timeseries: MxN matrix of samples
        :param Pscore: probability/score of detection
        :return: None
        """
        self.det["MTT"].extend(self.timestamp_conv.datetime2matlabdn(starts))
        self.det["MPP"].append(peak2peakRL)
        self.det["MSP"].append(spectra)
        self.det["MSN"].append(timeseries)
        self.det["Pscore"].append(Pscore)

    def merge(self):
        "Merge multiple datasets added with add into a single instance"

        # Convert everything to single arrays
        self.det["MTT"] = np.asarray(self.det["MTT"])
        self.det["MPP"] = np.hstack(self.det["MPP"])
        self.det["MSP"] = np.vstack(self.det["MSP"])
        self.det["MSN"] = np.vstack(self.det["MSN"])
        self.det["Pscore"] = np.hstack(self.det["Pscore"])

    @classmethod
    def load(self, filename):
        """
        load - read in data from specified DetEdit file
        :param filename:  DetEdit file to load
        :return: DetEditFile
        """

        det_file = Detections(None, None)  # new instance
        det_file.det = scipy.io.loadmat(filename)  # load data
        # Convert to Python datetimes
        det_file.det["MTT"] = self.timestamp_conv.matlabdn2datetime(self.det["MTT"])
        return det_file

    def save(self, filename):
        """
        save
        :param filename:  Save DetEdit detections to specified file
        :return: None
        """

        self.merge()
        scipy.io.savemat(filename, self.det)













