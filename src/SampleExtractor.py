'''
Created on Apr 17, 2018

@author: gsingh
'''

# Library builtins
import pickle,gzip,traceback
import os
import os.path
import math
import pytz
import re
import sys
from datetime import datetime, timedelta
from fractions import gcd

# Add on packages
import numpy as np
import h5py # HDF5 interace
import scipy.io
import scipy.signal
import matplotlib.pyplot as plt

# Local packages
from dsp.abstractstream import StreamGap, StreamEnd
from dsp.FrameStream import AudioFrames
from dsp.AudioStreamDescriptor import XWAVhdr, WAVhdr
import dsp.SampleStream as SampleStream
from timer import Timer


    
class SampleExtractor:
    
    """SampleExtractor 
    Class for processing non-contiguous detections
    """
    
    date_re = re.compile(""".*
        _(?P<year>\d\d(\d\d)?)(?P<month>\d\d)(?P<day>\d\d)
        _(?P<hour>\d\d)(?P<min>\d\d)(?P<sec>\d\d)
        \..*
        """, re.RegexFlag.VERBOSE)
    
    def __init__(self, wavPath, matPath,Fs,stepsize_s, history_s=65):
        '''
        SampleExtractor(wavPath, matPath, Fs, stepsize_s, history_s)
        wavPath - directory containing audio files.  All audio files will
            be assembled into this SampleExtractor
        matPath - directory containing call start times
        Fs - Presumed sample rate of all files
        stepsize_s - Duration in seconds of trial bin
        history_s - Buffer size when reading
        '''
        self.wavPath = wavPath      #directory containing xwav files
        self.matPath = matPath      #directory containing detEdited matlab click times
        self.Fs = Fs
        self.delta_sample = timedelta(seconds=1/Fs)  # time between samples
        self.stepSize_s = stepsize_s
        self.history_s = history_s
        
        # Cache of finite impulse response filters for bandpassing data
        # This will be constructed as two-level dictionary where the first
        # key is the start-pass frequency and the second key is the stop-pass
        # frequency.
        self._bell_approx = {}  

        
        #Calling relevant methods required on instantiation
        print("Sample Extractor\n\tGathering audio files...")
        self.getWavPaths()
        if self.has_labels():
            print("\tGathering and converting label files...")
            self.loadLabelTimes()
            self.convertTimeStamp()
        print("\tSample Extractor initialized")
    
    def has_labels(self):
        "has_labels() - Are labels available?"
        return self.matPath is not None
        
    @staticmethod
    def build(audioDir, labelDir, fs, stepsize_s, cache_dir=".", history_s=65):
        '''
        Create a SampleExtractor
        :param audioDir:  Directory containing audio files
        :param labelDir:  Directory containing label fils (or None) if unavailable
        :param fs: target sample rate of data
        :param stepsize_s: duration of trial bin in seconds
        :param cache_dir: Location to cache SampleExtractor objects (no longer used)
        :param history_s: Buffer read size (seconds)
        :return:
        A SampleExtractor object is stored/loaded.
        '''

        # fname = os.path.join(cache_dir,
        #                      os.path.basename(matPath) +
        #                      str(fs) + str(stepsize_s) + ".pkz")
        # if os.path.isfile(fname):
        #     print("SampleExtractor is using saved file: "+fname)
        #     with gzip.open(fname) as f:
        #         se = pickle.load(f)
        #         se.history_s = history_s
        #
        #     # If loaded on a different machine, the path may be wrong, fix it
        #     if se.wavPath != wavPath:
        #         se.wavFileNames = [f.replace(se.wavPath, wavPath, 1)
        #                            for f in se.wavFileNames]
        #         se.wavPath = wavPath
        #
        #     if not hasattr(se, '_bell_approx'):
        #         # Older versions of this class did not instantiate
        #         # this in the constructor
        #         se._bell_approx = {}
        #
        # else:
        se = SampleExtractor(audioDir, labelDir, fs, stepsize_s, history_s)
        # print("...Writing File...")
        # with gzip.open(fname,mode='wb') as f:
        #     pickle.dump(se,f)
        # print("...Done!...")
            
        se._setupStreams()
        print("Done")
        return se
    
    def _setupStreams(self):
        "_setupStreams() - Initialize the sample streamer"

        # Create a stream descriptor object and create an entry for each file
        # we will be processing.
        self._streams = SampleStream.Streams()

        for i in range(0,len(self.wavFileNames)):
            audiofile = self.wavFileNames[i]
            audiofile_norm = audiofile.lower()   # normalize case for checks
            if audiofile_norm.endswith(".x.wav"):
                hdr = XWAVhdr(audiofile)  # extended wave file format
            elif self.wavFileNames[i].endswith(".wav"):
                hdr = WAVhdr(audiofile)

            # Analyze the audio file to determine information needed to make
            # a streams (look for file gaps, extract timestamps, etc.)
            self._streams.add_file(*hdr.get_stream_descriptor())

        self._sampleStream = SampleStream.SampleStream(self._streams,
            targetFs = self.Fs, maxhistory=int(self.Fs*self.history_s))

    
    def getWavPaths(self):
        '''
        getWavPaths() - Initializes instance variable wavFileNames
        with a list of audio files
        '''
        rootfolder = self.wavPath
        self.wavFileNames = []
        audio_ext = [".wav"]  # Audio file extension

        if not os.path.exists(rootfolder):
            raise ValueError("%s : directory does not exist"%(rootfolder))

        # Gather up all files
        for dirpath, dirnames, filenames in os.walk(rootfolder):
            for f in filenames:
                (_, ext) = os.path.splitext(f)
                if ext in audio_ext:
                    self.wavFileNames.append(os.path.join(dirpath, f))

        if len(self.wavFileNames) == 0:
            print("%s CONTAINS NO AUDIO FILES"%(rootfolder))

    def getAudioRoot(self):
        "getAudioRoot() - Returns root directory of audio data"
        return self.wavPath
    
    def getAudioNamesRelativeToRoot(self):
        """getAudioNamesRelativeToRoot()
        Returns list of audio names with root removed
        """
        names = [f.replace(self.wavPath, "", 1) 
                 for f in self.wavFileNames]
        return names
        
        
    def loadLabelTimes(self):
        '''loadLabelTimes()
        Reads a list of start times of clicks and saves them in
        self.startOfClicks
        '''
        
        # Open data either in HDF or Matlab format 
        try:
            f = h5py.File(self.matPath,'r')
        except:
            f = scipy.io.loadmat(self.matPath)
        # Convert data to a dictionary keyed on variable name
        contents = dict(f.items())        
        
        possible_names = [
            'click_times',  # Name used in Matlab merge: identify_encounters
            'allLabels', # Name used in HDF5 format
            'MTT' # Name used in Matlab TPWS files
            ]
        found = False
        idx = 0
        while not found and idx < len(possible_names):
            if possible_names[idx] in contents:
                found = True
                data = contents[possible_names[idx]]
                if type(data) is h5py.Dataset:  
                    data = data[:]  # Extract HDF5 data
                self.startOfClicks = data

                # Retrieve peak-to-peak received level if present
                if 'MPP' in contents:
                    peakscontainer = contents['MPP']
                    self.p2pRL = peakscontainer[:]
            idx += 1
        
        if not found:
            raise RuntimeError('Could not identify label times in file %s'%(
                self.matPath))
    
    def convertTimeStamp(self):
        '''
        Return click times in python date format.  Assume that all are UTC
        '''
        
        starttimes = []
        dims = self.startOfClicks.shape
        if np.min(dims) > 1:
            # Labels contain class identifier, ignore
            matlab_datenum = self.startOfClicks[0,:]
        else:
            matlab_datenum = self.startOfClicks

        if type(matlab_datenum[0]) is np.ndarray:
                matlab_datenum = np.array(matlab_datenum).flatten()
        for i in range(0, len(matlab_datenum)):            
            mdn = matlab_datenum[i]
            tstamp = datetime.fromordinal(int(mdn)) + timedelta(days=mdn%1) - timedelta(days = 366)
            if tstamp.year < 1000:
                tstamp = datetime(tstamp.year + 2000,tstamp.month,tstamp.day,
                                  tstamp.hour,tstamp.minute,tstamp.second,
                                  tstamp.microsecond)
            starttimes.append(tstamp)

        self.absoluteStartOfClicks = starttimes
        return starttimes
    
    def getWavFileIdx(self,targettime):
        '''
        Returns exact index of xwav file for click time
        '''
        
        return (np.argmax([x > targettime for x in self.wavDates]) - 1)

    '''Helpers for public function'''
    
    def getStreams(self):
        "getStreams() - expose the underlying SampleStream implementation"
        return self._streams

    def getFs(self):
        "getFs - Return sample rate"
        return self.Fs

    def getBitDepth(self):
        """getBitDepth -
        Return # bits per sample in underlying data stream.  Samples are
        normalized [-1, 1], but this gives the information to convert back
        to samples if necessary
        """
        return self._sampleStream.get_current_bitdepth()

    def getClickIdxForAbsTime(self,absTime):    #Need to fix logical error
        ''' returns closest click index for a specific time value'''
                    
        if not hasattr(self, '_prevIdx'):
            self._prevIdx = 0
        
        t = self.absoluteStartOfClicks[self._prevIdx] 
        while absTime < t and self._prevIdx > 0:
            self._prevIdx -= 1
            t = self.absoluteStartOfClicks[self._prevIdx] 
        for idx in range(self._prevIdx,len(self.absoluteStartOfClicks)):
            t = self.absoluteStartOfClicks[idx]
            if absTime < t:
                self._prevIdx = idx
                #t is currently the start time of gt after the current abstime
                #We need to see if current overlaps
                if absTime + timedelta(seconds=self.stepSize_s) >= t:
                    #we overlap
                    return idx
                if idx == 0:
                    #bad nes, cant check click before
                    return None
                t = self.absoluteStartOfClicks[idx-1]
                #t is the start time of the click before the abstime
                if absTime >= t + timedelta(seconds=self.stepSize_s):
                    return None
                else:
                    return idx-1
    def findFirstClickIdxAfterAbsoluteTime(self, time, debug=False):
        """findFirstClickIdxAfterAbsoluteTime(time) - Search for the first 
        click at or after a specified time.  Returns click index or None if no
        such click exists.
        
        Optional debug flag shows binary search on console
        """
                    
        # Game over if time is after the last click
        if time > self.absoluteStartOfClicks[-1]:
            return None
        
        # Start in the middle
        lower = 0
        upper = len(self.absoluteStartOfClicks)
        cur_idx = int(round(upper/2))        
        cur_time = self.absoluteStartOfClicks[cur_idx]
        
        # Guaranteed to find a click
        found = False
        msg = ""
        while not found:
            if cur_time < time:
                # too early, we have a new lower bound
                lower = cur_idx
            else:
                # correct click if it is the first one or the one before it
                # is prior to the goal time
                found = cur_idx == 0 or \
                    self.absoluteStartOfClicks[cur_idx-1] < time
                if not found:
                    # too late, we have a new upper bound
                    upper = cur_idx
                    
            if not found:
                # Move to middle of current bounds
                prev_idx = cur_idx
                cur_idx = int(round((upper+lower)/2.0))
                if prev_idx == cur_idx:
                    print("Rounding bug, need to handle...")
                    pass
                cur_time = self.absoluteStartOfClicks[cur_idx]
            
            if debug:
                if found:
                    msg = "found"
                print("[%d->%s, %d->%s, %d->%s] goal %s %s"%(
                    lower, self.absoluteStartOfClicks[lower], 
                    cur_idx, cur_time, 
                    upper, self.absoluteStartOfClicks[upper], 
                    time, msg))
            pass
            
        return cur_idx
    
    def findFirstClickIdxBeforeAbsoluteTime(self, time, debug=False):            
        """findFirstClickIdxBeforeAbsoluteTime(time) - Search for the first 
        click at or after a specified time.  Returns click index or None if no
        such click exists.
        
        Optional debug flag shows binary search on console
        """
        
        # Game over if time is before the first click
        if time < self.absoluteStartOfClicks[0]:
            return None
        
        N = len(self.absoluteStartOfClicks)
        # Start in the middle
        lower = 0
        upper = N
        cur_idx = int(round(upper/2))        
        cur_time = self.absoluteStartOfClicks[cur_idx]
        
        # Guaranteed to find a click
        found = False
        msg = ""
        while not found:
            if cur_time > time:
                # too late, we have a new upper bound
                upper = cur_idx
            else:
                # correct click if it is the last one or the one after it
                # is prior to the goal time
                found = cur_idx + 1 >= N or \
                    self.absoluteStartOfClicks[cur_idx+1] > time
                if not found:
                    # too early, we have a new lower bound
                    lower = cur_idx
                    
            if not found:
                # Move to middle of current bounds
                prev_idx = cur_idx
                cur_idx = int(round((upper+lower)/2.0))
                if prev_idx == cur_idx:
                    print("Rounding bug, need to handle...")
                    pass
                cur_time = self.absoluteStartOfClicks[cur_idx]
            
            if debug:
                if found:
                    msg = "found"
                print("[%d->%s, %d->%s, %d->%s] goal %s %s"%(
                    lower, self.absoluteStartOfClicks[lower], 
                    cur_idx, cur_time, 
                    upper, self.absoluteStartOfClicks[upper], 
                    time, msg))
            pass
            
        return cur_idx
    
    def getAllClickIdxBetweenTimes(self,start,end):
        """getAllClickIdxBetweenTimes(start,end)
        Find all click indices between specified start and end time
        """

        start_idx = 0                    

        #run start index to first index after start
        while ( start_idx < len(self.absoluteStartOfClicks) 
                and self.absoluteStartOfClicks[start_idx] < start ):
            start_idx += 1
        end_idx = start_idx
        #run end idx to first index after end
        while ( end_idx < len(self.absoluteStartOfClicks) 
                and self.absoluteStartOfClicks[end_idx] < end ):
            end_idx += 1
        return list(range(start_idx,end_idx))
    
    def getSampleIdxForTime(self,absTime):
        ''' returns sample index for any absolute time value '''
        
        fidx = self.getWavFileIdx(absTime)
        delta = absTime - self.wavDates[fidx]
        return math.floor((delta.seconds + delta.microseconds/1000000)*self.Fs)
    
    def getTimeOfClickIdx(self, click_idx):
        "getTimeOfClickIdx(click_idx) - Return start time of Nth click"
        return self.absoluteStartOfClicks[click_idx]
        
    def cvtAbsTimeToRelativeTimeDelta(self,abstime):
        '''cvtAbsTimeToRelativeTimeDelta(abstime)
        Find timedelta relative to the file that contains abstime
         '''
            
        fidx = self.getWavFileIdx(abstime)
        delta = abstime - self.wavDates[fidx]
        return delta
    
    def cvtSampleIdxToRelativeTimeDelta(self,idx):
        ''' '''
        return timedelta(microseconds=((idx/self.Fs)*1000))
    def cvtIdxToAbsTimeRelativeToAbsTime(self,abstime,idx):
        ''' '''
        return  abstime + timedelta(seconds=idx/self.Fs)
    
    def _buildBandPassFilter(self,low_stop,high_stop,stopband_width,fs=None,atten_db=80,ripple_tolerance=10**-4):
        if(fs == None):
            fs = self.Fs
        fred_harris_tap_approx = atten_db/(22*(stopband_width/fs))
        atten_ratio = 10**(-atten_db/10)
        bellanger_tap_approx = (2/3)*np.log10(1/(10*atten_ratio*ripple_tolerance))*(fs/stopband_width)
        fred_harris_tap_approx = int(fred_harris_tap_approx)
        bellanger_tap_approx = int(bellanger_tap_approx)
        if fred_harris_tap_approx % 2 == 0:
            fred_harris_tap_approx += 1
        if bellanger_tap_approx % 2 == 0:
            bellanger_tap_approx += 1
        nyq = int(fs/2)
        bands = (0,low_stop-stopband_width,low_stop,high_stop,high_stop+stopband_width,nyq)
        desired = (0,0,1,1,0,0)
        fred_fir = scipy.signal.firls(fred_harris_tap_approx, bands, desired, nyq=nyq)
        bellanger_fir = scipy.signal.firls(bellanger_tap_approx, bands, desired, nyq=nyq)
        return {"fred_approx":fred_fir,"bellanger_approx":bellanger_fir}
    
    def getBandPassedSamplesForAbsoluteTime(self,abs_time,lf,hf,numSeconds=1,fir=None,resample_targetFs=None):
                
        if(type(fir) == type(None)):
            #cache bellanger fir approximated filters based on low freq and high freq cutoffs
            if not (lf in self._bell_approx):
                self._bell_approx[lf] = {}
            if not (hf in self._bell_approx[lf]):
                filters = self._buildBandPassFilter(lf,hf,1000)
                self._bell_approx[lf][hf] = filters["bellanger_approx"]
            fir = self._bell_approx[lf][hf]
        numSamplesToPad = len(fir)
        if(type(resample_targetFs) == type(None)):
            extraTime = (numSamplesToPad*2)/self.Fs
            data = self.getSamplesForAbsoluteTime(abs_time-timedelta(seconds=(extraTime/2)), numSeconds+extraTime)
        else:
            extraTime = (numSamplesToPad*2)/resample_targetFs
            data = self.getResampledSamplesForAbsoluteTime(abs_time-timedelta(seconds=(extraTime/2)),resample_targetFs,numSeconds+extraTime)
        # filter forwards-backwards for zero-phase filtering
        filtered = scipy.signal.filtfilt(fir,[1],data,padlen=min(len(data)-1,3*len(fir)))
        return filtered[numSamplesToPad:-numSamplesToPad]
    
    def getBandPassedSamplesForClickIdx(self, click_idx, lf, hf, new_Fs=None):
        """getBandPassedSamplesForClickIdx(click_idx, lf, hf, resample_targetFs=None)
        Retrieve a bandpassed echolocation click - click is padded before filter
        and padding is removed.
        
        click_idx - detection index
        lf, hf - bandpass filter corners
        new_Fs - Resample to new specified rate        
        """
        
        return self.getBandPassedSamplesForAbsoluteTime(
            self.absoluteStartOfClicks[click_idx], lf, hf, 
            self.stepSize_s, resample_targetFs = new_Fs)
    
        
        
    def getSamplesForClickIdx(self,click_idx):
        ''' return samples for a specific click start index'''
        return self.getSamplesForAbsoluteTime(self.absoluteStartOfClicks[click_idx], self.stepSize_s)
    
    def getSamplesForAbsoluteTime(self,absTime,numSeconds=1):
        ''' returns samples for a specific time'''
                
        self._sampleStream.set_time(absTime)
        samplesToRead = int(self.Fs * numSeconds)
        samples = []
        try:
            data = self._sampleStream.read(samplesToRead)
            samples = np.squeeze(data[0])
        except StreamGap as e:
            print("UhOh! We don't know what to do with gaps!")
            print(e)
            raise e
        return samples
    
    def getResampledSamplesForAbsoluteTime(self, abs_time, targetFs, numSeconds=1):
        ''' 
        calculates resampled data for different freq i.e. 320kz
        and plots waveform comparison between original 
        predicted data and resampled data
        '''
                
        bottom = gcd(self.Fs,targetFs)
        up = targetFs/bottom
        down = self.Fs/bottom
        out_pad_count = 5 #dependent on the window used in the resample_poly, which defaults to a kaiser with 5 samples
        numSamplesToPad = out_pad_count*down/up
        extraTime = (numSamplesToPad*2)/self.Fs #5 sammples on each side
        pred_data = self.getSamplesForAbsoluteTime(abs_time-timedelta(seconds=(extraTime/2)), numSeconds+extraTime)
        
        filtered_data = scipy.signal.resample_poly(pred_data, up, down)
        filtered_data = filtered_data[out_pad_count:-out_pad_count]
        return filtered_data
#         plt.plot(pred_data[int(numSamplesToPad):-int(numSamplesToPad)])
#         plt.title('predicted data')
#         plt.figure()
#         plt.plot(filtered_data)
#         plt.title('resampled data')
#         plt.show()
    
    def getGT(self, absTime, numSecondsToPredict):    
        ''' maps each ground truth detection to fixed size bins'''
        
        binCount = numSecondsToPredict/self.stepSize_s
        return [self.getClickIdxForAbsTime(absTime+timedelta(seconds=self.stepSize_s*i)) for i in range(binCount)]
        
        