import math,time,struct
from datetime import datetime, timedelta

class TritonLabeler:
    
    def __init__(self,filename,fmt="Binary"):
        if(fmt != "Binary"):
            raise ValueError("ERROR! CAN CURRENTLY ONLY OUTPUT BINARY FILES")
        self._fname = filename
        self._fmt = fmt
        
    @staticmethod
    def cvtMatlabDateNumToDateTime(dnum):
        return datetime.fromordinal(int(dnum)) + timedelta(days=dnum%1) - timedelta(days = 366)
    @staticmethod
    def cvtDateTimeToMatlabDateNum(dt,triton_offset=2000):
        #fix triton year offset
        dt = datetime(dt.year - triton_offset,dt.month,dt.day,dt.hour,dt.minute,dt.second,dt.microsecond)
        #fix difference in epoch for matlab vs pyton
        dt += timedelta(days=366)
        #calculate dnum double
        return dt.toordinal() + (dt.hour/24 + dt.minute/(24*60) + dt.second/(60*60*24) + dt.microsecond/(1000*1000*60*60*24))
    '''
        Writes the start/stop labels to the filename,
        truncating the file.
    '''
    def write(self,starts,stops):
        out = []
        
        with open(self._fname,mode='wb') as f:
            #<d -- The < format means little endian(triton assume le), d is double percision
            for i in range(0,len(starts)):
                bin_st = self.cvtDateTimeToMatlabDateNum(starts[i])
                bin_sp = self.cvtDateTimeToMatlabDateNum(stops[i])
                bin = struct.pack('<dd',bin_st,bin_sp)
                f.write(bin)
                f.write('\n'.encode('ascii'))