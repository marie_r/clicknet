'''
Created on Apr 17, 2018

@author: slindeneau
'''

import numpy as np
import math
from datetime import datetime, timedelta
from scipy import signal
import matplotlib.pyplot as plt

from SampleExtractor import SampleExtractor

def driver():
    #numSecondsToGet = .001 #exact click
    #predictAbsTimeStart = datetime(2011, 12, 26, 3, 20,0,593000 ) #exact click
    numSecondsToGet = 1 #
    predictAbsTimeStart = datetime(2011, 12, 26, 3, 20) #
    train_labels_path = '/cache/slindeneau/6-2018/ASA-5-GT/CINMS_B_111226_000500_5_clicks.bcTg_Delphin_TPWS1.mat_TD.mat'
    train_xwav_path = '/media/dclde/DCLDE2015_HF_training/CINMS_B/CINMS17B_1/'
    Fs = 200000
    sampEx = SampleExtractor.build(train_xwav_path,train_labels_path,Fs,.001)
    
    filters = sampEx._buildBandPassFilter(5000,92000,1000)
    fig, axs = plt.subplots(1)
    hs = list()
    ax = axs
    nyq = Fs/2
    bands = (0,4000,5000,92000,93000,100000)
    desired = (0,0,1,1,0,0)
    for fir in (filters["fred_approx"],filters["bellanger_approx"]):
        freq, response = signal.freqz(fir)
        hs.append(ax.semilogy(nyq*freq/(np.pi), np.abs(response))[0])
    for band, gains in zip(zip(bands[::2], bands[1::2]), zip(desired[::2], desired[1::2])):
        ax.semilogy(band, np.maximum(gains, 1e-7), 'k--', linewidth=2)
    
    ax.legend(hs, ('fred_approx', 'bellanger_approx'), loc='lower center', frameon=False)
    ax.set_xlabel('Frequency (Hz)')
    ax.grid(True)
    ax.set(title='Band-pass %d-%d Hz' % bands[2:4], ylabel='Magnitude')

    fig.tight_layout()
    plt.show()
    
    unfiltered = sampEx.getSamplesForAbsoluteTime(predictAbsTimeStart,numSecondsToGet)
    filtered = sampEx.getBandPassedSamplesForAbsoluteTime(predictAbsTimeStart,5000,92000,numSecondsToGet)
    fig, axs = plt.subplots(2)
    axs[0].plot(unfiltered)
    axs[1].plot(filtered)
    axs[0].set(title="Original")
    axs[1].set(title="BandPassed")
    
    plt.show()
    print("Done")
if __name__ == '__main__':
    driver()