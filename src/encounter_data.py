'''
Created on Oct 27, 2018

@author: mroch
'''

# Standard imports
from datetime import timedelta
import hashlib, re, os
import pytz
from collections import namedtuple
from multiprocessing.dummy import Pool
#from multiprocessing import Pool
from multiprocessing import cpu_count
import sys

# Add-on imports
import numpy as np
import h5py  # hierarchical data format 5 library
import matplotlib.pyplot as plt
from scipy import signal as sig
import requests


# Select which real discrete Fourier transform we will use
dftpack = "fftw"
if dftpack == "fftw":
    # conda/pip install pyfftw
    from pyfftw.interfaces.numpy_fft import rfft as dftreal
else:
    from np.fft import rfft as dftreal

from scipy.stats import kurtosis, kurtosistest

# local imports
from SampleExtractor import SampleExtractor
from timer import Timer
from tethys import Tethys, Calibration
from dsp.SampleStream import TimeNotInStream

class EncounterData:
    """
    EncounterData - class for managing data from an encounter
    """
    
    def __init__(self, Fs, 
                 start, stop, duration_us, filters=None, block_s=60,
                 sampEx=None,
                 data_path=None, label_file=None,
                 name="unknown",
                 project = None,
                 site = None,
                 subsite = None,
                 deploymentid = None,
                 calibration = None,
                 cache_dir = None,
                 cache_reset = False):
        """EncounterData

        Construct an encounter that allows accessing data for 
        individual trials through iteration

        Data are cached if not present, making subsequent reads faster
        Fs - Resample data to Fs if not already at that sample rate
        start, stop - datetime's over which iteration will take place
        trial_duration_us - duration of test cases
        filters - (low_Hz, high_Hz) or None for bandpassing data
        block_s - Amount of time to process each iteration
        name - encounter name for bookkeeping purposes

        One of the following must be passed in:
            1. An instance of a SampleExtractor (sampEx).  Note that the
               SampleExtractor's history should be at least a bit bigger
               than block_s or errors will occur.
            2. Data needed to construct a SampleExtractor
                data_path - dir containing audio data
                label_file - location of labels (leave None if no labels
                    are available). Permits construction of
                    label arrays for training or testing
                            
        The parameters project, site, and deploymentid are used to
        track instrument deployments and can be used in cross validation
        to ensure that certain criteria are met (e.g. don't mix training
        and test data from the same site).  These fields are interpreted
        as per the Tethys metadata project (http://tethys.sdsu.edu):
         
        project - project name, used to specify a set of deployments/funder/etc
        site - location name within a project
        deploymentid - Number associated with the deployment of an instrument
            at the project's specified site        

        dft_Hz - Hertz label for trial bins
        dft_offset_dB - Transfer function offset at trial bin frequencies

        Data can be cached for improved performance
        cache_dir - A directory where cached examples and possibly labels
            are stored.  Cache filenames are generated automatically based
            on the data characteristics and in most cases will be unique
            If None, no caching is used.
        cache_reset - If True, ignore an existing cache file and regenerate it.


        """

        self.Fs = Fs

        # effort in encounter
        self.start = start
        self.stop = stop
        self.span = stop - start
        
        self.data_path = data_path

        self.label_file = label_file
        
        self.us_per_s = 1000000
        # various ways to measure the duration of each trial:
        #  microsceconds (us), samples (N), timedelta (delta)
        self.trial_duration_us = duration_us
        self.trial_duration_N = int(self.trial_duration_us / self.us_per_s * Fs)
        self.trial_duration_delta = timedelta(microseconds = duration_us)
        # Currently assumes non-overlapping trials
        self.trial_advance_delta = self.trial_duration_delta
        self.trial_advance_N = self.trial_duration_N
        
        self.block_s = block_s
        self.block_duration = timedelta(seconds=block_s)
        
        self.filters = filters
        
        self.project = project
        self.site = site
        self.subsite = subsite
        self.deploymentid = deploymentid

        # Store transfer function
        self.calibration = calibration
        if self.calibration is None:
            # Create a flat calibration so that we can treat this like everything else
            self.calibration = tethys.Calbration("dummy", "preamp", [0, Fs], [0, 0])

        # List of Encounter Data associated with this one.
        # Can be used to list associated noise estimation regions or
        # for other purposes
        self.associated_encounters = []
        
        if sampEx is None:
            self.sampEx = SampleExtractor.build(
                data_path, label_file, self.Fs, .001, 
                cache_dir = "." if cache_dir is None else cache_dir,
                history_s=self.block_duration.total_seconds()+2)            
        else:
            self.sampEx = sampEx
            
        self.process_data = True
        self.process_labels = self.sampEx.has_labels()
        
        self.name = name
        
        self.cache_dir = cache_dir
        self.cache_read = False
        self.cache_write = False
        
        self.cache_reset = cache_reset
        self.extraction_metadata = ""
        
        # Function to be called on blocks of extracted features
        self.feature_extractor = None
        
        # Methods for nnormalizing audio data ---------------------
        # Available methods
        self.normalization_schemes = ("z-score")
        # Current method    
        self.normalize = None
        # Parameters for methods
        self.mu = None
        self.std = None
        
    def add_associated_encounter(self, e):
        "add_assocation_encounter(e) - Add an associated encounter"
        self.associated_encounters.append(e)
        
    def get_associated_encounters(self):
        """get_associated_encounters() - Get list of associated encounters"""
        return self.associated_encounters
        
    def set_feature_extractor(self, feat_ex):
        """set_feature_extractor(feat_ex)
        Function handle that can be run on retrieved examples to
        extract features.  If the features are to be concatenated the
        feature_ex function must return the existing and concatenated
        features, otherwise they are simply replaced
        """        
        self.feature_extractor = feat_ex
        
    def get_feature_extractor(self):
        "get_feature_extractor()"
        return self.feature_extractor
    
    def get_example_dim(self):
        """get_example_dim() 
        Return dimension of examples
        """
        if self.feature_extractor is not None:
            # Feature extractor must work on a dummy example
            dummy = np.random.random(
                (3, self.trial_duration_N, self.get_channelsN()))
            dumout = self.feature_extractor(self, dummy)
            N = dumout.shape[1]
        else:
            N = self.trial_duration_N
        return N
    
    def get_normalization(self):
        "get_normalization() - Report normalization state"
        return self.normalize
    
    def __generate_cache_name(self):
        """__generate_cache_name()
        Compute cache filename based on object characteristics
        """
        components = []
        components.append(
            os.path.basename(
                os.path.normpath(
                    self.sampEx.getAudioRoot())))
        duration = self.stop - self.start
        # Add timestamp/duration, changing : to _ which as : is not supported
        # on NTFS (Windows).
        components.append(str(self.start).replace(':','_'))
        components.append("dur-"+str(duration).replace(':', '_'))
        components.append("us%d"%(self.trial_duration_us))
        md5 = hashlib.md5()
        audionames = self.sampEx.getAudioNamesRelativeToRoot()
        audioname = [os.path.splitext(p)[0] for p in audionames]
        string = "".join(audioname)
        md5.update(string.encode('utf-8'))
        hashkey = md5.hexdigest()
        
        components.append(hashkey)
        self.cache_file = os.path.join(
            self.cache_dir, "_".join(components) + ".hd5")
        
        return os.path.join(
            self.cache_dir, "_".join(components) + ".hd5")
        
    def __open_cache_read(self, cachename):
        """__open_cache_read(cachename)
        Open the cache for reading.  Returns cache handle or None
        if cache is invalid or not readable
        """
        if not os.path.isfile(cachename):
            h5 = None
            print("Cache does not exist: %s"%(cachename))
        else:
            h5 = h5py.File(cachename, 'r')
            # NEED TO FIX THIS, DON'T UNDERSTAND THE ATTRIBUTES
            # seemed to be working before...
            if h5.attrs.get("status", "invalid") != "valid":
                print("Cache exists, but is marked as invalid, regenerating...")
                h5.close()
                h5 = None
        return h5
         
    def __iter__(self):
        self.current = self.start

        # Assume no information about labels until we learn otherwise
        first_label_idx = None
        last_label_idx = None
        more_labels = False             
        
        if self.process_labels:
            first_label_idx = \
                self.sampEx.findFirstClickIdxAfterAbsoluteTime(self.current)
            # if there were labels in the encounter
            if first_label_idx is not None:
                # Last label in encounter
                last_label_idx = \
                    self.sampEx.findFirstClickIdxBeforeAbsoluteTime(self.stop)
                # Any labels in the data set?
                more_labels = ((last_label_idx is not None) and
                               (first_label_idx <= last_label_idx))
        
        # Set the current, first, and last labels as well as whether
        # or not there's any labels left to process (or at all in this case)
        self.label_idx = first_label_idx
        self.start_idx = first_label_idx
        self.end_idx = last_label_idx
        self.more_labels = more_labels
        self.block_idx = 0
        
        if self.cache_dir is not None:
            # Compute cache filename based on object characteristics
            self.cache_file = self.__generate_cache_name()

            # Try and open the cache
            self.cache_write = self.cache_reset
            if not self.cache_write: 
                # See if a valid cache file exists
                self.h5 = self.__open_cache_read(self.cache_file)
                if self.h5 is None:
                    # Could not open for reading
                    self.cache_write = True
                    self.cache_read = False
                else:
                    self.cache_read = True
                    # Verify that the cache was fully written and valid.
                    # If the process died while building the cache, the state might not be valid.
                    # The last thing we do when building the cache is to set status to valid.
                    status = self.h5.attrs.get('status', "not set")
                    if status == "valid":
                        self.cache_examples = self.h5['examples']
                        self.cache_labels = self.h5['labels']
                    else:
                        # Bad cache file.  We could remove and set cache_write to True
                        # but this seems dangerous.  Thorw an exception instead.
                        self.h5.close()
                        raise ValueError("encounter %s: cache file %s exists but status = %s"%(
                            self, self.cache_file, status))

            if self.cache_write:
                self.h5 = h5py.File(self.cache_file, 'w')
                # Cache is not valid until all features have been processed 
                self.h5.attrs['status'] = "invalid"
                if self.extraction_metadata is not None:
                    self.h5.attrs['metadata'] = self.extraction_metadata
                # Set up data sets to be stored
                duration = self.stop - self.start                
                expected_trials = int(duration / self.trial_duration_delta)
                self.cache_examples = self.h5.create_dataset(
                    "examples", (expected_trials, self.trial_duration_N))
                self.cache_labels = self.h5.create_dataset(
                    "labels", (expected_trials,), dtype="b")
            
            print("Cache enabled, %s %s"%(
                "reading" if self.cache_read else "writing", self.cache_file))
        self.t = Timer()
        return self
        
    def __next__(self):
        """next() - Get the next block of data.
           Multiple items are returned
           frames - Framed data that may be augmented by any feature extraction
             methods that are enabled
           labels - Label for each frame, 1 if signal is present.  If label
             information was not provided when this object was constructed,
              the labels will be all zeros.
           start - Timestamp of first sample
           frameadv - Timedelta between subsequent samples
           blockdata - Unframed version of block data, useful for post test
              analysis when a detection is near a boundary.
        """
        
        if self.current >= self.stop:
            if self.cache_read or self.cache_write:
                if self.cache_write:
                    self.h5.attrs["status"] = "valid"  # Completed writing
                    self.cache_write = False
                self.h5.close()
                                
            raise StopIteration

        if self.cache_dir is not None:
            # Will need to worry about rounding at some point...but not today
            block_rows = int(self.block_duration / self.trial_duration_delta)
            
            cfirst = self.block_idx * block_rows
            clast = cfirst + block_rows 
            
        if self.cache_read:
            # Make sure we don't read past the end
            if self.cache_examples.shape[0] < clast:
                clast = self.cache_examples.shape[0]
            # Retrieve the examples
            frames = self.cache_examples[cfirst:clast, :]
            # Retrieve the labels if they are present
            if self.cache_labels.shape[0] > 0:
                labels = self.cache_labels[cfirst:clast]
            else:
                labels = np.zeros([0])
            
        else:
            read_delta = min(self.stop -self.current, self.block_duration)
            if self.process_data and self.filters is not None:
                datablock = self.sampEx.getBandPassedSamplesForAbsoluteTime(
                    self.current, self.filters[0], self.filters[1], 
                    numSeconds=read_delta.total_seconds())
            else:
                # User didn't want to bandpass or we won't be using the data
                # for anything other than to determine the number of frames
                # (no need to spend time bandpassing the data if we won't
                # predict from it)
                datablock = self.sampEx.getSamplesForAbsoluteTime(
                    self.current, numSeconds=read_delta.total_seconds())
                                
            # Convert to frames
            # We assume data are contiguous here, the underlying dsp.SampleStream
            # class should have enforced this.
            partial_frame = datablock.shape[0] % self.trial_duration_N
            if partial_frame > 0:
                # Partial read, probably the last block, trim it
                datablock = datablock[:-partial_frame,:]
            # Number of complete frames
            frames_N = int(datablock.shape[0] / self.trial_duration_N)
            if len(datablock.shape) > 1:
                channels = datablock.shape[1]
            else:
                # Single channel data, add channel dim to make it look like
                # everything else.
                channels = 1
                datablock = np.expand_dims(datablock, axis=2)
            frames = datablock.reshape((frames_N, self.trial_duration_N, channels))

            if self.process_labels:
                labels = self.get_labels(frames_N)
            else:
                labels = np.zeros([0])
            
            if self.cache_write:
                self.cache_examples[cfirst:cfirst+frames_N,:] = frames
                if self.process_labels:
                    self.cache_labels[cfirst:cfirst+frames_N] = labels

        if self.feature_extractor is not None:
            # Compute derived features
            frames = self.feature_extractor(self, frames)
            
        if self.normalization_schemes:
            if self.normalization_schemes == "z-score":
                frames = self.normalize_zscore(frames)

        # Block processed, prime for next
        start = self.current
        self.current = self.current + self.block_duration
        self.block_idx += 1
        
        return frames, labels, start, self.trial_advance_delta, datablock
           
    def cache_features(self):
        """cache_features()
        If caching is supported, make sure that the cache exists.        
        """ 

        # for progress reporting
        progress_m = 30 # Show progress every N m
        # Calculate how many iterations we need to hit progress_m
        m_per_s = 60  # minutes/second
        progress_blocks = int(progress_m * m_per_s / self.block_s)
        delta = timedelta(minutes=progress_m)
        duration = self.stop - self.start

        if self.cache_dir is not None:
            it = iter(self)
            if it.cache_read == False:                
                idx = 0
                while True:
                    try:
                        _data, _labels = next(it)
                        # Show progress every progress_m minutes of processed time
                        if idx % progress_blocks == 0:
                            processed = self.current - self.start
                            percent =  processed / duration * 100
                            print("Caching %s, at %s (%.0f%%)"%(self, self.current, percent))
                    except StopIteration:
                        break
                    idx = idx + 1
            else:
                print("Encounter %s: features already cached"%(self))
    

        
        
    def get_start(self):
        "get_start() - Return encounter start time"
        return self.start
    
    def get_effort(self):
        return (self.start, self.stop)
    
    def get_effort_duration(self):
        return self.stop - self.start
    
    def get_effort_ISO8601(self):
        """get_effort_ISO8601() - Return ISO8601 strings
        (Almost ISO8061, no time zone support but we always use UTC...)
        """
        return (self.start.strftime("%Y%m%dT%H:%M:%S.%fZ"),
                self.stop.strftime("%Y%m%dT%H:%M:%S.%fZ"))

    def get_filename(self, timestamp):
        "get_filename - Return filename associated with a timestamp"
        return self.sampEx._streams.get_filename(timestamp)

    def get_end(self):
        "get_end() - Return encounter end time"
        return self.stop
    
    def get_block_duration(self):
        "get_block_duration() - Time step size when iterating"
        return self.block_duration

    def get_calibration(self):
        """get_calibration - Return Calibration object,
        set to a flat calibration if one is not available
        """
        if self.calibration is None:
            calibration = Calibration("uncalibrated", "end-to-end",
                                      [0.0, self.get_Fs() / 2.0], [0.0, 0.0])
        else:
            calibration = self.calibration
        return calibration

    def get_Fs(self):
        "get_Fs() - Return sample rate"
        return self.sampEx.getFs()

    def get_bit_depth(self):
        "get_bit_depth() - Return # bits per sample before normalization to [-1,1]"
        return self.sampEx.getBitDepth()

    def get_channelsN(self):
        "get_channelsN() - Return number of channels of sample data"
        return self.sampEx._sampleStream.get_channelsN()

    def get_site(self):
        "get_site() - Return site name"
        return self.site

    def get_subsite(self):
        "get_subsite() - Return subsite (if exists, else None)"
        return self.subsite

    def get_span(self):
        "get_span() - Return (start, end) of encounter"
        return (self.start, self.stop)

    def get_deploymentId(self):
        "get_deploymentId() - Return deployment identifier"
        return self.deploymentid

    def get_project(self):
        "get_project() - Return project name"
        return self.project

    def get_dataset_descriptor(self):
        "get_dataset_descriptor() - Return data source identification string"

        # Specific to how we (http://acoustics.ucsd.edu/) do it
        return "%s%s%s"%(self.project, self.deploymentid, self.site)

    def get_labels(self, N):
        """get_labels(N) Get labels for the next N frames of data 
        
        Returns labels:
            vector with 0/1 values for when a click occurs, and 
        """

        # Analyze to build labels, which have been default to 0
        labels = np.zeros([N], dtype=np.uint8)
        frame_t = self.current
        next_frame_t = self.current + self.trial_duration_delta
        
        # Examine each frame to see if a label is in the interval
        if self.more_labels:
            next_click_start = self.sampEx.getTimeOfClickIdx(self.label_idx)
            for _fidx in range(N):
                if self.more_labels == False:
                    break # Nothing to see here with respect to labels
                else:
                    # Is the next positive label inside the current interval?
                    if next_click_start >= frame_t and \
                       next_click_start < next_frame_t:
                        # click fell in current block, note
                        labels[_fidx] = 1
                        # There might be more than one click in the processing
                        # window, move to first click outside of window
                        move_forward = True
                        self.label_idx += 1  # next label
                        self.more_labels = self.label_idx <= self.end_idx
                        while move_forward and self.more_labels:
                            next_click_start = \
                                self.sampEx.getTimeOfClickIdx(self.label_idx)
                            move_forward = next_click_start < next_frame_t
                            if move_forward:
                                self.label_idx += 1
                                self.more_labels = \
                                    self.label_idx <= self.end_idx
                                
                # update current and next frame times for next frame
                frame_t = next_frame_t
                next_frame_t += self.trial_duration_delta
    
        return labels            
    
    def set_name(self, name):
        "set_name(name) - Give encounter a name"
        self.name = name
    
    def set_normalization(self, state):
        """set_normalization(state)
        Set normalization strategy for data.
        Iterated blocks of data have the normalization strategy
        applied to each block.  Training data have the strategy
        applied to the entire data set.  
        """
        
        if not (state is None or state in self.normalization_schemes):
            raise ValueError("state must be None or string: %s"%(
                ",".join(self.normalization_schemes)))
        self.normalization_schemes = state
        
    def set_extraction_metadata(self, data):
        """"set_extraction_metadata(data)
        If set, will be saved with cache files.
        
        CAVEATS:  
            Only affects cache files that are being written
        """
        
        self.extraction_metadata = data
        if self.cache_write:
            # Already opened, go ahead and set it, otherwise we will
            # set when we create.
            self.h5['metadata'] = data
        
    def get_extraction_metadata(self):
        """get_extraction_metadata()
        
        Returns description of extraction if one was set.
        CAVEATS:  
            Must be used after calling iter()
            Only affects cache files that are being written
        """
        if self.cache_read:
            return self.h5.attrs.get("metadata", "")
        else:
            raise ValueError("No read cache available")
         
    def progress(self):
        "progress() - Return progress on iteration string"
        
        if not hasattr(self, "t"):
            value = "Iteration not started"
        else:
            value = "Encounter %s block %d (%.1f%%). Elapsed %s s"%(
                self.name, self.block_idx, 
                (self.current - self.start)/self.span*100.0, self.t.elapsed()
                )
            
        return value
    
    def get_encounter_ppRL(self, plot=False):
        """get_encounter_ppRL() - Iterate over data and return received levels
        CAVEATS:  Do not call while another iterator is in progress
        """
        data_list = []
        labels_list = []
        for b_data, b_labels in self:
            data_list.append(b_data)
            labels_list.append(b_labels)
            
        examples = np.vstack(data_list)
        labels = np.hstack(labels_list)
        
        RL_dB = self.compute_peak2peakRL_dB(examples)
        segments = 3
        seg_samples = int(np.floor(examples.shape[1] / segments))
        ratios = np.zeros([RL_dB.shape[0], segments])
        for idx in range(3):
            ratios[:,idx] = self.compute_peak2peakRL_dB(
                examples[:,idx*seg_samples:(idx+1)*seg_samples])
        
        snr_dB = np.max(ratios, axis=1)-np.min(ratios,axis=1)
        #kur = kurtosis(examples, axis=1)

        
        if plot:
            plt.figure()
            plt.hist((RL_dB[labels == 0], RL_dB[labels==1]), 100, 
                     color=("red","blue"), density=True)
            plt.legend(['noise', 'click'])
        
        return RL_dB, snr_dB #, kur
            
    def compute_peak2peakRL_dB(self, examples, bits=16, peak_freq=True):
        """"compute_peak2peakRL_dB(examples, bits)
        Return peak to peak RL in dB for each row example
        
        Assume to be floating point from N bits (scaled up for computation)
        Set bits to 0 to leave data unscaled.        
       
        """
        

        if bits > 0:
            examples = examples * 2**(bits - 1)
        mins = np.min(examples, axis=1)
        maxes = np.max(examples, axis=1)
        diff = (maxes - mins)

        p2pRL_dB = 20*np.log10(diff)
        if np.any(np.isnan(p2pRL_dB)):
            print("uh oh!")

        N = examples.shape[1]  # duration in samples


        # We are calculating the calibrated frequency response just to get
        # the peak frequency.
        calib = self.calibration.get_response(N, self.Fs)
        if peak_freq:
            ex_dB = 20 * np.log10(np.abs(dftreal(examples * calib.window)))
            # Divide (log subtract) by energy/bin to get / 1 Hz and adjust for calibration
            # Don't really need to subtract off bin1Hz_dB as we are just taking the max, but if we
            # don't someone will probably copy this code elsewhere where they should be doing it.
            ex_dB_per_Hz = ex_dB - calib.bin1Hz_dB + calib.dB

            # Find peak frequency bins
            peak_freq = np.argmax(ex_dB, axis=1)
        else:
            sample_at_Hz = 25000  # Approximate at 25 kHz
            peak_freq = int(np.round(sample_at_Hz / self.Fs * N))
        # Adjust for transfer function at peak frequency bin
        p2pRL_dB = p2pRL_dB + calib.dB[peak_freq]

        return p2pRL_dB
    
    def get_trial_idx(self, time):
        """get_trial_idx(time)
        Return the trial bin (frame) associated with a given time
        and the offset into the frame
        """
        
        delta = time - self.start
        ratio = delta / self.trial_duration_delta
        frame, start_p = divmod(ratio, 1)
        
        
        return int(frame), start_p
        
    def get_encounter_training_features(self, negsample="next"):
        """get_encounter_features(feature_ex)
        Extract features for training
        Assumes that cached features have already been generated
        
        negsample - Strategy for selecting negative examples
            "next" - next frame
            "none" - Only return positives
        Returns
        training examples - data matrix of time series and possibly
            derived features
        labels - vector of 0/1 for negative and positive class

        CAVEATS:  Does not yet support multi-channel recordings
        """

        
        texamples = []
        tlabels = []
        firstClickIdx = self.sampEx.findFirstClickIdxAfterAbsoluteTime(self.start)
        lastClickIdx = self.sampEx.findFirstClickIdxBeforeAbsoluteTime(self.stop)
            
        Nclicks = lastClickIdx - firstClickIdx + 1
        print("Extracting data for %d positives"%(Nclicks))

        cidx = firstClickIdx
        missed_neg = 0
        click_start = self.sampEx.absoluteStartOfClicks[cidx]
        more = cidx <= lastClickIdx
        read_s = self.trial_duration_delta.total_seconds()
        while more:
            # Get frame index and where we start within frame
            click_bin, frac = self.get_trial_idx(click_start)
            # Convert frame index to start time
            frame_start = click_bin*self.trial_duration_delta + self.start
            samples = self.sampEx.getSamplesForAbsoluteTime(frame_start, read_s)
            texamples.append(samples)
            tlabels.append(1)

            cidx = cidx + 1
            more = cidx <= lastClickIdx
            if more:
                next_start = self.sampEx.absoluteStartOfClicks[cidx]

            if negsample == "next":
                # If start is close to the end of the frame
                # append the next one over
                shift = 1 if frac <= .3 else 2
                noise_start = frame_start + shift * self.trial_duration_delta
                # Verify that there is not another click nearby
                # no more clicks or more clicks but too far away
                if not more or (more and next_start - noise_start > self.trial_duration_delta):
                    try:
                        negsamples = self.sampEx.getSamplesForAbsoluteTime(noise_start, read_s)
                        texamples.append(negsamples)
                        tlabels.append(0)
                    except TimeNotInStream as e:
                        missed_neg = missed_neg + 1

            click_start = next_start

            # Uncomment for benchmarking performance retrieving examples
            # if cidx - firstClickIdx > 5000:
            #     sys.exit(0)


        if negsample == "noise":
            raise NotImplementedError("Need to remove cache dependencies with move to using filtered data")
            # Retrieve noise regions and pull random bins from them 
            noiseN = lastClickIdx - firstClickIdx + 1
            noise = self.get_associated_encounters()
            choose_from = [r for r in range(len(noise))]
            last_idx = [int((n.stop - n.start) / self.trial_duration_delta)
                        for n in noise]
            self.noise_samples = []
            nidx = 0
            # Number samples per noise group
            pickN, remainder = divmod(noiseN, len(noise))
            pickN = [pickN] * len(noise)
            # Put remainder in some group... 
            pickN[0] = pickN[0] + remainder
            for n in noise:
                # Pick N random indices randomly
                # These are regions outside of encounters, so they should
                # not have any clicks, but as we have the label information
                # we will double check in case there's a labeled stray click.
                # It is always possible that there are unlabeled clicks :-(                    
                sample = np.sort(
                    np.random.randint(0, last_idx[nidx], [pickN[nidx]]))

                for idx in sample:
                    l = nlabels[idx]
                    # Only retain negative examples
                    if l == 0:
                        nsample.append(ndata[idx,:])
                        nsample_label.append(l)
                # Add to example data
                texamples.extend(nsample)
                tlabels.extend(nsample_label)
                    
                
                h5.close()
                nidx = nidx + 1

        if len(texamples) == 0:
            print("No training examples found for %s"%(self))
            texamples = np.empty([0, self.trial_duration_N])
            tlabels = np.empty([0,])
        else:
            texamples = np.vstack(texamples)
            tlabels = np.hstack(tlabels)

        produced_data = texamples.shape[0] > 0

        debug = False
        if debug and produced_data:
            # Create signal (upper) and noise (lower) subplots with linked
            # axes
            _fig, ax = plt.subplots(2, 1, num=str(self), sharex=True)
            flatax = ax.flatten()
            click_data = texamples[tlabels == 1, :]
            noise_data = texamples[tlabels == 0, :]
            w = sig.hann(click_data.shape[1])
            C = 20*np.log10(np.abs(dftreal(click_data * w)))
            N = 20*np.log10(np.abs(dftreal(noise_data * w)))
            f = np.linspace(0, self.sampEx.Fs/2, C.shape[1])
            flatax[0].pcolormesh([t for t in range(C.shape[0])], f, C.T)
            flatax[0].set_title("Clicks " + str(self))
            flatax[1].pcolormesh([t for t in range(N.shape[0])], f, N.T)
            flatax[1].set_title('Noise' + str(self))
            plt.tight_layout()
            print(self)

        # Reshape the data to add a single channel dimension
        texamples = np.expand_dims(texamples, axis=2)
        if self.feature_extractor is not None:
            texamples = self.feature_extractor(self, texamples)

        if self.normalization_schemes and produced_data:
            if self.normalization_schemes == "z-score":
                # As the training data are balanced, but the 
                # test data are heavily biased towards the negative
                # case, let's only normalize on the negative cases
                mu, std = self.get_mean_std(texamples[tlabels == 0,:])
                texamples = (texamples - mu )/ std
        
        return texamples, tlabels

    def normalize_zscore(self, data, ax=(0)):
        """normalize_zscore(data)
        Subtract mean and divide by standard deviation.
        This is intended to be run on audio samples and global
        statistics (across rows and columns) are used.
        
        Returns new z-score normalized feature, mu, and std 
        """

        # Use a preestablished mean (mu) and standard deviation (std)
        # if available, or estimate from the data.
        if self.mu == None:
            mu, std = self.get_mean_std(data, axes=ax)
        else:
            mu = self.mu
            std = self.std
            
        z = (data - mu) / std
        
        return z
    
    def get_mean_std(self, data, axes=(0)):
        "get_mean_std(data) Get mean and standard deviation"
        mu = np.mean(data, axis=axes)
        std = np.std(data, axis=axes)
        return mu, std
    
    def set_mean_std(self, mu, sigma):
        """"set_mean_std(mu, sigma)
        Set the mean (mu) and standard deviation (sigma).  If set, these
        will be used when using normalization techniques that rely on these
        statistics such as z-score.  
        
        Caveats:
        Dimensions must be consistent with how this will be used by the
        normalization technique
        """
        self.mu = mu
        self.std = sigma

    def __repr__(self):
        return "EncounterData(%s,%s)"%(self.get_start(), self.get_end())
        
def build_encounter_managers(spec, block_s = 60):
    """build_encounter_managers(spec, block_s = 60)
    Given a specification from XML, build a list of EncounterManagers.
    Each EncounterManager manages the encounters in a set of data covered
    by a specific SampleExtractor object which usually covers a deployment
    or portions thereof.
    """
    
    deployments = spec['effort']['deployment']
    if not isinstance(deployments, list):
        deployments = [deployments]

    # Some methods of getting negative examples require us to construct
    # noise encounters from between encounters.
    negatives = spec.get('negatives', 'next')
    noise_encounters = True if negatives in ['noise'] else False
    
    managers = []
    for d in deployments:
        managers.append(EncounterManager(spec, d, block_s, noise=noise_encounters))
    return managers
    
            

class EncounterManager:
    """
    EncounterManager - manages multiple encounters that are handled by
    the same SampleExtractor
    """
    deployment_info_patterns = [
        # Classic, e.g. SOCAL_E34
        r'''
                (?P<project>[A-Za-z]+) _ 
                (?P<site>[A-Za-z]+)
                (?P<deploymentid>\d+)''',
        # Deployment first:  SOCAL34E
        r'''
                (?P<project>[A-Za-z]+)
                (?P<deploymentid>\d+) 
                (?P<site>[A-Za-z]+)''',
        # New style:  SOCAL_E_63_EE
        r'''
                (?P<project>[A-Za-z]+)_
                (?P<site>[A-Za-z]+)_        
                (?P<deploymentid>\d+)_
                (?P<subsite>[A-Za-z]+) 
        '''
    ]
    deployment_info_re = [re.compile(pat, re.RegexFlag.VERBOSE)
                          for pat in deployment_info_patterns]
        
    def __init__(self, spec, effortspec, block_s = 60, 
                 noise=False, verbosity=1):
        """EncounterManager constructor(spec, effortspec, block_s, verbosity)
        spec - XML experiment specification
        effort_spec - XML specification of effort (contained in spec, but 
            there may be many, this is for a specific one)
        block_s - data block size in s
        noise - if True, create noise regions between encounters.  These are created
            as Encounter objects that are associated with the encounters being
            managed.  See if noise: block for details on selection criteria.  If managed encounters
            are too close, the noise enouncter may be from an earlier time.
        verbosity - Information log level        
        """

        self.Fs = spec['Fs']  # sample rate
        self.Nyquist = self.Fs / 2.0
    
        # duration of test bin in microseconds and samples
        self.duration_us = spec['duration_us']
        self.us_per_s = 1000000
        self.duration_N = int(self.duration_us / self.us_per_s * self.Fs) # in samples
        self.duration_delta = timedelta(microseconds = self.duration_us) # in time

        # Get information about this instrument deployment if possible
        self.deployment_name = effortspec['name']
        # Attempt to parse out instrument
        for pattern in self.deployment_info_re:
            m = pattern.match(self.deployment_name)
            if m is not None:
                break

        if m:
            # Get information needed to retrieve calibration function
            self.project = m.group('project')
            self.site = m.group('site')
            self.subsite = m.group("subsite") \
                if "subsite" in m.groupdict().keys() else None
            self.deploymentid = m.group('deploymentid')

            # Create a client for the Tethys acoustic metadata workbench
            self.tethys = Tethys(rtype="etree")
            # Get preamplifier id from deployment
            try:
                dep = self.tethys.get_deployment(self.project,
                                                 self.site,
                                                 self.deploymentid)
                parse_result = True  # obtained calibration, need to parse it
            except requests.exceptions.ConnectionError as e:
                self.calibration = None
                parse_result = False
                print("No calibration information:  Error connecting to Tethys")

            if parse_result:
                preamps = dep.findall(".//Sensors/Audio/PreampID")  # XPath
                if len(preamps) == 0:
                    print("Cannot find deployment's preamplifier")
                    self.calibration = None
                else:
                    cal = preamps[0].text
                    self.calibration = self.tethys.get_calibration_object(cal)

        else:
            self.calibration = None  # not available

        # Did user provide a location for labels?  If they provided a labelroot
        # element, we assume that they did
        if 'labelroot' in spec:
            self.label_file = os.path.join(spec['labelroot'],
                                       self.deployment_name + ".mat")
        else:
            self.label_file = None
        self.data_path = os.path.join(spec['audioroot'], self.deployment_name)
        
        
        self.block_s = block_s
        self.block_duration = timedelta(seconds = self.block_s)  # Amount of time for a block
        # If a cache directory is specified, sample extractor will be stored there, otherwise put it
        # in the audio data directory
        try:
            targetdir = spec['cachedir']
        except KeyError:
            targetdir = spec['audioroot']

        self.sampEx = SampleExtractor.build(
            self.data_path, self.label_file, self.Fs,.001,
            cache_dir = targetdir,
            history_s = self.block_duration.total_seconds()+2)
        
        try:        
            self.filterbank = [float(spec['filtering']['low_Hz']), 
                               float(spec['filtering']['high_Hz'])]
            self.bandpass = True
        except KeyError:
            self.filterbank = None
            self.bandpass = False
            print("Data are not bandpass filtered.")
            
        encounter = 0
        
        # Determine how to set up effort -------------------------------
        eff_type = effortspec['type']
        assert(eff_type in ['time', 'offset_h', 'offset_label'])
        
        starts = effortspec['start']
        ends = effortspec['end']
        if eff_type == "time":
            pass
        else:
            # Replicate end if only one given
            if len(ends) == 1:
                ends = ends * len(starts)
            if eff_type == "offset_h":
                # Won't deal well with gaps in data
                starts = [self.sampEx.wavDates[0]+timedelta(hours=float(h)) 
                          for h in starts]
                ends = [s + timedelta(hours=float(h)) for s, h in zip(starts, ends)]
            else:
                # Set effort a little before and after the first and last
                # click in the encounter
                self.start_clicks = [int(s) for s in starts]
                self.end_clicks = [s+int(e) for s, e in zip(self.start_clicks, ends)] 
                starts = [self.sampEx.absoluteStartOfClicks[s] 
                          - 2*self.duration_delta 
                             for s in self.start_clicks]
                ends =  [self.sampEx.absoluteStartOfClicks[e] 
                         + 2*self.duration_delta
                             for e in self.end_clicks]                        
        
        self.starts = starts
        self.ends = ends
        
        # Check that ends are after starts and that they do not overlap
        previous_end = None
        err = []
        print("Encounter description")
        for eidx in range(len(starts)):
            # Validate
            if starts[eidx] > ends[eidx]:
                err.append("Encounter %d: start %s after end %s"%(
                    eidx+1,starts[eidx],ends[eidx]))
            if eidx > 0:
                if starts[eidx] < ends[eidx-1]:
                    err.append("Encounters overlap:  %d: (%s, %s) %d: (%s %s)"%(
                        eidx, starts[eidx-1], ends[eidx-1],
                        eidx+1,starts[eidx],ends[eidx]))
        if len(err) > 0:
            raise ValueError("Encounter specification problem\n\t%s"%(
                "\n\t".join(err)))

        try:
            cache_dir = spec["cachedir"]
        except KeyError:
            cache_dir = None
        
        if verbosity > 0:
            print("Identified encounters: --------------------------------------------------")
        self.encounters = []
        for start_effort, end_effort in zip(starts, ends):
            e = EncounterData(
                self.Fs, start_effort, end_effort, self.duration_us, 
                self.filterbank, sampEx = self.sampEx,
                cache_dir = cache_dir,
                project = self.project,
                site = self.site,
                subsite = self.subsite,
                deploymentid = self.deploymentid,
                calibration = self.calibration)
            if verbosity > 0:
                print(e)
            self.encounters.append(e)
        if verbosity > 0:
            print("end identified encounters: ----------------------------------------------")

        if noise:
            # Build up noise regions from which we will draw negative
            # examples.  We identify regions with minimum time spans
            # between encounters and create EncounterData's without labels for
            # these "noise encounters"
            min_noise_regions = timedelta(minutes=30)
            noise_delta = timedelta(minutes=5)  # Amount of noise on either side


            noise_region_spans = []
            # Examine end of previous encounter and start of next.
            # if long enough, pick center and place noise start/end
            # noise_delta away from the center
            self.noise_regions = []
            eidx = -1
            for s, e in zip(ends[0:-1], starts[1:]):
                eidx = eidx + 1
                duration = e - s
                if duration > min_noise_regions:
                    # Long enough to use as a noise region
                    center = s + (e-s) / 2
                    nstart = center - noise_delta
                    nend = center + noise_delta
                    noise_encounter = EncounterData(
                        self.Fs, nstart, nend, self.duration_us,
                        self.filterbank, sampEx=self.sampEx,
                        cache_dir = cache_dir,
                        project = self.project,
                        site = self.site,
                        deploymentid=self.deploymentid,
                        calibration = self.calibration)
                    self.noise_regions.append(noise_encounter)

            # Find closest noise region before and after
            prior_idx = 0  # index of noise region prior to encounter
            noiseN = len(self.noise_regions)
            if verbosity > 0:
                print("Encounter noise descriptions ---------------------------------------")
            eidx = 0
            for e in self.encounters:
                # Find the closest noise region before and after

                if self.noise_regions[prior_idx].stop < e.start:
                    # Noise region is before the start of the encounter.
                    # See if we can find a closer one.
                    more = prior_idx < noiseN - 2
                    while more and self.noise_regions[prior_idx+1].stop < e.start:
                        prior_idx = prior_idx + 1
                    e.add_associated_encounter(self.noise_regions[prior_idx])

                # Find the closest noise region after the encounter
                post_idx = prior_idx
                at_end = False
                while not at_end and e.stop >= self.noise_regions[post_idx].start:
                    post_idx = post_idx + 1
                    at_end = post_idx >= noiseN - 1
                if not at_end:
                    e.add_associated_encounter(self.noise_regions[post_idx])

                if verbosity > 0:
                    print("%d: %s, noise:\n"%(eidx, e), end="")
                    print("\n".join(['\t%s'%(n)
                                     for n in e.get_associated_encounters()]))
                eidx = eidx+1
            if verbosity > 0:
                print("end encounter noise descriptions -----------------------------------")

                
                
                
            
        
        
                
            
        
        
        
    def __getitem__(self, idx):
        return self.encounters[idx]
    
    def __iter__(self):
        return iter(self.encounters)
    
    def get_durations(self):
        "get_durations() - Return list of timedeltas for encounters"
        
        deltas = [e.stop - e.start for e in self.get_encounters()]
        return deltas
    
    def get_encounters(self):
        "get_encounters() - Return list of encounter objects"
        return self.encounters
    
    def get_encounter_containing_time(self, span):
        """get_encounter_containing_time(span)
        Given a 2 element array-like object, span, with start and end timestamps,
        find the EncounterData object that overlaps with this time span
        """

        # Look through the encounters until we find one that overlaps
        found = False
        idx = 0
        # Early versions of the cross-validator save code
        # rounded the time and did not store milliseconds
        # Provide fudge factor for checking the span over
        # which detections were run
        offset = timedelta(seconds=1)
        
        while not found and idx < len(self.encounters):
            encounter = self.encounters[idx]
            effort = encounter.get_effort()
            # effort is timezone naive, assume UTC and make timezone
            # aware
            effstart = pytz.utc.localize(effort[0])
            effend = pytz.utc.localize(effort[1])
            found = effstart - offset <= span[0] and \
                span[1] <= effend + offset
            if not found:
                idx += 1
        if not found:
            encounter = None
        return encounter
    
    def get_example_dim(self):
        """get_example_dim() 
        Return dimension of examples
        """        
        # Use the first EncounterData object to retrieve the shape
        # of the example data
        return self.encounters[0].get_example_dim()

    def get_feature_extractor(self):
        """get_feature_extractor()
        """
        # All encounters should have the same feature extractor.
        # If you are using an EncounterManager and you set different
        # encounters to different feature extractors... boy are you
        # hosed...
        #
        # Use these getter and setters and you will be A-OK.
        return self.encounters[0].feature_extractor
    
    def set_feature_extractor(self, feat_ex):
        """set_feature_extractor(feat_ex)
        Function handle that can be run on retrieved examples to
        extract features.  If the features are to be concatenated the
        feature_ex function must return the existing and concatenated
        features, otherwise they are simply replaced
        """

        # Set 'em all...        
        for e in self.encounters:
            e.set_feature_extractor(feat_ex)

    def set_normalization(self, state):
        """set_normalization(state)
        Set normalization strategy for data.
        Iterated blocks of data have the normalization strategy
        applied to each block.  Training data have the strategy
        applied to the entire data set.  
        """
        
        # Set normalization strategy for each encounter
        for e in self.encounters:
            e.set_normalization(state)
    
    

def precache_encounters(encounters, cpus=None, reserve = 2):
    """precache_encounters(cpus, concurrency)

    encounters - List of encounters to be cached.  Any associated encounters contained in these encounters
       will also be cached
    concurrency - Number of processors to use.  If None, uses number of processors - reserve
    reserve - When concurrency is not specified, uses all processors except for number reserved
    :type encounters: list(EncounterData)
    :type cpus: int or None
    :type reserve: int
    """
    if cpus is None:
         # Number of CPUs that should not be exploited
        try:
            cpus = max(1, cpu_count() - reserve)
        except NotImplementedError:
            cpus = 2
            print("Unable to determine number of CPUS, defaulting to %s"%(cpus))
    # Reduce cpu pool if not needed
    cpus = min(len(encounters), cpus)

    # Create copy of encounter list as we will extend
    cache_targets = []
    cache_targets.extend(encounters)

    # Add in associated encounters if present
    associated = []
    for e in encounters:
        assoc = e.get_associated_encounters()
        for a in assoc:
            if a not in assoc:
                associated.append(a)
    cache_targets.extend(associated)

    # Set up the pool and start caching
    if cpus > 1:
        print("Parallel pool for caching with %d processors"%(cpus))
        pool = Pool(cpus)
        output = pool.map(precache_an_encounter, cache_targets)
        pool.close()
        print(output)
    else:
        for target in cache_targets:
            precache_an_encounter((target))

def precache_an_encounter(enc):
    """precache_an_encounter(enc)
    :type e: EncounterData
    """
    print("Caching %s"%(enc))
    try:
        enc.cache_features()
    except Exception as e:
        print("Unable to cache encounter %s"%(enc))
        print(e, file=sys.stderr)
        raise e

    return True  # success

