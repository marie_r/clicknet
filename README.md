# DeepClick

Keras/Tensorflow time-domain click detector

## Dependencies
Required pacakges that can be installed
by using conda or pip:

- tensorflow
- keras
- matplotlib
- scikit-learn
- pandas


Required packages that can be installed by 
using pip

- xmltodict
- pysoundfile
- sortedcontainers

Other required pacakges:

- [dsp](https://bitbucket.org/deepcontext/dsp/) on BitBucket


## Description

This package is used time-domain echolocation click detection.  The
sample rate of the test data must match the sample rate of the test
data, and any filtering on training data must be applied to test data.

## System parameters
  
The system expects an extended markup language (XML) description file.  See sample.xml for a description and exmaple file.  In broad strokes, it has the following format:

~~~
<experiment>
  <method> MethodType </method>
  <model>
    <type> ModelName </type>
    <parameters>
      <NameP1> value </NameP1>
      ...
      <NamePN> value </NamePN>
    </parameters>
    <eval> ModelFilename </eval>
  </model>

  <Fs type="int"> SampleRate </Fs>
  <audioroot> PathToRootAudioDirectory </audioroot>
  <labelroot> PathToLabelRootDirectory </labelroot>

  <duration_us type="float"> N </duration_us>

  <negatives> NonClickSelectionStrategy </negatives>

  <effort>
     <deployment>
        <name> Name1 </name>
	<type> DataSelectionType e.g. time </type>
	<start> List of regions to start </start>
	<end> List of corresponding end times </end>
     </deployment>
     <deployment>  2nd deployment (name, ...) </deployment>
     <deployment> ...  </deployment>
     <deployment> last deployment </deployment>
  </effort>
</experiment>
~~~

When non-character values such as lists or numbers are desired, use
the attribute type in the element name to specify a type.  Valid types
are "int" for integer, "float" for real numbers, "datetime" for
ISO8601 timestamps (e.g. 2015-02-20T15:37:29.990Z), and list(type) for
a list of comma separated entries of the specified type.  For example, to use timestamps to specify a list of start times, one would use:

~~~
      <start type="list(datetime)">
        2014-10-07T00:00:00.010Z,...more timestamps...,2015-02-17T00:00:00.010Z
      </start>
~~~

A concrete example (without data) can be found in
sample_experiment.xml.  The system is invoked with run_model.py
followed by the filename of the XML specification file.  The command
line argument -m (or --method) can be used to override the method
parameter of the XML file.

- method: Describes system task.  Must be one of:

    - crossval - Run a cross validation experiment.  Each element of
   effort/deployment/start is considered one group of data and will
   always be entirely in train or test.

    - crossval_results - After a cross validation experiment has been
    generated, interpret the results with respect to ground truth.

    - eval - Apply the trained model specified whose filename is in
   model/eval to the effort data.

   - postprocess - Runs a method called postprocess which is designed for
   ad-hoc analysis.

- model: Specify model architecture.

    - type - One of the models listed in
      classifier.click_arch.architectures.keys().

    - parameters - Depends on type. Each architecture has a set of
      named parameters that can be passed to it.  See the model
      descriptions in classifier.click_arch.arhitectures.  Examples of
      things specified include width, number of epochs to train,
      regularization parameters, etc.

    - eval - Only needed if using method eval. Specifies a filename
      for a trained model.

- Fs: Expected sample rater of data

- audioroot: System path to audio data

- labelroot: System path to labels.  Only needed when scoring or training.

- duration_us: Data are analyzed for click presence based on bins of
  this duration in microseconds.

- negatives: Only used in training.  Strategy for find examples
  without echolocation clicks.


    - next - Uses next bin without a click. 

    - TODO:  Fill in other strategies.

- effort: Specifies which data are to be used. Data are frequently
  organized into subdirectories. Each directory is assumed to be a
  deployment (it may have subdirectories within it), and a sequence of
  deployments are nested within the effort.

    - Each deployment element contains:

        - name:  Deployment name.  Corresponds to the subdirectory name.

        - type: How do we reference effort within this deployment?
          Must be one of:
	    - time - ISO8601 datetimes are used
	    - Other methods are implmented, but less useful now and not
	      documented here.

        - start:  List of starts (be sure to specify the type, e.g.
	  type=list(datetime).

        - end - List of ends (specify type).

- feature-transformation: Specify name of data transdformation to run.
  Valid transforms: add_SNR_ppRL_dB

Running an experiment produces results in a directory that is a peer to the XML specification file with the same name minus the extension.  If the specifcation is foobar.xml, results will be in foobar.

Timestamped directories are created within this directory for training or crossvalidation.  One model is created per test fold, with the pattern model_fi.h5 where i is the fold number.  

When testing, a result file is produced (either in the peer directory or in a timestamped directory depending on whether or training was done), the following files are created:

- result.h5: An HDF5 file containing reuslts.  It specifies effort and stores the timestamp, received level, and proability score for each potential click. To prevent very large files, only clicks with scores above a threshold (not yet settable in the XML) are stored.

- Detection editor files:  For each deployment and fold, a TPWS file is created for each deployment using a data id that is derived from audio filenames. A master TPWS file merges all deployments within a fold.  These are stored in fold directories, deafulting to fold0 for non cross-validation experiments.

- Triton label files:  When label data are available, for each fold we create .tlab files that document missed detections, false positive detections, and matched detections.  These can be displayed in Triton with the lable Remora.

## Notes on DetEdit

DetEdit is a separate program https://github.com/MarineBioAcousticsRC/DetEdit designed to analyze and edit detections.  The TPWS files that we create are used in DetEdit.



  
	
	
	







